<?php
	class Usuarios{
		///Propiedades
		public $id_usuario = false;
		public $ap_Paterno = false;
		public $ap_Materno = false;
		public $nombre = false;
		public $f_nacimiento = false;
		public $correo_electronico = false;
		public $contrasena = false;
		public $telefono = false;
		public $organizacion = false;
		public $colonia = false;
		public $id_tipo_usuario = false;
		public $usuario_activo = false;
	
		///Metodos
		function Get_id_usuario(){
			if ( $this->id_usuario !== false) return $this->id_usuario;
		}

		function Get_ap_Paterno(){
			if ( $this->ap_Paterno !== false) return $this->ap_Paterno;
		}

		function Get_ap_Materno(){
			if ( $this->ap_Materno !== false) return $this->ap_Materno;
		}
		
		function Get_nombre(){
			if ( $this->nombre !== false) return $this->nombre;
		}

		function Get_f_nacimiento(){
			if ( $this->f_nacimiento !== false) return $this->f_nacimiento;
		}

		function Get_correo_electronico(){
			if ( $this->correo_electronico !== false) return $this->correo_electronico;
		}
		
		function contrasena(){
			if ( $this->contrasena !== false) return $this->contrasena;
		}

		function Get_telefono(){
			if ( $this->telefono !== false) return $this->correo_electronico;
		}
		
		function Get_organizacion(){
			if ( $this->organizacion !== false) return $this->correo_electronico;
		}
		
		function Get_colonia(){
			if ( $this->colonia !== false) return $this->correo_electronico;
		}
		
		function Get_id_tipo_usuario(){
			if ( $this->id_tipo_usuario !== false) return $this->id_tipo_usuario;
		}

		function Get_usuario_activo(){
			if ( $this->usuario_activo !== false) return $this->usuario_activo;
		}		
		
	}
	
	class Cursos{
		//Propiedades
		public $idCurso = false;
		public $nombre_curso = false;
		public $sub_titulo_curso = false;
		public $descripcion_curso = false;
		public $duracion_curso = false;
		public $inicio_curso = false;
		public $cierre_curso = false;		
		public $hora_llegada_h = false;
		public $hora_llegada_p = false;
		public $hora_salida_h = false;
		public $hora_salida_p = false;
		public $dias_c = false;		
		public $nivel_curso = false;
		public $organizacion = false;
		public $certificacion = false;		
		public $pago_unico_curso = false;
		public $pago_semanal_curso = false;
		public $tendra_costo_ins = false;
		public $pago_inscripcion_curso = false;
		public $Imagen = false;
		public $IsActive = false;
		
		//Metodos	
		function Get_idCurso(){
		if ( $this->idCurso !== false) return $this->idCurso;
		}	

		function Get_nombre_curso(){
		if ( $this->nombre_curso !== false) return $this->nombre_curso;
		}

		function Get_sub_titulo_curso(){
		if ( $this->sub_titulo_curso !== false) return $this->sub_titulo_curso;
		}

		function Get_descripcion_curso(){
		if ( $this->descripcion_curso !== false) return $this->descripcion_curso;
		}

		function Get_duracion_curso_n(){
		if ( $this->duracion_curso_n !== false) return $this->duracion_curso_n;
		}

		function Get_duracion_curso_p(){
		if ( $this->duracion_curso_p !== false) return $this->duracion_curso_p;
		}

		function Get_inicio_curso(){
		if ( $this->inicio_curso !== false) return $this->inicio_curso;
		}

		function Get_cierre_curso(){
		if ( $this->cierre_curso !== false) return $this->cierre_curso;
		}
		
		function Get_hora_llegada_h(){
		if ( $this->hora_llegada_h !== false) return $this->hora_llegada_h;
		}
		
		function Get_hora_llegada_p(){
		if ( $this->hora_llegada_p !== false) return $this->hora_llegada_p;
		}
		
		function Get_hora_salida_h(){
		if ( $this->hora_salida_h !== false) return $this->hora_salida_h;
		}
		
		function Get_hora_salida_p(){
		if ( $this->hora_salida_p !== false) return $this->hora_salida_p;
		}
		
		function Get_dias_c(){
		if ( $this->dias_c !== false) return $this->dias_c;
		}

		function Get_nivel_curso(){
		if ( $this->nivel_curso !== false) return $this->nivel_curso;
		}
		
		function Get_organizacion(){
		if ( $this->organizacion !== false) return $this->organizacion;
		}		

		function Get_certificacion(){
		if ( $this->certificacion !== false) return $this->certificacion;
		}
		
		function Get_pago_unico_curso(){
		if ( $this->pago_unico_curso !== false) return $this->pago_unico_curso;
		}
		
		function Get_pago_semanal_curso(){
		if ( $this->pago_semanal_curso !== false) return $this->pago_semanal_curso;
		}
		
		function Get_tendra_costo_ins(){
		if ( $this->tendra_costo_ins !== false) return $this->tendra_costo_ins;
		}
		
		function Get_pago_inscripcion_curso(){
		if ( $this->pago_inscripcion_curso !== false) return $this->pago_inscripcion_curso;
		}

		function Get_Imagen(){
		if ( $this->Imagen !== false) return $this->Imagen;
		}

		function Get_IsActive(){
		if ( $this->IsActive !== false) return $this->IsActive;
		}		
	}
	
	class Eventos{
		//Propiedades
		public $id_evento = false;
		public $nombre_evento = false;
		public $sub_titulo_evento = false;
		public $descripcion_evento = false;
		public $duracion_evento = false;
		public $fecha_inicio = false;
		public $fecha_cierre = false;
		public $hora_inicio_evento_h = false;
		public $hora_inicio_evento_p = false;
		public $PO_imparten = false;
		public $isActive = false;
		
		//Metodos	
		function Get_id_evento(){
		if ( $this->id_evento !== false) return $this->id_evento;
		}
		
		function Get_nombre_evento(){
		if ( $this->nombre_evento !== false) return $this->nombre_evento;
		}
		
		function Get_sub_titulo_evento(){
		if ( $this->sub_titulo_evento !== false) return $this->sub_titulo_evento;
		}
		
		function Get_descripcion_evento(){
		if ( $this->descripcion_evento !== false) return $this->descripcion_evento;
		}
		
		function Get_duracion_evento(){
		if ( $this->duracion_evento !== false) return $this->duracion_evento;
		}
		
		function Get_fecha_inicio(){
		if ( $this->fecha_inicio !== false) return $this->fecha_inicio;
		}
		
		function Get_fecha_cierre(){
		if ( $this->fecha_cierre !== false) return $this->fecha_cierre;
		}
		
		function Get_hora_inicio_evento_h(){
		if ( $this->hora_inicio_evento_h !== false) return $this->hora_inicio_evento_h;
		}
		
		function Get_hora_inicio_evento_p(){
		if ( $this->hora_inicio_evento_p !== false) return $this->hora_inicio_evento_p;
		}
		
		function Get_PO_imparten(){
		if ( $this->PO_imparten !== false) return $this->PO_imparten;
		}
		
		function Get_isActive(){
		if ( $this->isActive !== false) return $this->isActive;
		}
		
	}
	
	class Estudiantes_cenaltec{
		//Propiedades
		public $id_usuario = false;
		public $fecha_actual = false;
		public $apellido_paterno = false;
		public $apellido_materno = false;
		public $nombre = false;
		public $calle_numero = false;
		public $colonia = false;
		public $cp = false;
		public $municipio = false;
		public $estado = false;
		
		public $sexo = false;
		public $curp = false;
		public $fecha_nacimiento = false;
		public $lugar_nacimiento = false;
		public $telefono_casa = false;
		public $telefono_trabajo = false;
		public $celular = false;
		public $estado_civil = false;
		public $correo_electronico = false;
		public $especificar_estudios = false;
		
		public $tomado_curso_antes = false;
		public $servicio_medico = false;
		public $alergias_enf = false;
		public $tipo_sangre = false;
		public $presenta_discapacidad = false;
		public $nombre_emergencia = false;
		public $telefono_emergencia = false;
		public $nombre_empresa = false;
		public $puesto = false;
		public $direccion_empresa = false;
		
		public $antiguedad = false;
		public $apoyo_empresa = false;
		public $medio_enterado = false;
		public $motivos_entrenamiento = false;
		public $curso = false;
		public $inicio_curso = false;
		public $duracion_curso = false;
		public $horario_curso = false;
		public $requiere_factura = false;
		public $nombre_factura = false;
		
		public $razon_social = false;
		public $rfc_fac = false;
		public $SFDI_fac = false;
		
		//Metodos	
		function Get_id_usuario(){
		if ( $this->id_usuario !== false) return $this->id_usuario;
		}
		
		function Get_fecha_actual(){
		if ( $this->fecha_actual !== false) return $this->fecha_actual;
		}	

		function Get_apellido_paterno(){
		if ( $this->apellido_paterno !== false) return $this->apellido_paterno;
		}	

		function Get_apellido_materno(){
		if ( $this->apellido_materno !== false) return $this->apellido_materno;
		}	

		function Get_nombre(){
		if ( $this->nombre !== false) return $this->nombre;
		}	

		function Get_calle_numero(){
		if ( $this->calle_numero !== false) return $this->calle_numero;
		}	

		function Get_colonia(){
		if ( $this->colonia !== false) return $this->colonia;
		}	

		function Get_cp(){
		if ( $this->cp !== false) return $this->cp;
		}	

		function Get_municipio(){
		if ( $this->municipio !== false) return $this->municipio;
		}	

		function Get_estado(){
		if ( $this->estado !== false) return $this->estado;
		}	

		function Get_sexo(){
		if ( $this->sexo !== false) return $this->sexo;
		}	

		function Get_curp(){
		if ( $this->curp !== false) return $this->curp;
		}	

		function Get_fecha_nacimiento(){
		if ( $this->fecha_nacimiento !== false) return $this->fecha_nacimiento;
		}	

		function Get_lugar_nacimiento(){
		if ( $this->lugar_nacimiento !== false) return $this->lugar_nacimiento;
		}	

		function Get_telefono_casa(){
		if ( $this->telefono_casa !== false) return $this->telefono_casa;
		}	

		function Get_telefono_trabajo(){
		if ( $this->telefono_trabajo !== false) return $this->telefono_trabajo;
		}	

		function Get_celular(){
		if ( $this->celular !== false) return $this->celular;
		}	

		function Get_estado_civil(){
		if ( $this->estado_civil !== false) return $this->estado_civil;
		}	

		function Get_correo_electronico(){
		if ( $this->correo_electronico !== false) return $this->correo_electronico;
		}	

		function Get_especificar_estudios(){
		if ( $this->especificar_estudios !== false) return $this->especificar_estudios;
		}	

		function Get_tomado_curso_antes(){
		if ( $this->tomado_curso_antes !== false) return $this->tomado_curso_antes;
		}	

		function Get_servicio_medico(){
		if ( $this->servicio_medico !== false) return $this->servicio_medico;
		}	

		function Get_alergias_enf(){
		if ( $this->alergias_enf !== false) return $this->alergias_enf;
		}	

		function Get_tipo_sangre(){
		if ( $this->tipo_sangre !== false) return $this->tipo_sangre;
		}	

		function Get_presenta_discapacidad(){
		if ( $this->presenta_discapacidad !== false) return $this->presenta_discapacidad;
		}	

		function Get_nombre_emergencia(){
		if ( $this->nombre_emergencia !== false) return $this->nombre_emergencia;
		}	

		function Get_telefono_emergencia(){
		if ( $this->telefono_emergencia !== false) return $this->telefono_emergencia;
		}	

		function Get_nombre_empresa(){
		if ( $this->nombre_empresa !== false) return $this->nombre_empresa;
		}	

		function Get_puesto(){
		if ( $this->puesto !== false) return $this->puesto;
		}	

		function Get_direccion_empresa(){
		if ( $this->direccion_empresa !== false) return $this->direccion_empresa;
		}	

		function Get_antiguedad(){
		if ( $this->antiguedad !== false) return $this->antiguedad;
		}	

		function Get_apoyo_empresa(){
		if ( $this->apoyo_empresa !== false) return $this->apoyo_empresa;
		}	

		function Get_medio_enterado(){
		if ( $this->medio_enterado !== false) return $this->medio_enterado;
		}	

		function Get_motivos_entrenamiento(){
		if ( $this->motivos_entrenamiento !== false) return $this->motivos_entrenamiento;
		}	

		function Get_curso(){
		if ( $this->curso !== false) return $this->curso;
		}	

		function Get_inicio_curso(){
		if ( $this->inicio_curso !== false) return $this->inicio_curso;
		}	

		function Get_duracion_curso(){
		if ( $this->duracion_curso !== false) return $this->duracion_curso;
		}	

		function Get_horario_curso(){
		if ( $this->horario_curso !== false) return $this->horario_curso;
		}	

		function Get_requiere_factura(){
		if ( $this->requiere_factura !== false) return $this->requiere_factura;
		}	

		function Get_nombre_factura(){
		if ( $this->nombre_factura !== false) return $this->nombre_factura;
		}		

		function Get_razon_social(){
		if ( $this->razon_social !== false) return $this->razon_social;
		}	

		function Get_rfc_fac(){
		if ( $this->rfc_fac !== false) return $this->rfc_fac;
		}	

		function Get_SFDI_fac(){
		if ( $this->SFDI_fac !== false) return $this->SFDI_fac;
		}			
	}	
	
	class Estudiantes_icatech{
		//Propiedades
		public $id_usuario  = false;
		public $fecha_actual = false;
		public $nombre_curso = false;
		public $fecha_inicio = false;
		public $fecha_termino = false;
		public $horario_curso = false;
		public $apellido_paterno = false;
		public $apellido_materno = false;
		public $nombre = false;
		public $edad = false;
		
		public $rfc = false;
		public $curp = false;
		public $domicilio = false;
		public $colonia = false;
		public $ciudad = false;
		public $estado = false;
		public $telefono = false;
		public $telefono_trabajo = false;
		public $correo_electronico = false;
		public $fecha_nacimiento = false;
		
		public $lugar_nacimiento = false;
		public $sexo = false;
		public $estado_civil = false;
		public $discapacidad_presenta = false;
		public $grado_estudios = false;
		public $condicion_social = false;
		public $nombre_empresa = false;
		public $antiguedad = false;
		public $direccion_empresa = false;
		public $ocupacion_principal	 = false;
		
		public $situacion_actual = false;
		public $medio_enterado = false;
		public $razon_eleccion = false;
		public $razon_registro = false;
		public $explicacion_pago = false;
		public $areas_capacitacion = false;
		
		//Metodos	
		function Get_id_usuario(){
		if ( $this->id_usuario !== false) return $this->id_usuario;
		}	
		
		function Get_fecha_actual(){
		if ( $this->fecha_actual !== false) return $this->fecha_actual;
		}	

		function Get_nombre_curso(){
		if ( $this->nombre_curso !== false) return $this->nombre_curso;
		}	

		function Get_fecha_inicio(){
		if ( $this->fecha_inicio !== false) return $this->fecha_inicio;
		}	

		function Get_fecha_termino(){
		if ( $this->fecha_termino !== false) return $this->fecha_termino;
		}	

		function Get_horario_curso(){
		if ( $this->horario_curso !== false) return $this->horario_curso;
		}	

		function Get_apellido_paterno(){
		if ( $this->apellido_paterno !== false) return $this->apellido_paterno;
		}	

		function Get_apellido_materno(){
		if ( $this->apellido_materno !== false) return $this->apellido_materno;
		}	

		function Get_nombre(){
		if ( $this->nombre !== false) return $this->nombre;
		}	

		function Get_edad(){
		if ( $this->edad !== false) return $this->edad;
		}	

		function Get_rfc(){
		if ( $this->rfc !== false) return $this->rfc;
		}	

		function Get_curp(){
		if ( $this->curp !== false) return $this->curp;
		}	

		function Get_domicilio(){
		if ( $this->domicilio !== false) return $this->domicilio;
		}	

		function Get_colonia(){
		if ( $this->colonia !== false) return $this->colonia;
		}	

		function Get_ciudad(){
		if ( $this->ciudad !== false) return $this->ciudad;
		}	

		function Get_estado(){
		if ( $this->estado !== false) return $this->estado;
		}	

		function Get_telefono(){
		if ( $this->telefono !== false) return $this->telefono;
		}	

		function Get_telefono_trabajo(){
		if ( $this->telefono_trabajo !== false) return $this->telefono_trabajo;
		}	

		function Get_correo_electronico(){
		if ( $this->correo_electronico !== false) return $this->correo_electronico;
		}	

		function Get_fecha_nacimiento(){
		if ( $this->fecha_nacimiento !== false) return $this->fecha_nacimiento;
		}	

		function Get_lugar_nacimiento(){
		if ( $this->lugar_nacimiento !== false) return $this->lugar_nacimiento;
		}	

		function Get_sexo(){
		if ( $this->sexo !== false) return $this->sexo;
		}	

		function Get_estado_civil(){
		if ( $this->estado_civil !== false) return $this->estado_civil;
		}	

		function Get_discapacidad_presenta(){
		if ( $this->discapacidad_presenta !== false) return $this->discapacidad_presenta;
		}	

		function Get_grado_estudios(){
		if ( $this->grado_estudios !== false) return $this->grado_estudios;
		}	

		function Get_condicion_social(){
		if ( $this->condicion_social !== false) return $this->condicion_social;
		}	

		function Get_nombre_empresa(){
		if ( $this->nombre_empresa !== false) return $this->nombre_empresa;
		}	

		function Get_antiguedad(){
		if ( $this->antiguedad !== false) return $this->antiguedad;
		}	

		function Get_direccion_empresa(){
		if ( $this->direccion_empresa !== false) return $this->direccion_empresa;
		}	

		function Get_ocupacion_principal(){
		if ( $this->ocupacion_principal !== false) return $this->ocupacion_principal;
		}	

		function Get_situacion_actual(){
		if ( $this->situacion_actual !== false) return $this->situacion_actual;
		}	

		function Get_medio_enterado(){
		if ( $this->medio_enterado !== false) return $this->medio_enterado;
		}	

		function Get_razon_eleccion(){
		if ( $this->razon_eleccion !== false) return $this->razon_eleccion;
		}	

		function Get_razon_registro(){
		if ( $this->razon_registro !== false) return $this->razon_registro;
		}	

		function Get_explicacion_pago(){
		if ( $this->explicacion_pago !== false) return $this->explicacion_pago;
		}	

		function Get_areas_capacitacion(){
		if ( $this->areas_capacitacion !== false) return $this->areas_capacitacion;
		}			
	}		
	
	class Instructores{
		//Propiedades
		public $id_instructor  = false;
		public $nombre_ins = false;
		public $apellido_pa_ins = false;
		public $apellido_ma_ins = false;
		public $correo_ins = false;
		public $CURP = false;
		public $contrasena = false;
		public $cambiocontrasena = false;
		public $id_tipo_usuario = false;
		public $isActive = false;
		
		//Metodos	
		function Get_id_instructor(){
		if ( $this->id_instructor !== false) return $id_instructor->idCurso;
		}	

		function Get_nombre_ins(){
		if ( $this->nombre_ins !== false) return $this->nombre_ins;
		}	

		function Get_apellido_pa_ins(){
		if ( $this->apellido_pa_ins !== false) return $this->apellido_pa_ins;
		}	

		function Get_apellido_ma_ins(){
		if ( $this->apellido_ma_ins !== false) return $this->apellido_ma_ins;
		}	

		function Get_correo_ins(){
		if ( $this->correo_ins !== false) return $this->correo_ins;
		}	

		function Get_CURP(){
		if ( $this->CURP !== false) return $this->CURP;
		}	

		function Get_contrasena(){
		if ( $this->contrasena !== false) return $this->contrasena;
		}	

		function Get_cambiocontrasena(){
		if ( $this->cambiocontrasena !== false) return $this->cambiocontrasena;
		}	

		function Get_id_tipo_usuario(){
		if ( $this->id_tipo_usuario !== false) return $this->id_tipo_usuario;
		}	

		function Get_isActive(){
		if ( $this->isActive !== false) return $this->isActive;
		}	

		function Get_(){
		if ( $this->idCurso !== false) return $this->idCurso;
		}			
	}	

	class Motivos_Visitas{
		//Propiedades
		public $id_motivos_de_visita = false;
		public $descripcion = false;
		public $campo_valido = false;
		
		//Metodos	
		function Get_id_motivos_de_visita(){
		if ( $this->id_motivos_de_visita !== false) return $this->id_motivos_de_visita;
		}
		
		function Get_descripcion(){
		if ( $this->descripcion !== false) return $this->descripcion;
		}
		
		function Get_campo_valido(){
		if ( $this->campo_valido !== false) return $this->campo_valido;		
		}	
	}

	class Personas_Registradas{
		//Propiedades
		public $id_usuario = false;
		public $ap_Paterno = false;
		public $ap_Materno = false;
		public $nombre = false;
		public $ano_nacimiento = false;
		public $correo_electronico = false;
		public $telefono = false;
		public $organizacion = false;
		public $colonia = false;
		
		//Metodos	
		function Get_id_usuario(){
		if ( $this->id_usuario !== false) return $this->id_usuario;
		}
		
		function Get_ap_Paterno(){
		if ( $this->ap_Paterno !== false) return $this->ap_Paterno;
		}
		
		function Get_ap_Materno(){
		if ( $this->ap_Materno !== false) return $this->ap_Materno;
		}
		
		function Get_nombre(){
		if ( $this->nombre !== false) return $this->nombre;
		}
		
		function Get_ano_nacimiento(){
		if ( $this->ano_nacimiento !== false) return $this->ano_nacimiento;
		}
		
		function Get_correo_electronico(){
		if ( $this->correo_electronico !== false) return $this->correo_electronico;
		}
		
		function Get_telefono(){
		if ( $this->telefono !== false) return $this->telefono;
		}
		
		function Get_organizacion(){
		if ( $this->organizacion !== false) return $this->organizacion;
		}

		function Get_colonia(){
		if ( $this->colonia !== false) return $this->colonia;
		}	
	}

	class Visitas{
		//Propiedades
		public $no_de_visita = false;
		public $correo_telefono = false;
		public $fecha_solicito_visita = false;
		public $descripcion = false;
		
		//Metodos	
		function Get_no_de_visita(){
		if ( $this->no_de_visita !== false) return $this->no_de_visita;
		}	

		function Get_correo_telefono(){
		if ( $this->correo_telefono !== false) return $this->correo_telefono;
		}

		function Get_fecha_solicito_visita(){
		if ( $this->fecha_solicito_visita !== false) return $this->fecha_solicito_visita;
		}

		function Get_descripcion(){
		if ( $this->descripcion !== false) return $this->descripcion;
		}		
	}	
	
	class Requisitos{
		//Propiedades
		public $id_requisito = false;
		public $idCurso = false;
		public $requisito = false;
		public $isActive = false;
		
		//Metodos	
		function Get_id_requisito(){
		if ( $this->id_requisito !== false) return $this->id_requisito;
		}
		
		function Get_idCurso(){
		if ( $this->idCurso !== false) return $this->idCurso;
		}
		
		function Get_requisito(){
		if ( $this->requisito !== false) return $this->requisito;
		}	
		
		function Get_isActive(){
		if ( $this->isActive !== false) return $this->isActive;
		}		
	
	}	
	
	class asistentes_evento{
		//Propiedades
		public $id_usuario = false;
		public $id_evento = false;
		public $fecha_inscrito = false;
		public $apellido_paterno = false;
		public $apellido_materno = false;
		public $nombre = false;
		public $nombre_evento = false;
		public $correo_electronico = false;
		
		//Metodos	
		function Get_id_usuario(){
		if ( $this->id_usuario !== false) return $this->id_usuario;
		}
		
		function Get_id_evento(){
		if ( $this->id_evento !== false) return $this->id_evento;
		}
		
		function Get_fecha_inscrito(){
		if ( $this->fecha_inscrito !== false) return $this->fecha_inscrito;
		}	
		
		function Get_apellido_paterno(){
		if ( $this->apellido_paterno !== false) return $this->apellido_paterno;
		}		
	
		function Get_apellido_materno(){
		if ( $this->apellido_materno !== false) return $this->apellido_materno;
		}
		
		function Get_nombre(){
		if ( $this->nombre !== false) return $this->nombre;
		}
		
		function Get_nombre_evento(){
		if ( $this->nombre_evento !== false) return $this->nombre_evento;
		}		

		function Get_correo_electronico(){
		if ( $this->correo_electronico !== false) return $this->correo_electronico;
		}		
	}	

	class sobre_nosotros{
		//Propiedades
		public $id_sb = false;	
		public $titulo_sn = false;	
		public $subtitulo_sn = false;	
		public $parrafo1_sn = false;	
		public $parrafo2_sn = false;	
		public $punto1_sn = false;	
		public $punto2_sn = false;	
		public $punto3_sn = false;	
		public $parrafo3_sn = false;	
		public $img_sn = false;	
		
		//Metodos	
		function Get_id_sb(){
		if ( $this->id_sb !== false) return $this->id_sb;
		}

		function Get_titulo_sn(){
		if ( $this->titulo_sn !== false) return $this->titulo_sn;
		}

		function Get_subtitulo_sn(){
		if ( $this->subtitulo_sn !== false) return $this->subtitulo_sn;
		}

		function Get_parrafo1_sn(){
		if ( $this->parrafo1_sn !== false) return $this->parrafo1_sn;
		}

		function Get_parrafo2_sn(){
		if ( $this->parrafo2_sn !== false) return $this->parrafo2_sn;
		}

		function Get_punto1_sn(){
		if ( $this->punto1_sn !== false) return $this->punto1_sn;
		}

		function Get_punto2_sn(){
		if ( $this->punto2_sn !== false) return $this->punto2_sn;
		}

		function Get_punto3_sn(){
		if ( $this->punto3_sn !== false) return $this->punto3_sn;
		}

		function Get_parrafo3_sn(){
		if ( $this->parrafo3_sn !== false) return $this->parrafo3_sn;
		}

		function Get_img_sn(){
		if ( $this->img_sn !== false) return $this->img_sn;
		}		
	}		
?>