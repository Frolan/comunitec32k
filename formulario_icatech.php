<?php  
require_once "conexion.php";
	session_start();
		
	$_SESSION['registro'] = false;	

	$nombre_curso = $_GET['Nombre_curso'];
	
	$qry = 'SELECT * FROM comunitec32k_cursos WHERE nombre_curso = :curso';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':curso' => $nombre_curso)
	);
	
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
		$idCurso = htmlentities($row['idCurso']);
		$sub_titulo_curso = htmlentities($row['sub_titulo_curso']);
		$descripcion_curso = htmlentities($row['descripcion_curso']);
		$duracion_curso = htmlentities($row['duracion_curso']);
		$inicio_curso = htmlentities($row['inicio_curso']);
		$cierre_curso = htmlentities($row['cierre_curso']);
		
		$hora_llegada_h = htmlentities($row['hora_llegada_h']);
		$hora_llegada_p = htmlentities($row['hora_llegada_p']);
		$hora_salida_h = htmlentities($row['hora_salida_h']);
		$hora_salida_p = htmlentities($row['hora_salida_p']);		
		$dias_c = htmlentities($row['dias_c']);		
		
		$nivel_curso = htmlentities($row['nivel_curso']);
		$organizacion = htmlentities($row['organizacion']);
		$certificacion = htmlentities($row['certificacion']);
		
		$pago_unico_curso = htmlentities($row['pago_unico_curso']);
		$pago_semanal_curso = htmlentities($row['pago_semanal_curso']);
		$tendra_costo_ins = htmlentities($row['tendra_costo_ins']);
		$pago_inscripcion_curso = htmlentities($row['pago_inscripcion_curso']);		
		
		$Imagen = htmlentities($row['Imagen']);
		$IsActive = htmlentities($row['IsActive']);
	
	if($Imagen == ""){
		$Imagen = "assets/img/comunitec32k-logo.png";
	}

if(isset($_POST['btnRegUsuario'])){
		try{
			$fecha_actual = htmlentities($_POST['fecha_actual']);	
			$nombre_curso = htmlentities($_POST['nombre_curso']);	
			$fecha_inicio = htmlentities($_POST['fecha_inicio']);	
			$fecha_termino = htmlentities($_POST['fecha_termino']);	
			
			$apellido_paterno = htmlentities($_POST['apellido_paterno']);	
			$apellido_materno = htmlentities($_POST['apellido_materno']);
			$nombre = htmlentities($_POST['nombre']);
			$edad = htmlentities($_POST['edad']);
			$rfc = htmlentities($_POST['rfc']);
			
			$curp = htmlentities($_POST['curp']);
			$domicilio = htmlentities($_POST['domicilio']);
			$colonia = htmlentities($_POST['colonia']);
			$ciudad = htmlentities($_POST['ciudad']);
			$estado = htmlentities($_POST['estado']);
			$telefono = htmlentities($_POST['telefono']);
			$telefono_trabajo = htmlentities($_POST['telefono_trabajo']);
			$correo_electronico = htmlentities($_POST['correo_electronico']);
			$fecha_nacimiento = htmlentities($_POST['fecha_nacimiento']);
			$lugar_nacimiento = htmlentities($_POST['lugar_nacimiento']);
			
			$sexo = htmlentities($_POST['sexo']);
			$estado_civil = htmlentities($_POST['estado_civil']);
			$discapacidad_presenta = htmlentities($_POST['discapacidad_presenta']);
			$grado_estudios = htmlentities($_POST['grado_estudios']);
			$condicion_social = htmlentities($_POST['condicion_social']);
			$nombre_empresa = htmlentities($_POST['nombre_empresa']);
			$antiguedad = htmlentities($_POST['antiguedad']);
			$direccion_empresa = htmlentities($_POST['direccion_empresa']);
			$ocupacion_principal = htmlentities($_POST['ocupacion_principal']);
			$situacion_actual = htmlentities($_POST['situacion_actual']);
			
			$medio_enterado = htmlentities($_POST['medio_enterado']);
			$razon_eleccion = htmlentities($_POST['razon_eleccion']);
			$razon_registro = htmlentities($_POST['razon_registro']);
			$explicacion_pago = htmlentities($_POST['explicacion_pago']);

			//Procesar datos del check box
			$areas_capacitacion = '';
			if(isset($_POST['areas_capacitacion'])){
				$areas_capacitacion = implode(' ' , $_POST['areas_capacitacion']);
			}

			$agregar_persona = "INSERT INTO icatech_tbl_usuarios(fecha_actual, nombre_curso, fecha_inicio, fecha_termino, apellido_paterno, apellido_materno, nombre, edad, rfc, curp, domicilio, colonia, ciudad, estado, telefono, telefono_trabajo, correo_electronico, fecha_nacimiento, lugar_nacimiento, sexo, estado_civil, discapacidad_presenta, grado_estudios, condicion_social, nombre_empresa, antiguedad, direccion_empresa, ocupacion_principal, situacion_actual, medio_enterado, razon_eleccion, razon_registro, explicacion_pago, areas_capacitacion) 
								VALUES (:fecha_a, :nombre_c, :fecha_i, :fecha_t, :apellido_p, :apellido_m, :nombre, :edad, :rfc, :curp, :domicilio, :colonia, :ciudad, :estado, :telefono, :telefono_t, :correo_e, :fecha_n, :lugar_n, :sexo, :estado_c, :discapacidad_p, :grado_e, :condicion_s, :nombre_e, :antiguedad, :direccion_e, :ocupacion_p, :situacion_a, :medio_e, :razon_e, :razon_r, :explicacion_p, :areas_c)";
			$persona = $link->prepare($agregar_persona);
			$persona->execute(array(
					':fecha_a' => $fecha_actual,
					':nombre_c' => $nombre_curso,
					':fecha_i' => $fecha_inicio,
					':fecha_t' => $fecha_termino,
					':apellido_p' => $apellido_paterno,
					':apellido_m' => $apellido_materno,
					':nombre' => $nombre,
					':edad' => $edad,
					':rfc' => $rfc,
					':curp' => $curp,
					':domicilio' => $domicilio,
					':colonia' => $colonia,
					':ciudad' => $ciudad,
					':estado' => $estado,
					':telefono' => $telefono,
					':telefono_t' => $telefono_trabajo,
					':correo_e' => $correo_electronico,
					':fecha_n' => $fecha_nacimiento,
					':lugar_n' => $lugar_nacimiento,
					':sexo' => $sexo,
					':estado_c' => $estado_civil,
					':discapacidad_p' => $discapacidad_presenta,
					':grado_e' => $grado_estudios,
					':condicion_s' => $condicion_social,
					':nombre_e' => $nombre_empresa,
					':antiguedad' => $antiguedad,
					':direccion_e' => $direccion_empresa,
					':ocupacion_p' => $ocupacion_principal,
					':situacion_a' => $situacion_actual,
					':medio_e' => $medio_enterado,
					':razon_e' => $razon_eleccion,
					':razon_r' => $razon_registro,
					':explicacion_p' => $explicacion_pago,
					':areas_c' => $areas_capacitacion
					)
				);
				
			$subject = "Inscripcion a curso";
			$message = "El usuario ".$nombre.' '.$apellido_paterno.' '.$apellido_materno.' se a registrado al curso '.$nombre_curso."";			
			enviarEmail($subject, $message, $correo_electronico); 
			
			$_SESSION['registro'] = "Ha sido registrado con exito";	
				header('Location: index.php');
				return;
			
		}catch(Exception $ex){
			echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
			echo '<h3> Error : '.$ex->getMessage().'</h3>';
			return;
		}
	}

	//========Funciones========// 	
	function enviarEmail($subject, $message, $correo_electronico){
		
		$sender = "felipe.galan@comunitec32k.com";
		$email = $sender;
		$name2send = "Comunitec32k";
		///$mailto = $email.",".$sender;
		$mailto = $sender.",".$correo_electronico;
		///$mailto = $ceo;
		
		$from="From: $name2send<$email>\r\nReturn-path: $sender";
		///$subject=
		///$message=
			
		mail($mailto, $subject, $message, $from);	
	}	
?>
<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Registro ICATECH</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
<br>
	<div class="container">
		<h1><center>¿Primera vez que nos visitas? Favor de registrarse a ICATECH:<center></h1>
		
		<p>Al completar este formulario recibirá un correo para darle seguimiento a su inscripción (puede que no sea inmediatamente o que llegue como correo no deseado)</p>
		
		<form method="POST">
		
			<div class="form-group">
				<label>Fecha de hoy:</label>
				<input type="date" name="fecha_actual" id="fecha_actual" class="form-control" required />
			</div>
			<br>
			
			<h2><center>DATOS GENERALES DEL CURSO</center></h2>

			<div class="form-group">
				<label hidden >Nombre del curso:</label>
				<input type="text" name="nombre_curso" id="nombre_curso" class="form-control" value="<?php echo $nombre_curso?>" hidden />
			</div>

			<div class="form-group">
				<label hidden >Fecha de inicio:</label>
				<input type="date" name="fecha_inicio" id="fecha_inicio" class="form-control" value="<?php echo $inicio_curso?>" hidden />
			</div>

			<div class="form-group">
				<label hidden >Fecha de termino:</label>
				<input type="date" name="fecha_termino" id="fecha_termino" class="form-control" value="<?php echo $cierre_curso?>" hidden />
			</div>
 
			<label><b>Curso:</b></label>						<?php echo $nombre_curso ?>			
				<p><h4>Duración</h4>
				<?php echo $duracion_curso ?><br>
						<br>
				<label><b>Fecha de inicio:</b></label>
				<?php echo $inicio_curso ?>
						<br>
				<label><b>Fecha de cierre:</b></label>
				<?php echo $cierre_curso ?></p>
	
			<p><h4>Horario</h4>
				<label><b>Dias del curso :</b></label>
				<?php echo $dias_c ?>
						<br>
				<label><b>Hora de curso:</b></label>
				<?php echo 'De '.$hora_llegada_h .' '.$hora_llegada_p.' a '.$hora_salida_h .' '.$hora_salida_p  ?>
	

			<h3><center>DATOS DEL PARTICIPANTE</center></h3>

			<div class="form-group">
				<label>Apellido paterno:</label>
				<input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Apellido materno:</label>
				<input type="text" name="apellido_materno" id="apellido_materno" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Nombre(s):</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Edad:</label>
				<input type="number" name="edad" id="edad" class="form-control" required />
			</div>

			<div class="form-group">
				<label>RFC:</label>
				<input type="text" name="rfc" id="rfc" class="form-control" />
			</div>

			<div class="form-group">
				<label>CURP: ¿No conoces tu CURP? <a href="https://www.gob.mx/curp/" target="_blank"> Buscala aqui</a></label> </label>
				<input type="text" name="curp" id="curp" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Domicilio particular (calle y numero):</label>
				<input type="text" name="domicilio" id="domicilio" class="form-control" placeholder="Ej. Plan de ayala 1234" required />
			</div>

			<div class="form-group">
				<label>Colonia:</label>
				<input type="text" name="colonia" id="colonia" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Ciudad en que reside:</label>
				<input type="text" name="ciudad" id="ciudad" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Estado: en que reside</label>
				<input type="text" name="estado" id="estado" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Telefono casa/celular:</label>
				<input type="text" name="telefono" id="telefono" class="form-control" />
			</div>

			<div class="form-group">
				<label>Telefono trabajo:</label>
				<input type="text" name="telefono_trabajo" id="telefono_trabajo" class="form-control" />
			</div>

			<div class="form-group">
				<label>Correo electronico:</label>
				<input type="email" name="correo_electronico" id="correo_electronico" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Fecha de nacimiento:</label>
				<input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Lugar de nacimiento: (Pais)</label>
				<input type="text" name="lugar_nacimiento" id="lugar_nacimiento" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Sexo:</label>
				<select type="select" name="sexo" id="sexo" class="form-control" required />
					<option value="Hombre">Hombre</option>
					<option value="Mujer">Mujer</option>
				</select>
			</div>

			<div class="form-group">
				<label>Estado civil:</label>
				<select type="select" name="estado_civil" id="estado_civil" class="form-control" required />
					<option value="Soltero">Soltero</option>
					<option value="Casado">Casado</option>
					<option value="Viudo">Viudo</option>
					<option value="Divorciado">Divorciado</option>
					<option value="Union libre">Union libre</option>
					<option value="Separado">Separado</option>
				</select>
			</div>

			<div class="form-group">
				<label>Discapacidad que presenta:</label>
				<select type="select" name="discapacidad_presenta" id="discapacidad_presenta" class="form-control" required />
					<option value="Visual">Visual</option>
					<option value="Auditiva">Auditiva</option>
					<option value="De lenuguaje">De lenguaje</option>
					<option value="Motriz musculoesqueletica">Motriz o musculoesqueletica</option>
					<option value="Mental">Mental</option>
					<option selected="" value="Ninguna">Ninguna</option>
				</select>
			</div>	

			<div class="form-group">
				<label>Grado maximo de estudios:</label>
				<select type="select" name="grado_estudios" id="grado_estudios" class="form-control" required />
					<option value="Sin escolaridad">Sin escolaridad</option>
					<option value="Primaria inconclusa">Primaria inconclusa</option>
					<option value="Primaria terminada">Primaria terminada</option>
					<option value="Secundaria inconclusa">Secundaria inconclusa</option>
					<option value="Secundaria terminada">Secunaria terminada</option>
					<option value="Preparatoria inconclusa">Preparatoria inconclusa</option>
					<option value="Preparatoria terminada">Preparatoria terminada</option>
					<option value="Licenciatura inconclusa">Licenciatura inconclusa</option>
					<option value="Licenciatura terminada">Licenciatura terminada</option>
					<option value="Posgrado">Posgrado</option>
				</select>
			</div>
				
			<div class="form-group">
				<label>Condicion social: (Revise las opciones que se despliegan y seleccione la que más lo describa )</label>
				<select type="select" name="condicion_social" id="condicion_social" class="form-control" required />
					<option value="Ninguna">Ninguna</option>
					<option value="Tercera edad">Tercera edad</option>
					<option value="Ceresos">CERESOS</option>
					<option value="Adolscente cc">Adolescente CC</option>
					<option value="Indigena">Indigena</option>
					<option value="Jefa de familia">Jefa de familia</option>
					<option value="Migrante">Migrante</option>
				</select>
			</div>
			<br>
			
			<h4><center>¿ACTUALMENTE TIENE TRABAJO?</center></h4>

			<div class="form-group">
				<label>Nombre de la empresa:</label>
				<input type="text" name="nombre_empresa" id="nombre_empresa" class="form-control" />
			</div>

			<div class="form-group">
				<label>Antigüedad: (Años / Meses)</label>
				<input type="text" name="antiguedad" id="antiguedad" class="form-control" placeholder="Ej. 1 año 3 meses" />
			</div>

			<div class="form-group">
				<label>Direccion</label>
				<input type="text" name="direccion_empresa" id="direccion_empresa" class="form-control" placeholder="Ej. Juárez-porvenir 1580, Bermúdez, Cd Juárez, Chih. "/>
			</div>

			<div class="form-group">
				<label>Ocupacion principal:</label>
				<select type="select" name="ocupacion_principal" id="ocupacion_principal" class="form-control"  />
					<option value="Ninguna">Ninguna</option>
					<option value="Profesionistas">Profesionistas</option>
					<option value="Tecnicos">Tecnicos</option>
					<option value="Trabajadores de la eduacion">Trabajadores de la educacion</option>
					<option value="Trabajadores del arte">Trabajadores del arte</option>
					<option value="Funcionarios y directivos">Funcionarios y directivos</option>
					<option value="Trabajadores agropecuarios">Trabajadores agrupecuarios</option>
					<option value="Inspectores y supervisores">Inspectores y supervisores</option>
					<option value="Artesanos y obreros">Artesanos y obreros</option>
					<option value="Operador de maquina industrial">Operador de maquina industrial</option>
					<option value="Ayudante y similares">Ayudante y similares</option>
					<option value="Operadores de transporte">Operadores de transporte</option>
					<option value="Oficinistas">Oficinistas</option>
					<option value="Comerciantes y dependientes">Comerciantes y dependientes</option>
					<option value="Trabajadores ambulantes">Trabajadores ambulantes</option>
					<option value="Trabajadores en servicios publicos">Trabajadores en servicios publicos</option>
					<option value="Trabajadores en servicios domesticos">Trabajadores en servicios domesticos</option>
					<option value="Proteccion y vigilancia">Proteccion y vigilancia</option>
					<option value="Ama de casa">Ama de casa</option>
					<option value="Otro">Otro</option>
				</select>
			</div>

			<div class="form-group">
				<label>Si no trabaja; ¿Cual es su situacion actual?: (Seleccione la que más lo describa)</label>
				<select type="select" name="situacion_actual" id="situacion_actual" class="form-control" required />
					<option value="Estudiante">Estudiante</option>
					<option value="Se dedica al hogar">Se dedica al hogar</option>
					<option value="Jubilado o pensionado">Jubilado o pensionado</option>
					<option value="Empleado">Empleado</option>
					<option value="Negocio propio">Negocio propio</option>
					<option value="Otro">Otro</option>
				</select>
			</div>
			
	<h4><center>Otros datos</center></h4>
	
			<div class="form-group">
				<label>¿Por que medio se entero del curso?:</label>
				<select type="select" name="medio_enterado" id="medio_enterado" class="form-control" required />
					<option value="Folletos tripticos carteles y volantes">Folletos, trípticos, carteles y volantes</option>
					<option value="Prensa">Prensa</option>
					<option value="Amigos familiares">Amigos/familiares</option>
					<option value="Radio">Radio</option>
					<option value="Reinscripcion">Reinscripcion</option>
					<option value="Internet">Internet</option>
					<option value="Eventos">Eventos</option>
					<option value="Otro">Otro</option>
				</select>
			</div>

			<div class="form-group">
				<label>¿Por qué razón eligió el ICATECH?:</label>
				<select type="select" name="razon_eleccion" id="razon_eleccion" class="form-control" required />
					<option value="Ubicacion">Ubicación</option>	
					<option value="Precio">Precio</option>
					<option value="Horarios">Horarios</option>
					<option value="Servicio">Servicio</option>
					<option value="Calidad_del_curso">Calidad del curso</option>
					<option value="Instalaciones">Instalaciones</option>
				</select>
			</div>

			<div class="form-group">
				<label>¿Por qué razón desea tomar este curso?:</label>
				<select type="select" name="razon_registro" id="razon_registro" class="form-control" required />
					<option value="Emplearse">Emplearse</option>
					<option value="Auto emplearse">Auto emplearse</option>
					<option value="Disposicion de tiempo libre">Disposicion de tiempo libre</option>
					<option value="Mejorar situacion en empleo actual">Mejorar situacion en empleo actual</option>
					<option value="Ahorrar gastos al ingreso familiar">Ahorrar gastos al ingreso familiar</option>
					<option value="Superacion personal">Superacion personal</option>
					<option value="Otra">Otra</option>
				</select>
			</div>

			<div class="form-group">
				<label>¿Quién pagará el curso? (Liquidación):</label>
				<select type="select" name="explicacion_pago" id="explicacion_pago" class="form-control" required />
					<option value="Participante">Participante</option>
					<option value="Empresa">Empresa</option>
					<option value="Sector publico">Sector Público</option>
					<option value="Plantel">Plantel(beca)</option>
					<option value="Otro">Otro</option>
				</select>
			</div>

			<div class="form-group">
				<label>Seleccione otras áreas dónde desee capacitarse:</label>
							
				<input type="checkbox" id="Administracion" name="areas_capacitacion[]" value="Administracion">
				<label for="Administracion"> Administracion</label>
				 <br>
				<input type="checkbox" id="Agropecuario" name="areas_capacitacion[]" value="Agropecuario">
				<label for="Agropecuario"> Agropecuario</label>
				 <br>
				<input type="checkbox" id="Artesanal" name="areas_capacitacion[]" value="Artesanal">
				<label for="Artesanal"> Artesanal</label>
				 <br>
				<input type="checkbox" id="Asistencia_social" name="areas_capacitacion[]" value="Asistencia social">
				<label for="Asistencia_social"> Asistencia social</label>
				 <br>
				<input type="checkbox" id="Automotor" name="areas_capacitacion[]" value="Automotor">
				<label for="Automotor"> Automotor</label>
				 <br>
				<input type="checkbox" id="Comunicacion" name="areas_capacitacion[]" value="Comunicacion">
				<label for="Comunicacion"> Comunicacion</label>
				 <br>
				<input type="checkbox" id="Construccion" name="areas_capacitacion[]" value="Construccion">
				<label for="Construccion"> Construccion</label>
				 <br>
				<input type="checkbox" id="Educacion" name="areas_capacitacion[]" value="Educacion">
				<label for="Educacion"> Educacion</label>
				 <br>
				<input type="checkbox" id="Electricidad" name="areas_capacitacion[]" value="Electricidad">
				<label for="Electricidad"> Electricidad</label>
				 <br>
				<input type="checkbox" id="Electronica" name="areas_capacitacion[]" value="Electronica">
				<label for="Electronica"> Electronica</label>
				 <br>
				<input type="checkbox" id="Equipos_y_sistemas" name="areas_capacitacion[]" value="Equipos y sistemas">
				<label for="Equipos_y_sistemas"> Equipos y sistemas</label>
				 <br>
				<input type="checkbox" id="Imagen_y_bienestar_personal" name="areas_capacitacion[]" value="Imagen y bienestar personal">
				<label for="Imagen_y_bienestar_personal"> Imagen y bienestar personal</label>
				 <br>
				<input type="checkbox" id="Industrial" name="areas_capacitacion[]" value="Industrial">
				<label for="Industrial"> Industrial</label>
				 <br>
				<input type="checkbox" id="Mecatronica" name="areas_capacitacion[]" value="Mecatronica">
				<label for="Mecatronica"> Mecatronica</label>
				 <br>
				<input type="checkbox" id="Medio_ambiente" name="areas_capacitacion[]" value="Medio ambiente">
				<label for="Medio_ambiente"> Medio ambiente</label>
				 <br>
				<input type="checkbox" id="Metal_mecanica" name="areas_capacitacion[]" value="Metal_mecanica">
				<label for="Metal_mecanica"> Metal_mecanica</label>
				 <br>
				<input type="checkbox" id="Plasticos" name="areas_capacitacion[]" value="Plasticos">
				<label for="Plasticos"> Plasticos</label>
				 <br>
				<input type="checkbox" id="Procesos_de_produccion_industrial" name="areas_capacitacion[]" value="Procesos de produccion industrial">
				<label for="Procesos_de_produccion_industrial"> Procesos de produccion industrial</label>
				 <br>
				<input type="checkbox" id="Sistemas_de_impresion" name="areas_capacitacion[]" value="Sistemas de impresion">
				<label for="Sistemas_de_impresion"> Sistemas de impresion</label>
				 <br>
				<input type="checkbox" id="Tecnicas_de_la_informacion" name="areas_capacitacion[]" value="Tecnicas de la informacion">
				<label for="Tecnicas_de_la_informacion"> Tecnicas de la informacion</label>
				 <br>
				<input type="checkbox" id="Turismo" name="areas_capacitacion[]" value="Turismo">
				<label for="Turismo"> Turismo</label>
				 <br>
				<input type="checkbox" id="Vestido_y_textil" name="areas_capacitacion[]" value="Vestido y textil">
				<label for="Vestido_y_textil"> Vestido y textil</label>
				 <br>
				<input type="checkbox" id="Otra" name="areas_capacitacion[]" value="Otra">
				<label for="Otra"> Otra</label>
				 <br>
				 
				 
				<!-- <select type="select" name="areas_capacitacion" id="areas_capacitacion" class="form-control" required />
					<option value="Administracion">Administración</option>
					<option value="Agropecuario">Agropecuario</option>
					<option value="Artesanal">Artesanal</option>
					<option value="Asistencia_social">Asistencia social</option>
					<option value="Automotor">Automotor</option>
					<option value="Comunicacion">Comunicación</option>
					<option value="Construccion">Construcción</option>
					<option value="Educacion">Educación</option>
					<option value="Electricidad">Electricidad</option>
					<option value="Electronica">Electrónica</option>
					<option value="Equipos_y_sistemas">Equipos y sistemas</option>
					<option value="Imagen_y_bienestar_personal">Imagen y bienestar personal</option>
					<option value="Industrial">Industrial</option>
					<option value="Mecatronica">Mecatrónica</option>
					<option value="Medio_ambiente">Medio ambiente</option>
					<option value="Metal_mecanica">Metal Mecánica</option>
					<option value="Plasticos">Plásticos</option>
					<option value="Procesos_de_produccion_industrial">Procesos de Producción Industrial</option>
					<option value="Sistemas_de_impresion">Sistemas de Impresión</option>
					<option value="Tecnicas_de_la_informacion">Técnicas de la Información</option>
					<option value="Turismo">Turismo</option>
					<option value="Vestido_y_textil">Vestido y textil</option>
					<option value="Otra">Otra</option>
				</select> -->
			</div>

			<div class="form-group">
				<input type="checkbox" name="checkbox" value="check" id="agree" required />
				He leido la <a href="politicasprivacidad.html">politica de privacidad</a> de la comunidad tecnologica del centro 

			</div>	
			
			<div class="form-group">
				<input type="submit" class="btnAgregar" value="Registrarse" name="btnRegUsuario">
			</div>
		</form>
	</div>	
	<br>
	<footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
</body>
</html>