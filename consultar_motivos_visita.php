<?php

	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();	
	
	$_SESSION['success'] = false;
	
	//Istanciacion de clases 
	$id_motivos_de_visita = new Motivos_Visitas();
	$descripcion = new Motivos_Visitas();
	$campo_valido = new Motivos_Visitas();

	//DB connection
	$conectado = 1;
	
	//statement
	$m_visita = "SELECT * FROM comunitec_tbl_motivos_de_visita ";
	$stmt_m = $link->query($m_visita); 
	$cant_reg = 0;
	while($row = $stmt_m->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_motivos_de_visita->id_motivos_de_visita = htmlentities($row['id_motivos_de_visita']);
		$descripcion->descripcion = htmlentities($row['descripcion']);
		$campo_valido->campo_valido = htmlentities($row['campo_valido']);

		$arrId_motivos_de_visita[] = ($id_motivos_de_visita->id_motivos_de_visita);
		$arrDescripcion[] = ($descripcion->descripcion);
		$arrCampo_valido[] = ($campo_valido->campo_valido);
		
		$cant_reg++;	
	}	
	
	//Agregar motivos de visita 	
	if(isset($_POST['nuevo_motivo'])){
		try{
			$motivo_visita = htmlentities(trim($_POST['descripcion']));
			$activo = 1;
			
			$qry_motivos = "INSERT INTO comunitec_tbl_motivos_de_visita 
							(descripcion,campo_valido)
							VALUES (:motivo_visita, :activo)
							";
			$stmt_motivos = $link->prepare($qry_motivos);
			$stmt_motivos->execute(array(
						':motivo_visita' => $motivo_visita,
						':activo' => $activo
						)
					);
			$_SESSION['success'] = "Motivo agregado con exito";
			
		}catch(Exception $ex){
			echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
				echo '<h3> Error : '.$ex->getMessage().'</h3>';
				return;
			
		}
	}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Motivos Visita Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
<br>
<!--Tabla de motivos activos -->
	<div class="container">
		<h1><center>Consulta de Motivos Visita</center></h1>
<?php
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
			
			//print_r($_SESSION); Descomentar en caso de revision
?>		
	
		<label>Agregar un motivo de visita :</label>
		<form method="POST">
			<input type="text" name="descripcion" class="form-control" placeholder="Motivo visita" required ><br>
			<input type="submit" name="nuevo_motivo" value="Agregar">
		</form>

		<br>
		<br>
	<table>
		<thead>
			<tr>
				<th scope="col" hidden >ID Motivo</th>
				<th scope="col">Descripción</th>
				<th scope="col">Activo</th>
				<th scope="col">Actualizar</th>
			</tr>
		</thead>
			
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrDescripcion as $descripcion){
					$id = $arrId_motivos_de_visita[$rowCount];
					$activo = $arrCampo_valido[$rowCount];
		
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id.'</th>';
						echo '<td>'.$descripcion.'</td>';
						echo '<td>'.$activo.'</td>';
						echo	"<td>
									<a href='modificar_motivos_visita.php?id=".$id."'>
										<button>Actualizar</button>
									</a>
								</td>";
					echo '</tr>';			
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay motivos registrados</p>';
				echo '</div>';	
			}
		?>
	</table>
	</div>
	  <br>
	<div class = "container">
		<a href= "main.php"><button type="button">Regresar</button></a>
	</div>	
<br>	


  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>