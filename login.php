<?php
require_once 'conexion.php';
session_start();

UNSET($_SESSION['entrada']);
UNSET($_SESSION['nombre_curso']);

if(isset($_SESSION['error'] )){
	$failure = $_SESSION['error'];
}

if(isset($_POST['acceder'])){	
	$correo = trim($_POST['email']);
	$contrasena_entrante = trim($_POST['contrasena_entrante']);
			
	$qry = "SELECT * FROM comunitec_tbl_instructores WHERE correo_ins = :correo LIMIT 1";
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
			':correo' => $correo,
			)
		);
				
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	$contrasena = $row['contrasena'];
			
		if (password_verify($contrasena_entrante, $contrasena)){
			///$_SESSION['login'] = "success!";
	
			if($row['cambiocontrasena'] == 1){ 						
				$id = $row['id_instructor'];						
				die("<script>location.href = 'cambio_contrasena.php?id=".$id."'</script>");
			}
			else{
				$_SESSION['entrada'] = 1;
						
				$_SESSION['id_instructor'] = htmlentities($row['id_instructor']);
				$_SESSION['nombre_ins'] = htmlentities($row['nombre_ins']);
				$_SESSION['apellido_pa_ins'] = htmlentities($row['apellido_pa_ins']);
				$_SESSION['apellido_ma_ins'] = htmlentities($row['apellido_ma_ins']);
				$_SESSION['correo_ins'] = htmlentities($row['correo_ins']);
				$_SESSION['CURP'] = htmlentities($row['CURP']);
				$_SESSION['contrasena'] = htmlentities($row['contrasena']);
				$_SESSION['cambiocontrasena'] = htmlentities($row['cambiocontrasena']);
				$_SESSION['id_tipo_usuario'] = htmlentities($row['id_tipo_usuario']);
				$_SESSION['isActive'] = htmlentities($row['isActive']);				
			
				if($_SESSION['id_tipo_usuario'] == 1){
					header('Location: main.php');
					return;	
				}
				
				else if($_SESSION['id_tipo_usuario'] == 2){
					header('Location: instructores.php');
					return;
				
				}			
			}
	
		}
		else{
			///$_SESSION['login'] = "fail!";
			$_SESSION['error'] = "El usuario o contraseña estan mal";
			usleep(500000);///0.5s
		}			
}
?>
<html>
<head>

  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Login Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  
  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  
</head>
<body>

<!-- ======= Header ======= -->
  <header id="header" class="header-inner-pages">

    <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
          <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <h1 class="text-light"><a href="index.php" class="scrollto"><span>Comunitec32k</span></a></h1>
        <!-- <a href="index.html" class="scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a> -->
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li><a href="index.php #services">Cursos</a></li>
          <li><a href="index.php #services">Eventos</a></li>
		  <li><a href="Registro.php">Resgístrarse</a></li>
		  <li><a href="login.php">Staff</a></li>
        </ul>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header  -->

    <div class="container">
				<br>
				<br>
				<br>
				<br>
				<h1>Inicio de sesión</h1>
				<?php
					if( isset($_SESSION['success']) ){
						echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
						unset($_SESSION['success']);
					}
					if( isset($_SESSION['error']) ){
						echo '<p style="color:red;">'.htmlentities($_SESSION['error']).'</p>';
						unset($_SESSION['error']);
					}
				?>
              <form method="POST" role="form" class="php-email-form">
			  
			    <div class="form-group">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required data-rule="email" data-msg="Porfavor, ingresa un correo valido" />
                  <div class="validate"></div>
                </div>
				
                <div class="form-group">
                  <input type="password" name="contrasena_entrante" class="form-control" id="name" placeholder="Contraseña" required />
                  <div class="validate"></div>
                </div>
				
                <div class="text-center">
					<input type="submit" name="acceder" Value="Acceder">
					<input type="reset" name="reset" Value="Restablecer" class="btn_submit" >
				</div>
              </form>
			  
			  
	</div>	
	
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
 
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
  
</body>
</html>