<?php
require_once "conexion.php";
	session_start();
	
	$_SESSION['error'] = false;
	
	$id_instructor = $_GET['id'];
	
	$qry = 'SELECT * FROM comunitec_tbl_instructores WHERE id_instructor = :id_instructor';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id_instructor' => $id_instructor
		)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);

	$contrasena_bd = htmlentities($row['contrasena']);
	 
	if(isset($_POST['actualizar_contrasena'])){
		$n_id_instructor = htmlentities(trim($_POST['id_instructor']));
		$contrasena_defecto = htmlentities(trim($_POST['contrasena_df']));
		$contrasena_nueva = htmlentities(trim($_POST['contrasena_nv']));
		$verificar_contrasena = htmlentities(trim($_POST['vr_contrasena']));
		$cambio_c = 0;
		
		//cifrar contrasena
		$contrasena_c = password_hash($contrasena_nueva, PASSWORD_DEFAULT);
			
		if(password_verify($contrasena_defecto, $contrasena_bd)){
			if($contrasena_nueva == $verificar_contrasena){
						
				$update = 'UPDATE comunitec_tbl_instructores SET 															
					contrasena = :contra_n,
					cambiocontrasena = :cambio				
				WHERE 
					id_instructor = :id';
					
				$stmt = $link->prepare($update);
				$stmt->execute(array(
					':contra_n' => $contrasena_c,
					':cambio' => $cambio_c,
					':id' => $n_id_instructor,
					)				
				);
				
				header('Location: login.php');
				return; 
				
			}else{
				$_SESSION['error'] = "Su nueva contraseña no coincide";
			}
		}else{
			$_SESSION['error'] = "La contraseña definida no coincide";
		}
				
	}	
	
	//DB connection
	$conectado = 1;
	

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Cambio de contraseña</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

	<br>
	<center><h1>Cambio de Contraseña</h1></center>
		<?php
			if( isset($_SESSION['error']) ){
				echo '<p style="color:red;">'.htmlentities($_SESSION['error']).'</p>';
				unset($_SESSION['error']);
						
						
			}
		?>
    <form method="POST" role="form" >
			
		<div class="container">
            <input type="text" class="form-control" name="id_instructor" id="id_instructor" value="<?php echo $id_instructor ?>" hidden />
            <div class="validate"></div>
        </div>

		<div class="container">
            <input type="password" class="form-control" name="contrasena_df" id="contrasena_df" placeholder="Contraseña definida" required />
            <div class="validate"></div>
        </div>
			<br>	
        <div class="container">
            <input type="password" class="form-control" name="contrasena_nv" id="contrasena_nv" placeholder="Nueva contraseña" required />
            <div class="validate"></div>
        </div>
			<br>
        <div class="container">
            <input type="password" class="form-control" name="vr_contrasena" id="vr_contrasena" placeholder="Vuelve a escribir la nueva contraseña" required />
            <div class="validate"></div>
        </div>		
			<br>	
        <div class="text-center">
			<input type="submit" name="actualizar_contrasena" Value="Actualizar">
		</div>
    </form>  
	<br>
	<br>
	<br>
  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>