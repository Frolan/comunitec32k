<?php
require_once "conexion.php";
	require_once "clases.php";
	session_start();
	
	$id_curso = $_GET['id_curso'];
	
	$qry = 'SELECT * FROM comunitec32k_cursos WHERE idCurso = :id_curso';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id_curso' => $id_curso)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$nombre_curso = htmlentities($row['nombre_curso']);
	$sub_titulo_curso = htmlentities($row['sub_titulo_curso']);	
	$descripcion_curso = htmlentities($row['descripcion_curso']);
	$duracion_curso = htmlentities($row['duracion_curso']);
	$inicio_curso = htmlentities($row['inicio_curso']);
	$cierre_curso = htmlentities($row['cierre_curso']);
	$organizacion = htmlentities($row['organizacion']);
	$nivel_curso = htmlentities($row['nivel_curso']);
	$certificacion = htmlentities($row['certificacion']);
	$Imagen = htmlentities($row['Imagen']);
	
	$_SESSION['success'] = false;

	//Istanciacion de clases 
	$id_requisito = new Requisitos();
	$idCurso = new Requisitos();
	$requisito = new Requisitos();
	$isActive = new Requisitos();

	//DB connection
	$conectado = 1;
	
	//statement
	$tbl_requisitos = "SELECT * FROM comunitec32k_requisitos WHERE idCurso ='$id_curso' ORDER BY idCurso ASC ";
	$stmt_r = $link->query($tbl_requisitos); 
	$cant_reg = 0;
	while($row = $stmt_r->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_requisito->id_requisito = htmlentities($row['id_requisito']);
		$idCurso->idCurso = htmlentities($row['idCurso']);
		$requisito->requisito = htmlentities($row['requisito']);
		$isActive->isActive = htmlentities($row['IsActive']);

		$arrId_requisito[] = ($id_requisito->id_requisito);
		$arrId_curso[] = ($idCurso->idCurso);
		$arrRequisito[] = ($requisito->requisito);
		$arrActivo[] = ($isActive->isActive);
		
		$cant_reg++;	
	}	
	
	//Agregar motivos de visita 	
	if(isset($_POST['nuevo_requisito'])){
		try{
			$requisito = htmlentities(trim($_POST['requisito']));
			$idCurso = $id_curso;
			$activo = 1;
			
			$qry_requisitos = "INSERT INTO comunitec32k_requisitos 
							(idCurso,requisito,IsActive)
							VALUES (:curso, :req, :activo)
							";
			$stmt_requisitos = $link->prepare($qry_requisitos);
			$stmt_requisitos->execute(array(
						':curso' => $idCurso,
						':req' => $requisito,
						':activo' => $activo
						)
					);
			
			$_SESSION['success'] = "Requisito agregado con exito";
			header('Location: agregar_requisitos.php?id_curso='.$id_curso.'');
			return; 
		
		}catch(Exception $ex){
			echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
				echo '<h3> Error : '.$ex->getMessage().'</h3>';
				return;
			
		}
	}


?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Requisito Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
<br>
<!--Tabla de motivos activos -->
	<div class="container">
		<h1><center>Consulta de Requisitos</center></h1>
<?php
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
?>		
	
		<label>Agregar requisitos para el curso <b><?php echo $nombre_curso.' (numero '.  $id_curso .')'?>:</b></label>
		<form method="POST">
			<input type="text" name="requisito" class="form-control" placeholder="Requisito" required ><br>
			<input type="submit" name="nuevo_requisito" value="Agregar">
		</form>

		<br>
		<br>
	<table>
		<thead>
			<tr>
				<th scope="col" hidden >ID Requisito</th>
				<th scope="col">Numero</th>
				<th scope="col">Requisito</th>
				<th scope="col" hidden>Activo</th>
				<th scope="col">Actualizar</th>
			</tr>
		</thead>
			
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrRequisito as $requisito){
					$id = $arrId_requisito[$rowCount];
					$id_curso = $arrId_curso[$rowCount];
					$activo = $arrActivo[$rowCount];
		
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id.'</th>';
						echo '<td>'.$id_curso.'</td>';
						echo '<td>'.$requisito.'</td>';
						echo '<td scope="row" hidden>'.$activo.'</td>';
						echo	"<td>
									<a href='modificar_requisito.php?id=".$id."'>
										<button>Actualizar</button>
									</a>
								</td>";
					echo '</tr>';			
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay requisitos registrados</p>';
				echo '</div>';	
			}
		?>
	</table>
	</div>
	  <br>
	<div class = "container">
		<a href= "consultar_cursos.php"><button type="button">Regresar</button></a>
	</div>	
<br>	


  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>