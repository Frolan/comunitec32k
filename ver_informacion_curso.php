<?php
	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();
		
	$id_curso = $_GET['Id_curso'];
	
	$qry = 'SELECT * FROM comunitec32k_cursos WHERE idCurso = :curso';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':curso' => $id_curso)
	);
	
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
		$idCurso = htmlentities($row['idCurso']);
		$nombre_curso = htmlentities($row['nombre_curso']);
		$sub_titulo_curso = htmlentities($row['sub_titulo_curso']);
		$descripcion_curso = htmlentities($row['descripcion_curso']);
		$duracion_curso = htmlentities($row['duracion_curso']);
		$inicio_curso = htmlentities($row['inicio_curso']);
		$cierre_curso = htmlentities($row['cierre_curso']);
		
		$hora_llegada_h = htmlentities($row['hora_llegada_h']);
		$hora_llegada_p = htmlentities($row['hora_llegada_p']);
		$hora_salida_h = htmlentities($row['hora_salida_h']);
		$hora_salida_p = htmlentities($row['hora_salida_p']);		
		$dias_c = htmlentities($row['dias_c']);		
		
		$nivel_curso = htmlentities($row['nivel_curso']);
		$organizacion = htmlentities($row['organizacion']);
		$certificacion = htmlentities($row['certificacion']);
		
		$pago_unico_curso = htmlentities($row['pago_unico_curso']);
		$pago_semanal_curso = htmlentities($row['pago_semanal_curso']);
		$tendra_costo_ins = htmlentities($row['tendra_costo_ins']);
		$pago_inscripcion_curso = htmlentities($row['pago_inscripcion_curso']);		
		
		$Imagen = htmlentities($row['Imagen']);
		$IsActive = htmlentities($row['IsActive']);
	
	if($Imagen == ""){
		$Imagen = "assets/img/comunitec32k-logo.png";
	}
	
	/*==========REQUISITOS==========*/
	
	//Istanciacion de clases 
	$id_requisito = new Requisitos();
	$idCurso = new Requisitos();
	$requisito = new Requisitos();
	$isActive = new Requisitos();
	
	//statement
	$tbl_requisitos = "SELECT * FROM comunitec32k_requisitos WHERE idCurso ='$id_curso' ORDER BY idCurso ASC ";
	$stmt_r = $link->query($tbl_requisitos); 
	$cant_reg = 0;
	while($row = $stmt_r->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_requisito->id_requisito = htmlentities($row['id_requisito']);
		$requisito->requisito = htmlentities($row['requisito']);
		$isActive->isActive = htmlentities($row['IsActive']);

		$arrId_requisito[] = ($id_requisito->id_requisito);

		$arrRequisito[] = ($requisito->requisito);
		$arrActivo[] = ($isActive->isActive);
		
		$cant_reg++;	
	}	
?>

		


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Información Curso</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

 <section id="about" class="about">
	  <div class="container">
			<div class="row">

			  <div class="col-lg-5 col-md-6">
				<div class="about-img">
				<img src="<?php echo $Imagen ?>"> 
				</div>
			  </div>

			  <div class="col-lg-7 col-md-6">
				<div class="about-content">
					<h1><?php echo $nombre_curso ?></h1>
					<h2><?php echo $sub_titulo_curso ?></h2>
										
					<p><h4>Descripción</h4>
						<?php echo $descripcion_curso ?><br>
								<br>
						<label><b>Nivel del curso:</b></label>
						<?php echo $nivel_curso ?>	
								<br>
						<label><b>Organizacion que imparte el curso:</b></label>
						<?php echo $organizacion  ?>
								<br>
						<label><b>Entrega certificado con validez oficial:</b></label>
						<?php echo $certificacion ?></p>
										
					<p><h4>Duración</h4>
						<?php echo $duracion_curso ?><br>
								<br>
						<label><b>Fecha de inicio:</b></label>
						<?php echo $inicio_curso ?>
								<br>
						<label><b>Fecha de cierre:</b></label>
						<?php echo $cierre_curso ?></p>
					
					<p><h4>Horario</h4>
						<label><b>Dias del curso :</b></label>
						<?php echo $dias_c ?>
								<br>
						<label><b>Hora de llegada:</b></label>
						<?php echo $hora_llegada_h .' '.$hora_llegada_p  ?>
								<br>
						<label><b>Hora de salida:</b></label>
						<?php echo $hora_salida_h .' '.$hora_salida_p ?></p>	

					<p><h4>Costo</h4>
						<label><b>Inscripcion con costo :</b></label>
						<?php echo $tendra_costo_ins ?>
								<br>
						<label id="precio_inscripcion_l"  ><b>Costo de inscripción:</b></label>
						<?php echo '<label  id="precio_inscripcion inscripcion"  >'.$pago_inscripcion_curso.' Pesos</label>'  ?>
								<br>
						<label><b>Puedes hacer un pago unico de:</b></label>
						<?php echo $pago_unico_curso.' Pesos'?>		
								<br>
						<label><b>Puedes hacer pagos semanales de:</b></label>
						<?php echo $pago_semanal_curso.' Pesos'?></p>							
				</div>
			  </div>
	
			</div>
	  </div>

 </section>

<!-- REQUISITOS -->

      <div class="container">      
          <h3>Requisitos</h3>
  
        <div class="row">
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrRequisito as $requisito){
		
						echo '<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">';
							echo '<div class="box">';
							  echo '<h4 class="title">'.$requisito.'</h4>';
							echo '</div>';
						echo '</div>';	
				$rowCount ++;			
				}
		}else{
				echo '<div class="">';
					echo '<p>No solicita algun requisito</p>';
				echo '</div>';	
			}
		?>


		</div>
   
	<br>
	<div class="container">
		<center>  <?php echo "<a href='redireccion.php?Nombre_curso=".$nombre_curso."'>
							<button>Inscribirse a curso </button>
						  </a>"; ?>	
		</center>				  
	</div>
	
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
   
</body>
</html>