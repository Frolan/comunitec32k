<?php
require_once "conexion.php";
	session_start();
	
	$id_motivo = $_GET['id'];
	
	$qry = 'SELECT * FROM comunitec_tbl_motivos_de_visita WHERE id_motivos_de_visita = :id_evento';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id_evento' => $id_motivo
		)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$descripcion = htmlentities($row['descripcion']);	
	$campo_valido = htmlentities($row['campo_valido']);
	
	if(isset($_POST['actualizar_motivos_visita'])){
		$n_id_motivo = $_POST['id_motivos_de_visita'];
		$n_descripcion = $_POST['descripcion'];
		$n_campo_valido = $_POST['activador'];

		
			$update = 'UPDATE comunitec_tbl_motivos_de_visita SET 
							descripcion = :descripcion,
							campo_valido = :activo
						WHERE 
							id_motivos_de_visita = :id_motivo';
							
						$stmt = $link->prepare($update);
						$stmt->execute(array(
							':id_motivo' => $n_id_motivo,
							':descripcion' => $n_descripcion,
							':activo' => $n_campo_valido
							)
						);					
		
		header('Location: consultar_motivos_visita.php');
		return; 
	}

	if(isset($_POST['borrar'])){
	$n_id_motivo = $_POST['id_motivos_de_visita'];

		
			$delete = 'DELETE FROM comunitec_tbl_motivos_de_visita 
					WHERE 
						id_motivos_de_visita  = :id';
					
					$stmt = $link->prepare($delete);
					$stmt->execute(array(
							':id' => $n_id_motivo,
							)
						);	
						
		header('Location: consultar_motivos_visita.php');
		return; 			
	}	
	
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Modificar motivos visita</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
<div class="container">
		<header class="section-header">
		   <br>
          <h3> Actualizar motivos de visita </h3>
        </header>
			<form action="" method="POST">
	
				<div class="container">
					<div class="">
						<br>
						<label hidden >id_motivo</label>
						<input type="text" name="id_motivos_de_visita" class="form-control"  value ="<?php echo $id_motivo ?>" hidden />
					</div>
				</div>	
	
				<div class="container">
					<div class="">
						<br>
						<label>Descripcion</label>
						<input type="text" name="descripcion" class="form-control" required value ="<?php echo $descripcion ?>" />
					</div>
				</div>
			
				<div class="container">
					<div class="">
						<br>
						<label>Activar / Desactivar:</label><br>
						<select name="activador" class="form-control" required>
							<option value="1">Activar</option>
							<option value="0">Desactivar</option>
						</select>
					</div>
				</div>
				<br>
				
				<div class="container">
						<input type="submit" name="actualizar_motivos_visita" value="Actualizar" >
						<a href="consultar_motivos_visita.php"><button type="button" title="Cancelar">Cancelar</button></a>
						<input type="submit" name="borrar" value="Borrar">
				</div>			
			</form>	
</div>  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>