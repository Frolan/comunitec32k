<?php
require_once 'conexion.php';
	session_start();
	
	$id_usuario = $_GET['id'];
		
	$qry = 'SELECT * FROM cenaltec_tbl_estudiantes_cenaltec WHERE id_usuario = :id';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id' => $id_usuario)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$apellido_paterno = htmlentities($row['apellido_paterno']);
	$apellido_materno = htmlentities($row['apellido_materno']);	
	$nombre = htmlentities($row['nombre']);
	$calle_numero = htmlentities($row['calle_numero']);
	$colonia = htmlentities($row['colonia']);
	$cp = htmlentities($row['cp']);	
	$municipio = htmlentities($row['municipio']);
	$estado = htmlentities($row['estado']);
	$curp = htmlentities($row['curp']);
	$celular = htmlentities($row['celular']);
	$telefono_casa = htmlentities($row['telefono_casa']);
	$correo_electronico = htmlentities($row['correo_electronico']);
	
	if(isset($_POST['actualizar_estudiante'])){
		
		$n_id_usuario = htmlentities(trim($_POST['id_usuario']));
		$n_apellido_paterno = htmlentities(trim($_POST['apellido_paterno']));
		$n_apellido_materno = htmlentities(trim($_POST['apellido_materno']));
		$n_nombre = htmlentities(trim($_POST['nombre']));
		$n_calle_numero = htmlentities(trim($_POST['calle_numero']));
		$n_colonia = htmlentities(trim($_POST['colonia']));
		$n_cp = htmlentities(trim($_POST['cp']));
		$n_municipio = htmlentities(trim($_POST['municipio']));
		$n_estado = htmlentities(trim($_POST['estado']));		
		$n_curp = htmlentities(trim($_POST['curp']));
		$n_celular = htmlentities(trim($_POST['celular']));
		$n_telefono_casa = htmlentities(trim($_POST['telefono_casa']));
		$n_correo_electronico = htmlentities(trim($_POST['correo_electronico']));
		
		
			$update = 'UPDATE cenaltec_tbl_estudiantes_cenaltec SET 
							apellido_paterno = :apellido_patern,
							apellido_materno = :apellido_matern,
							nombre = :nombr,
							calle_numero = :calle_numer,
							colonia = :coloni,
							cp = :c,
							municipio = :municipi,
							estado = :estad,
							curp = :cur,
							celular = :celula,
							telefono_casa = :telefono_cas,
							correo_electronico = :correo_electronic
						WHERE
							id_usuario = :id_usuari';
							
						$stmt = $link->prepare($update);
						$stmt->execute(array(
							':apellido_patern' => $n_apellido_paterno,
							':apellido_matern' => $n_apellido_materno,
							':nombr' => $n_nombre,
							':calle_numer' => $n_calle_numero,
							':coloni' => $n_colonia,
							':c' => $n_cp,
							':municipi' => $n_municipio,	
							':estad' => $n_estado,
							':cur' => $n_curp,
							':celula' => $n_celular,
							':telefono_cas' => $n_telefono_casa,
							':correo_electronic' => $n_correo_electronico,	
							':id_usuari' => $n_id_usuario	
							)
						);					
		header('Location: consultar_estudiantes_cenaltec.php');
		return; 
	}
	
	if(isset($_POST['borrar'])){
	$n_id_usuario = $_POST['id_usuario'];

		
			$delete = 'DELETE FROM cenaltec_tbl_estudiantes_cenaltec 
					WHERE 
						id_usuario = :id';
					
					$stmt = $link->prepare($delete);
					$stmt->execute(array(
							':id' => $n_id_usuario,
							)
						);	
						
		header('Location: consultar_estudiantes_cenaltec.php');
		return; 			
	}
	
?>

<html>
<head>

	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Modificar usuarios Cenaltec</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
				
	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/tablas_Style.css" rel="stylesheet">
				
	<!-- =======================================================
	* Template Name: Rapid - v2.1.0
	* Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->

</head>
<body>
    <div class="container">
		<header class="section-header">
		   <br>
          <h3> Actualizar datos del participante (Cenaltec) </h3>
        </header>
			<form method="POST">
				<label hidden>ID Usuario: </label>
				<input type="text" name="id_usuario" hidden value ="<?php echo $id_usuario ?>">
				 <br>		
				<label>Apellido Paterno</label>
				<input type="text" name="apellido_paterno" class="form-control" required value="<?php echo $apellido_paterno ?>" />
				 <br>
				<label>Apellido Materno</label>
				<input type="text" name="apellido_materno" class="form-control" required  value="<?php echo $apellido_materno ?>" />
				 <br> 				 
				<label>Nombre</label>
				<input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" >
				 <br>
				<label>Calle</label>
				<input type="text" class="form-control" name="edad" value="<?php echo $calle_numero ?>" >
				 <br>
				<label>Colonia:</label>
				<input type="text" name="colonia" class="form-control" required value="<?php echo $colonia ?>" />
				 <br> 				 
				<label>Codigo Postal:</label>
				<input type="text" name="cp" class="form-control" required value="<?php echo $cp ?>" />
				 <br> 	
				<label>Municipio:</label>
				<input type="text" name="municipio" class="form-control" required value="<?php echo $municipio ?>" />
				 <br> 	
				<label>Estado:</label>
				<input type="text" name="estado" class="form-control" required value="<?php echo $estado ?>" />
				 <br> 			 
				<label>Curp:</label>
				<input type="text" name="curp" class="form-control" required value="<?php echo $curp ?>" />
				 <br> 	
				<label>Celular:</label>
				<input type="text" name="celular" class="form-control" required value="<?php echo $celular ?>" />
				 <br> 				 
				<label>Telefono casa:</label>
				<input type="text" name="telefono_casa" class="form-control" required value="<?php echo $telefono_casa ?>" />
				 <br> 
				<label>Correo:</label>
				<input type="text" name="correo_electronico" class="form-control" required value="<?php echo $correo_electronico ?>" />
				 <br> 
				 
				<input type="submit" name="actualizar_estudiante" value="Actualizar" >
				<a href="consultar_estudiantes_cenaltec.php"><button type="button" title="Cancelar">Cancelar</button></a>
				<input type="submit" name="borrar" value="Borrar">				
			</form>  	
	</div>


  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
 
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
  
</body>
</html>