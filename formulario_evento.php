<?php
require_once "conexion.php";
	session_start();
		
	$_SESSION['registro'] = false;	
	
	$nombre_evento = $_GET['Nombre_evento'];
	
	$qry = 'SELECT * FROM comunitec32k_eventos WHERE nombre_evento = :evento';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':evento' => $nombre_evento)
	);

	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
		$id_evento = htmlentities($row['id_evento']);
		$nombre_evento = htmlentities($row['nombre_evento']);
		$sub_titulo_evento = htmlentities($row['sub_titulo_evento']);
		$descripcion_evento = htmlentities($row['descripcion_evento']);
		$duracion_evento = htmlentities($row['duracion_evento']);
		$fecha_inicio = htmlentities($row['fecha_inicio']);	
		$fecha_cierre = htmlentities($row['fecha_cierre']);
		$hora_inicio_evento_h = htmlentities($row['hora_inicio_evento_h']);
		$hora_inicio_evento_p = htmlentities($row['hora_inicio_evento_p']);
		$PO_imparten = htmlentities($row['PO_imparten']);		
		$isActive = htmlentities($row['isActive']);			

if(isset($_POST['btnRegUsuario'])){
		
			$fecha_actual = htmlentities($_POST['fecha_actual']);
			$apellido_paterno = htmlentities($_POST['apellido_paterno']);
			$apellido_materno = htmlentities($_POST['apellido_materno']);
			$nombre_persona = htmlentities($_POST['nombre_persona']);
			$correo_electronico = htmlentities($_POST['correo_electronico']);
			
			$agregar_usuario = "INSERT INTO comunitec_tbl_asistentes_evento 
									(id_evento, fecha_inscrito, apellido_paterno, apellido_materno, nombre, nombre_evento, correo_electronico) 
									VALUES (:id_event, :fecha_inscrit, :apellido_patern, :apellido_matern, :nombre_person, :nombre_event, :correo_electronic)";

			$usuario = $link->prepare($agregar_usuario);
			$usuario->execute(array(
					':id_event' => $id_evento,
					':fecha_inscrit' => $fecha_actual,
					':apellido_patern' => $apellido_paterno,
					':apellido_matern' => $apellido_materno,
					':nombre_person' => $nombre_persona,
					':nombre_event' => $nombre_evento,
					':correo_electronic' => $correo_electronico,
					)
				);
	
	$_SESSION['registro'] = "Ha sido registrado con exito";			
	$subject = "Inscripcion a evento";
	$message = "El usuario ".$nombre_persona.' '.$apellido_paterno.' '.$apellido_materno.' se a registrado al evento '.$nombre_evento."";			
	
	enviarEmail($subject, $message, $correo_electronico); 
			
	$_SESSION['registro'] = "Ha sido registrado con exito";	
		header('Location: index.php');
		return;
			
}

	//========Funciones========// 	
	function enviarEmail($subject, $message, $correo_electronico){
		
		$sender = "felipe.galan@comunitec32k.com";
		$email = $sender;
		$name2send = "Comunitec32k";
		///$mailto = $email.",".$sender;
		$mailto = $sender.",".$correo_electronico;
		///$mailto = $ceo;
		
		$from="From: $name2send<$email>\r\nReturn-path: $sender";
		///$subject=
		///$message=
			
		mail($mailto, $subject, $message, $from);	
	}
?>
<!DOCTYPE html>
<html>
<head>
	
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Registro evento</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
<br>
	<div class="container">
		<h1><center>Registro al evento <?php echo $nombre_evento?></center></h1>

		<form method="POST">
			<div class="form-group">
				<label>Fecha de hoy:</label>
				<input type="date" name="fecha_actual" id="fecha_actual" class="form-control" required />
			</div>

			<br>

			<h2><center>DATOS PERSONALES</center></h2>

			<div class="form-group">
				<label>Apellido paterno:</label>
				<input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Apellido materno:</label>
				<input type="text" name="apellido_materno" id="apellido_materno" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Nombre(s):</label>
				<input type="text" name="nombre_persona" id="nombre_persona" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Correo:</label>
				<input type="text" name="correo_electronico" id="correo_electronico" class="form-control" placeholder="Ej. alguien@hotmail.com" required />
			</div>
			
			<div class="form-group">
				<input type="submit" value="Registrarse" name="btnRegUsuario">
			</div>	
			
		</form>
		
	<footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
</body>
</html>