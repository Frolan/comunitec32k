<?php
	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();

	//Istaciacion de clases
	$id_instructor = new Instructores();
	$nombre_ins = new Instructores();
	$apellido_pa_ins = new Instructores();
	$apellido_ma_ins = new Instructores();
	$correo_ins = new Instructores();
	$CURP = new Instructores();
	$contrasena = new Instructores();
	$cambiocontrasena = new Instructores();
	$id_tipo_usuario = new Instructores();
	$isActive = new Instructores();
	
	//DB connection
	$conectado = 1;

	//statement
	$instructores_activos = "SELECT * FROM comunitec_tbl_instructores WHERE id_tipo_usuario = 2 ";
	$stmt = $link->query($instructores_activos);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_instructor->id_instructor = htmlentities($row['id_instructor']);
		$nombre_ins->nombre_ins = htmlentities($row['nombre_ins']);
		$nombre_ins->apellido_pa_ins = htmlentities($row['apellido_pa_ins']);
		$nombre_ins->apellido_ma_ins = htmlentities($row['apellido_ma_ins']);
		$correo_ins->correo_ins = htmlentities($row['correo_ins']);
		$CURP->CURP = htmlentities($row['CURP']);
		$contrasena->contrasena = htmlentities($row['contrasena']);
		$cambiocontrasena->cambiocontrasena = htmlentities($row['cambiocontrasena']);
		$id_tipo_usuario->id_tipo_usuario = htmlentities($row['id_tipo_usuario']);
		$isActive->isActive = htmlentities($row['isActive']);
		

		$arrIdinstuctor[] = ($id_instructor->id_instructor);
		$arrNombre_instructor[] = ($nombre_ins->nombre_ins.' '.$nombre_ins->apellido_pa_ins.' '.$nombre_ins->apellido_ma_ins );
		$arrCorreo_ins[] = ($correo_ins->correo_ins); 
		$arrCURP[] = ($CURP->CURP);
		$arrContrasena[] = ($contrasena->contrasena);
		$arrCambio_c[] = ($cambiocontrasena->cambiocontrasena);
		$arrTipo_usuario[] = ($id_tipo_usuario->id_tipo_usuario);
		$arrIsActive[] = ($isActive->isActive);
		
		$cant_reg++;	
	}	
	
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Instructores Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
<br>
<!--Tabla de instructores activos -->
	<div class="container">
		<h1><center>Consulta de Instructores</center></h1>

	<div class="container">
		<label>Buscar Instructores</label><br>
		<input type="text" id="nombre" name="nombre">
		<input type="button" id="busca_nombre" name="busca_nombre_usuario" value="Buscar Usuario" onclick="buscar()">
	</div>	
		<br>
	<div class="container" id="datos_elemento">	
	</div>
		<br>
		
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>ID instructor</th>
				<th scope="col" >Nombre</th>
				<th scope="col" >Correo</th>
				<th scope="col" hidden>CURP</th>
				<th scope="col" hidden>Contraseña</th>
				<th scope="col" hidden>Cambio de contraseña</th>
				<th scope="col" hidden>Id tipo usuario</th>
				<th scope="col" hidden>Activo</th>
				<th scope="col" >Actualizar </th>
			</tr>
		</thead>
		<tbody>	
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrNombre_instructor as $instructor){
					$id_instructor = $arrIdinstuctor[$rowCount];
					$cor = $arrCorreo_ins[$rowCount];
					$cur = $arrCURP[$rowCount];
					$con = $arrContrasena [$rowCount];
					$cam = $arrCambio_c [$rowCount];
					$tip = $arrTipo_usuario [$rowCount];
					$isa = $arrIsActive [$rowCount];
					
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id_instructor.'</th>';
						echo '<td>'.$instructor.'</td>';
						echo '<td>'.$cor.'</td>';
						echo '<td scope="row" hidden>'.$cur.'</td>';
						echo '<td scope="row" hidden>'.$con.'</td>';
						echo '<td scope="row" hidden>'.$cam.'</td>';
						echo '<td scope="row" hidden>'.$tip.'</td>';
						echo '<td scope="row" hidden>'.$isa.'</td>';
						echo	"<td>
									<a href='modificar_instructores.php?id=".$id_instructor."'>
										<button>Actualizar</button>
									</a>
								</td>";								
					echo '</tr>';		
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay usuarios registrados</p>';
				echo '</div>';	
			}
		?>
	</tbody>	
		</table>
	</div>
<br>	
	<div class = "container">
		<a href= "main.php"><button type="button">Regresar</button></a>
	</div>

<br>	
  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
<script>
	function buscar(){
		elemento = document.getElementById("nombre").value;
		elemento.trim();
		if( elemento != ''){///elemento
			///document.getElementById("datos_usuario").innerHTML = '<p style = "color: green";>Hola '+elemento;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText != ""){						
						document.getElementById("datos_elemento").innerHTML = "<br>"+this.responseText;
					}
				}
			};
			xmlhttp.open("GET","buscar_instructores.php?elemento="+elemento,true);
			xmlhttp.send();			
		}///elemento no está vacío		
	}
</script>
</html>