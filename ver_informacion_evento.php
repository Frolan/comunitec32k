<?php
require_once "conexion.php";
	session_start();
		
	$nombre_evento = $_GET['Nombre_evento'];
	
	$qry = 'SELECT * FROM comunitec32k_eventos WHERE nombre_evento = :evento';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':evento' => $nombre_evento)
	);
	
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
		$id_evento = htmlentities($row['id_evento']);
		$nombre_evento = htmlentities($row['nombre_evento']);
		$sub_titulo_evento = htmlentities($row['sub_titulo_evento']);
		$descripcion_evento = htmlentities($row['descripcion_evento']);
		$duracion_evento = htmlentities($row['duracion_evento']);
		$fecha_inicio = htmlentities($row['fecha_inicio']);
		$fecha_cierre = htmlentities($row['fecha_cierre']);
		$hora_inicio_evento_h = htmlentities($row['hora_inicio_evento_h']);
		$hora_inicio_evento_p = htmlentities($row['hora_inicio_evento_p']);
		$PO_imparten = htmlentities($row['PO_imparten']);
		$IsActive = htmlentities($row['isActive']);
	
		$Imagen = "assets/img/comunitec32k-logo.png";
	
?>

		


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Información Evento</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

 <section id="about" class="about">
	  <div class="container">
			<div class="row">

			  <div class="col-lg-5 col-md-6">
				<div class="about-img">
				<img src="<?php echo $Imagen ?>"> 
				</div>
			  </div>

			  <div class="col-lg-7 col-md-6">
				<div class="about-content">
					<h1><?php echo $nombre_evento ?></h1>
					<h2><?php echo $sub_titulo_evento ?></h2>
										
					<p><h4>Descripción</h4>
						<?php echo $descripcion_evento ?><br>
																<br>
						<label><b>Evento impartido por:</b></label>
						<?php echo $PO_imparten  ?>
								<br>

					<p><h4>Duración</h4>
						<?php echo $duracion_evento ?><br>
								<br>
						<label><b>Fecha de inicio:</b></label>
						<?php echo $fecha_inicio ?>
								<br>
						<label><b>Fecha de cierre:</b></label>
						<?php echo $fecha_cierre ?></p>
								
						<label><b>Hora de inicio:</b></label>
						<?php echo $hora_inicio_evento_h ?><?php echo $hora_inicio_evento_p ?></p>
					
				
				</div>
			  </div>
			</div>
	  </div>
	  
	<div class="container">
		<center>  <?php echo "<a href='formulario_evento.php?Nombre_evento=".$nombre_evento."'>
							<button>Inscribirse a evento </button>
						  </a>"; ?>	
		</center>				  
	</div>
	
 </section>


	
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>