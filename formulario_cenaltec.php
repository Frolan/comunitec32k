<?php
require_once "conexion.php";
	session_start();

	$_SESSION['registro'] = false;		
	
	$nombre_curso = $_GET['Nombre_curso'];
	
	$qry = 'SELECT * FROM comunitec32k_cursos WHERE nombre_curso = :curso';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':curso' => $nombre_curso)
	);
	
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
		$idCurso = htmlentities($row['idCurso']);
		$sub_titulo_curso = htmlentities($row['sub_titulo_curso']);
		$descripcion_curso = htmlentities($row['descripcion_curso']);
		$duracion_curso = htmlentities($row['duracion_curso']);
		$inicio_curso = htmlentities($row['inicio_curso']);
		$cierre_curso = htmlentities($row['cierre_curso']);
		
		$hora_llegada_h = htmlentities($row['hora_llegada_h']);
		$hora_llegada_p = htmlentities($row['hora_llegada_p']);
		$hora_salida_h = htmlentities($row['hora_salida_h']);
		$hora_salida_p = htmlentities($row['hora_salida_p']);		
		$dias_c = htmlentities($row['dias_c']);		
		
		$nivel_curso = htmlentities($row['nivel_curso']);
		$organizacion = htmlentities($row['organizacion']);
		$certificacion = htmlentities($row['certificacion']);
		
		$pago_unico_curso = htmlentities($row['pago_unico_curso']);
		$pago_semanal_curso = htmlentities($row['pago_semanal_curso']);
		$tendra_costo_ins = htmlentities($row['tendra_costo_ins']);
		$pago_inscripcion_curso = htmlentities($row['pago_inscripcion_curso']);		
		
		$Imagen = htmlentities($row['Imagen']);
		$IsActive = htmlentities($row['IsActive']);
	
	if($Imagen == ""){
		$Imagen = "assets/img/comunitec32k-logo.png";
	}

if(isset($_POST['btnRegUsuario'])){
		
			$fecha_actual = htmlentities($_POST['fecha_actual']);
			$apellido_paterno = htmlentities($_POST['apellido_paterno']);	
			$apellido_materno = htmlentities($_POST['apellido_materno']);	
			$nombre = htmlentities($_POST['nombre']);	
			$calle_numero = htmlentities($_POST['calle_numero']);	
			$colonia = htmlentities($_POST['colonia']);
			$cp = htmlentities($_POST['cp']);
			$municipio = htmlentities($_POST['municipio']);
			$estado = htmlentities($_POST['estado']);
			$sexo = htmlentities($_POST['sexo']);
			$curp = htmlentities($_POST['curp']);
			$fecha_nacimiento = htmlentities($_POST['fecha_nacimiento']);
			$lugar_nacimiento = htmlentities($_POST['lugar_nacimiento']);
			$telefono_casa = htmlentities($_POST['telefono_casa']);
			$telefono_trabajo = htmlentities($_POST['telefono_trabajo']);
			$celular = htmlentities($_POST['celular']);
			$estado_civil = htmlentities($_POST['estado_civil']);
			$correo_electronico = htmlentities($_POST['correo_electronico']);
			$especificar_estudios = htmlentities($_POST['especificar_estudios']);
			$tomado_curso_antes = htmlentities($_POST['tomado_curso_antes']);
			$servicio_medico = htmlentities($_POST['servicio_medico']);
			$alergias_enf = htmlentities($_POST['alergias_enf']);
			$tipo_sangre = htmlentities($_POST['tipo_sangre']);
			$presenta_discapacidad = htmlentities($_POST['presenta_discapacidad']);
			$nombre_emergencia = htmlentities($_POST['nombre_emergencia']);
			$telefono_emergencia = htmlentities($_POST['telefono_emergencia']);
			$nombre_empresa = htmlentities($_POST['nombre_empresa']);
			$puesto = htmlentities($_POST['puesto']);
			$direccion_empresa = htmlentities($_POST['direccion_empresa']);
			$antiguedad = htmlentities($_POST['antiguedad']);
			$apoyo_empresa = htmlentities($_POST['apoyo_empresa']);
			$medio_enterado = htmlentities($_POST['medio_enterado']);
			$motivos_entrenamiento = htmlentities($_POST['motivos_entrenamiento']);
			$curso = htmlentities($_POST['curso']);
			$inicio_curso = htmlentities($_POST['inicio_curso']);
			$duracion_curso = htmlentities($_POST['duracion_curso']);
			
			$requiere_factura = htmlentities($_POST['requiere_factura']);
			$nombre_factura = htmlentities($_POST['nombre_fac']);
			$razon_social = htmlentities($_POST['razon_social']);
			$rfc_fac = htmlentities($_POST['rfc_fac']);
			$SFDI_fac = htmlentities($_POST['SFDI_fac']);

			$agregar_persona = "INSERT INTO cenaltec_tbl_estudiantes_cenaltec
							(fecha_actual, apellido_paterno, apellido_materno, nombre, calle_numero, colonia, cp, municipio,
							estado, sexo, curp, fecha_nacimiento, lugar_nacimiento, telefono_casa, telefono_trabajo, celular, estado_civil, correo_electronico,
							especificar_estudios, tomado_curso_antes, servicio_medico, alergias_enf, tipo_sangre, presenta_discapacidad, nombre_emergencia,
							telefono_emergencia, nombre_empresa, puesto,
							direccion_empresa, antiguedad, apoyo_empresa, medio_enterado, motivos_entrenamiento, curso, inicio_curso, duracion_curso,
							requiere_factura, nombre_factura, razon_social, rfc_fac, SFDI_fac   
							) 
							VALUES (:fecha_a, :apellido_p, :apellido_m, :nombre, :calle_n, :col, :cp, :mun, :est, :sex, :crp, 
									:fecha_n, :lugar_n, :telefono_c, :telefono_t, :cel, :estado_c, :correo_e, :especificar_e, :tomado_c, :servicio_m, 
									:alergias_e, :tipo_s, :presenta_d, :nombre_e, :telefono_e, :nombre_em, 
									:puesto, :direccion_e, :antiguedad, :apoyo_e, :medio_e, :motivos_e, :curso, :inicio_c, :duracion_c, :req_f, :nom_f, :raz_f, :rfc_f, :SDFI_f 
									)";
			$persona = $link->prepare($agregar_persona);
			$persona->execute(array(
					':fecha_a' => $fecha_actual,
					':apellido_p' => $apellido_paterno,
					':apellido_m' => $apellido_materno,
					':nombre' => $nombre,
					':calle_n' => $calle_numero,
					':col' => $colonia,
					':cp' => $cp,
					':mun' => $municipio,
					':est' => $estado,
					':sex' => $sexo,
					':crp' => $curp,
					':fecha_n' => $fecha_nacimiento,
					':lugar_n' => $lugar_nacimiento,
					':telefono_c' => $telefono_casa,
					':telefono_t' => $telefono_trabajo,
					':cel' => $celular, 
					':estado_c' => $estado_civil,
					':correo_e' => $correo_electronico,
					':especificar_e' => $especificar_estudios,
					':tomado_c' => $tomado_curso_antes,
					':servicio_m' => $servicio_medico,
					':alergias_e' => $alergias_enf,
					':tipo_s' => $tipo_sangre,
					':presenta_d' => $presenta_discapacidad,
					':nombre_e' => $nombre_emergencia,
					':telefono_e' => $telefono_emergencia,
					':nombre_em' => $nombre_empresa,
					':puesto' => $puesto,
					':direccion_e' => $direccion_empresa,
					':antiguedad' => $antiguedad,
					':apoyo_e' => $apoyo_empresa,
					':medio_e' => $medio_enterado,
					':motivos_e' => $motivos_entrenamiento,
					':curso' => $curso,
					':inicio_c' => $inicio_curso,
					':duracion_c' => $duracion_curso,
					':req_f' => $requiere_factura,
					':nom_f' => $nombre_factura,
					':raz_f' => $razon_social,
					':rfc_f' => $rfc_fac,
					':SDFI_f' => $SFDI_fac
					)
				);
				
			$subject = "Inscripcion a curso";
			$message = "El usuario ".$nombre.' '.$apellido_paterno.' '.$apellido_materno.' se a registrado al curso '.$curso."";			
			enviarEmail($subject, $message, $correo_electronico); 			
			
			$_SESSION['registro'] = "Ha sido registrado con exito";	
				header('Location: index.php');
				return;

}

	//========Funciones========// 	
	function enviarEmail($subject, $message, $correo_electronico){
		
		$sender = "felipe.galan@comunitec32k.com";
		$email = $sender;
		$name2send = "Comunitec32k";
		///$mailto = $email.",".$sender;
		$mailto = $sender.",".$correo_electronico;
		///$mailto = $ceo;
		
		$from="From: $name2send<$email>\r\nReturn-path: $sender";
		///$subject=
		///$message=
			
		mail($mailto, $subject, $message, $from);	
	}	
?>
<!DOCTYPE html>
<html>
<head>
	
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Registro CENALTEC</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>
<body>
<br>
	<div class="container">
		<h1><center>¿Primera vez que nos visitas? Favor de registrarse a CENALTEC:</center></h1>

		<p>Al completar este formulario recibirá un correo para darle seguimiento a su inscripción (puede que no sea inmediatamente o que llegue como correo no deseado)</p>

		<form method="POST">
			<div class="form-group">
				<label>Fecha de hoy:</label>
				<input type="date" name="fecha_actual" id="fecha_actual" class="form-control" required />
			</div>

			<br>

			<h2><center>DATOS PERSONALES</center></h2>

			<div class="form-group">
				<label>Apellido paterno:</label>
				<input type="text" name="apellido_paterno" id="apellido_paterno" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Apellido materno:</label>
				<input type="text" name="apellido_materno" id="apellido_materno" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Nombre(s):</label>
				<input type="text" name="nombre" id="nombre" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Calle y numero:</label>
				<input type="text" name="calle_numero" id="calle_numero" class="form-control" placeholder="Ej. Plan de ayala 1234" required />
			</div>

			<div class="form-group">
				<label>Colonia:</label>
				<input type="text" name="colonia" id="colonia" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Codigo postal:</label>
				<input type="text" name="cp" id="cp" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Municipio:</label>
				<input type="text" name="municipio" id="municipio" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Estado:</label>
				<input type="text" name="estado" id="estado" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Sexo:</label>
				<select type="select" name="sexo" id="sexo" class="form-control" required />
					<option value="Hombre">Hombre</option>
					<option value="Mujer">Mujer</option>
				</select>
			</div>

			<div class="form-group">
				<label>CURP: ¿No conoces tu CURP? <a href="https://www.gob.mx/curp/" target="_blank"> Buscala aqui</a></label>
				<input type="text" name="curp" id="curp" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Fecha de nacimiento:</label>
				<input type="date" name="fecha_nacimiento" id="fecha_nacimiento" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Lugar de nacimiento: (País)</label>
				<input type="text" name="lugar_nacimiento" id="lugar_nacimiento" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Telefono de casa:</label>
				<input type="text" name="telefono_casa" id="telefono_casa" class="form-control" />
			</div>

			<div class="form-group">
				<label>Telefono de trabajo:</label>
				<input type="text" name="telefono_trabajo" id="telefono_trabajo" class="form-control" />
			</div>

			<div class="form-group">
				<label>Celular:</label>
				<input type="text" name="celular" id="celular" class="form-control" />
			</div>

			<div class="form-group">
				<label>Estado civil:</label>
				<select type="select" name="estado_civil" id="estado_civil" class="form-control" required />
					<option value="Soltero">Soltero</option>
					<option value="Casado">Casado</option>
					<option value="Viudo">Viudo</option>
					<option value="Divorciado">Divorciado</option>
					<option value="Union libre">Union libre</option>
					<option value="Separado">Separado</option>
				</select>
			</div>

			<div class="form-group">
				<label>Correo electronico:</label>
				<input type="email" name="correo_electronico" id="correo_electronico" class="form-control" required />
			</div>

			<div class="form-group">
				<label>Grado maximo de estudios:</label>
				<select type="select" name="especificar_estudios" id="especificar_estudios" class="form-control" required />
					<option value="Sin_escolaridad">Sin escolaridad</option>
					<option value="Primaria_inconclusa">Primaria inconclusa</option>
					<option value="Primaria terminada">Primaria terminada</option>
					<option value="Secundaria_inconclusa">Secundaria inconclusa</option>
					<option value="Secundaria_terminada">Secunaria terminada</option>
					<option value="Preparatoria_inconclusa">Preparatoria inconclusa</option>
					<option value="Preparatoria_terminada">Preparatoria terminada</option>
					<option value="Licenciatura_inconclusa">Licenciatura inconclusa</option>
					<option value="Licenciatura_terminada">Licenciatura terminada</option>
					<option value="Posgrado">Posgrado</option>
				</select>
			</div>

			<div class="form-group">
				<label>¿Ha tomado curso anteriormente en CENALTEC?:</label>
				<select type="select" name="tomado_curso_antes" id="tomado_curso_antes" class="form-control" required />
					<option value="No">No</option>
					<option value="Si">Si</option>
				</select>
			</div>

			<div class="form-group">
				<label>¿Cuenta con algun servicio medico?:</label>
				<select type="select" name="servicio_medico" id="servicio_medico" class="form-control" required />
					<option value="No">No</option>
					<option value="Imss">IMSS</option>
					<option value="Isste">ISSTE</option>
					<option value="Otro">Otro</option>
				</select>
			</div>

			<div class="form-group">
				<label>¿Padece alergias o alguna enfermedad?:</label>
				<input type="text" placeholder="Si la respuesta es positiva, indicar cual" name="alergias_enf" id="alergias_enf" class="form-control"  />
			</div>

			<div class="form-group">
				<label>Tipo de sangre:</label>
				<input type="text" name="tipo_sangre" id="tipo_sangre" class="form-control" />
			</div>

			<div class="form-group">
				<label>¿Presenta alguna discapacidad?:</label>
				<input type="text" placeholder="Si la respuesta es positiva, indicar cual" name="presenta_discapacidad" id="presenta_discapacidad" class="form-control"  />
			</div>

			<div class="form-group">
				<label>En caso de emergencia avisar a:</label>
				<input type="text" placeholder="Nombre:" name="nombre_emergencia" id="nombre_emergencia" class="form-control" required />
			</div>

			<div class="form-group">
				<input type="text" placeholder="Telefono:" name="telefono_emergencia" id="telefono_emergencia" class="form-control" required />
			</div>
			<br>

			<h2><center>INFORMACION LABORAL</center></h2>

			<div class="form-group">
				<label>Nombre de la empresa en que labora:</label>
				<input type="text" name="nombre_empresa" id="nombre_empresa" class="form-control" />
			</div>

			<div class="form-group">
				<label>Puesto:</label>
				<input type="text" name="puesto" id="puesto" class="form-control" />
			</div>

			<div class="form-group">
				<label>Direccion:</label>
				<input type="text" name="direccion_empresa" id="direccion_empresa" class="form-control" />
			</div>

			<div class="form-group">
				<label>Antigüedad (Años / Meses):</label>
				<input type="text" name="antiguedad" id="antiguedad" class="form-control" placeholder="Ej. 1 año 3 meses" />
			</div>

			<div class="form-group">
				<label>¿Recibira apoyo de la empresa?:</label>
				<select type="select" name="apoyo_empresa" id="apoyo_empresa" class="form-control" />
					<option value="No">No</option>
					<option value="Si">Si</option>
				</select>
			</div>
			<br>

			<h2><center>INFORMACION ADICIONAL</center></h2>

			<div class="form-group">
				<label>Medio por el cual se entero del entrenamiento:</label>
				<select type="select" name="medio_enterado" id="medio_enterado" class="form-control" required />
					<option value="Television">Television</option>
					<option value="Periodico">Periodico</option>
					<option value="Recomendacion">Recomendacion</option>
					<option value="Radio">Radio</option>
					<option value="Otro">Otro</option>
				</select>
			</div>

			<div class="form-group">
				<label>Motivos de eleccion del entrenamiento:</label>
				<select type="select" name="motivos_entrenamiento" id="motivos_entrenamiento" class="form-control" required />
					<option value="Para_emplearse_o_autoemplearse">Para emplearse o autoemplearse</option>
					<option value="Por_disposicion_de_tiempo_libre">Por disposicion de tiempo libre</option>
					<option value="Por_estar_en_espera">Por estar en espera de incorporarse a otra institucion educativa</option>
					<option value="Para_mejorar_trabajo">Para mejorar su situacion en el trabajo</option>
					<option value="Ahorrar_gasto">Para ahorra gasto al ingreso familiar</option>
					<option value="Otro">Otro</option>
				</select>
			</div>
			<br>

			<h2><center>ENTRENAMIENTO</center></h2>

			<div class="form-group">
				<label hidden>Curso:</label>
				<input type="text" name="curso" id="curso" class="form-control" required value="<?php echo $nombre_curso?>" hidden />
			</div>

			<div class="form-group">
				<label hidden>Inicio:</label>
				<input type="date" name="inicio_curso" id="inicio_curso" class="form-control" required value="<?php echo $inicio_curso?>" hidden />
			</div>

			<div class="form-group">
				<label hidden>Duracion:</label>
				<input type="text" name="duracion_curso" id="duracion_curso" class="form-control" required value="<?php echo $duracion_curso?>" hidden />
			</div>

						<label><b>Curso:</b></label>
						<?php echo $nombre_curso ?>			
				<p><h4>Duración</h4>
						<?php echo $duracion_curso ?><br>
								<br>
						<label><b>Fecha de inicio:</b></label>
						<?php echo $inicio_curso ?>
								<br>
						<label><b>Fecha de cierre:</b></label>
						<?php echo $cierre_curso ?></p>
			
				<p><h4>Horario</h4>
						<label><b>Dias del curso :</b></label>
						<?php echo $dias_c ?>
								<br>
						<label><b>Hora de curso:</b></label>
						<?php echo 'De '.$hora_llegada_h .' '.$hora_llegada_p.' a '.$hora_salida_h .' '.$hora_salida_p  ?>
								<br>
	
						
			<h2><center>FACTURACIÓN</center></h2>

			<div class="form-group">
				<label>¿Require Facturación?</label>
				<select type="select" name="requiere_factura" id="requiere_factura" class="form-control" />
					<option value="No">No</option>
					<option value="Si">Si</option>
				</select>
			</div>
			
			<div class="form-group">
				<label>Nombre:</label>
				<input type="text" name="nombre_fac" id="nombre_fac" class="form-control" />
			</div>	

			<div class="form-group">
				<label>Razón Social:</label>
				<input type="text" name="razon_social" id="razon_social" class="form-control" />
			</div>	

			<div class="form-group">
				<label>RFC:</label>
				<input type="text" name="rfc_fac" id="rfc_fac" class="form-control" />
			</div>	

			<div class="form-group">
				<label>SFDI:</label>
				<input type="text" name="SFDI_fac" id="SFDI_fac" class="form-control" />
			</div>				
			
			<div>
				<p>La comunidad Tecnológica del Centro pone a su disposición de su  <a href="https://www.inadet.com.mx/Home/wp-content/uploads/2018/06/AVISO_PRIVACIDAD-inadet.pdf">politica de privacidad</a>, el cuál especifica los detalles del manejo de la información que nos proporciona.<br>
				<br>Así mismo, esta información es turnada al Instituto de Apoyo al Desarrollo Tecnológico para la emisión de su constancia de terminación de estudios, por lo que también ponemos a su disposición el aviso de privacidad de este instituto. </p>
			</div>
			
			<div class="form-group">
				<input type="checkbox" name="checkbox" value="check" id="agree" required />
				He leido la politica de privacidad de CENALTEC.
			</div>

			<div class="form-group">
				<input type="submit" class="btnAgregar" value="Registrarse" name="btnRegUsuario">
			</div>
		</form>
	</div>
	<br>
	<footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
</body>
</html>