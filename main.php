<?php
require_once 'conexion.php';
session_start();
//var_dump($_POST);

UNSET($_SESSION['success']);

if($_SESSION['entrada'] !== 1){
	$_SESSION['error'] = 'Favor de iniciar sesion';
	header('Location: login.php'); 
	return;
}

$_SESSION['entrada'] = 1;

$failure = false;
$_SESSION['success'] = false;

//Variables de la seccion "Sobre nosotros"
$titulo_sn = "Titulo";
$subtitulo_sn = "Subtitulo";
$parrafo1_sn = "Parrafo #1 : Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ";
$parrafo2_sn = "Parrafo #2 : Aut dolor id. Sint aliquam consequatur ex ex labore. Et quis qui dolor nulla dolores neque. Aspernatur consectetur omnis numquam quaerat. Sed fugiat nisi. Officiis veniam molestiae. Et vel ut quidem alias veritatis repudiandae ut fugit. Est ut eligendi aspernatur nulla voluptates veniam iusto vel quisquam. Fugit ut maxime incidunt accusantium totam repellendus eum error. Et repudiandae eum iste qui et ut ab alias.";
$punto1_sn = "Punto # 1 : Ullamco laboris nisi ut aliquip ex ea commodo consequat. ";
$punto2_sn = "Punto # 2 : Duis aute irure dolor in reprehenderit in voluptate velit. ";
$punto3_sn = "Punto # 3 : Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur. ";
$parrafo3_sn = "Parrafo #3 : Parrafo extra";
$img_sn = "assets/img/comunitec32k-logo.png";


//Agregar evento a BD 
if(isset($_POST['agregar_evento'])){
	try{
		$nombre_evento = htmlentities(trim($_POST['nombre_evento']));
		$sub_titulo_evento = htmlentities(trim($_POST['sub_titulo_evento']));
		$descripcion_evento = htmlentities(trim($_POST['descripcion_evento']));
		$duracion_evento = htmlentities(trim($_POST['duracion_evento']));
		$fecha_inicio = htmlentities(trim($_POST['fecha_inicio']));
		$fecha_cierre = htmlentities(trim($_POST['fecha_cierre']));
		$hora_inicio_h = htmlentities(trim($_POST['hora_inicio_h']));
		$hora_inicio_p = htmlentities(trim($_POST['hora_inicio_p']));
		$PO_imparten = htmlentities(trim($_POST['PO_imparten']));
		$isActive = 1;
		
		$qry_evento = "INSERT INTO comunitec32k_eventos
							(nombre_evento,sub_titulo_evento,descripcion_evento,duracion_evento,fecha_inicio,fecha_cierre,hora_inicio_evento_h,hora_inicio_evento_p,PO_imparten, isActive)
							VALUES (:nombre_even,:sub_titulo, :descripcion_e, :duracion_hora, :fecha_i, :fecha_c, :inicio_hora, :inicio_pe, :imparte, :activo)";
		
		$stmt_eventos = $link->prepare($qry_evento);
		$stmt_eventos->execute(array(
					':nombre_even' => $nombre_evento,
					':sub_titulo' => $sub_titulo_evento,
					':descripcion_e' => $descripcion_evento,
					':duracion_hora' => $duracion_evento,
					':fecha_i' => $fecha_inicio,
					':fecha_c' => $fecha_cierre,
					':inicio_hora' => $hora_inicio_h,
					':inicio_pe' => $hora_inicio_p,
					':imparte' => $PO_imparten,
					':activo' => $isActive
					)
				);	
		
		$_SESSION['success'] = "Evento agregado exitosamente";	
		
		/*header('Location: main.php');
		return;	*/		
					
	}catch(Exception $ex){
		echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
			echo '<h3> Error : '.$ex->getMessage().'</h3>';
			return;
	}
}

//Agregar curso a BD 
if(isset($_POST['agregar_curso'])){
	try{
		$nombre_curso = htmlentities(trim($_POST['nombre_curso']));
		$sub_titulo_curso = htmlentities(trim($_POST['sub_titulo_curso']));
		$descripcion_curso = htmlentities(trim($_POST['descripcion_curso']));
		$duracion_curso = htmlentities(trim($_POST['duracion_curso']));
		$inicio_curso = htmlentities(trim($_POST['inicio_curso']));
		$cierre_curso = htmlentities(trim($_POST['cierre_curso']));
		
		$hora_llegada_h = htmlentities(trim($_POST['hora_llegada_h']));
		$hora_llegada_p = htmlentities(trim($_POST['hora_llegada_p']));
		$hora_salida_h = htmlentities(trim($_POST['hora_salida_h']));
		$hora_salida_p = htmlentities(trim($_POST['hora_salida_p']));
		
		//Procesar datos del check box
		$dias_c = '';
		if(isset($_POST['dias_c'])){
			$dias_c = implode(',  ' , $_POST['dias_c']);
		}
				
		$nivel_curso = htmlentities(trim($_POST['nivel_curso']));
		$organizacion = htmlentities(trim($_POST['organizacion']));
		$certificado = htmlentities(trim($_POST['certificado']));
		
		$pago_unico_curso = htmlentities(trim($_POST['pago_unico_curso']));
		$pago_semanal_curso = htmlentities(trim($_POST['pago_semanal_curso']));
		$tendra_costo_ins = htmlentities(trim($_POST['tendra_costo_ins']));
		$pago_inscripcion_curso = htmlentities(trim($_POST['pago_inscripcion_curso']));
		
		$imagen = htmlentities(trim($_POST['imagen']));
		$isActive = 1;
		
	
		
		$qry_cursos = "INSERT INTO `comunitec32k_cursos`
						(`nombre_curso`, `sub_titulo_curso`, `descripcion_curso`, `duracion_curso`, `inicio_curso`, `cierre_curso`, `hora_llegada_h`, `hora_llegada_p`, `hora_salida_h`, `hora_salida_p`, `dias_c` ,`nivel_curso`, `organizacion`, `certificacion`, `pago_unico_curso`, `pago_semanal_curso`, `tendra_costo_ins`, `pago_inscripcion_curso`, `Imagen`, `IsActive`) 
						VALUES (:nombre_c, :sub_titulo,:descripcion_c,:duracion,:inicio_c,:cierre_c,:h_ll_h,:h_ll_p,:h_s_h,:h_s_p, :dia_c,:nivel_c,:organ,:certifi,:p_u_c,:p_s_c,:t_c_i,:p_i_c,:imagen,:acti)";
		
		$stmt_cursos = $link->prepare($qry_cursos);
		$stmt_cursos->execute(array(
					':nombre_c' => $nombre_curso,
					':sub_titulo' => $sub_titulo_curso,
					':descripcion_c' => $descripcion_curso,
					':duracion' => $duracion_curso,
					':inicio_c' => $inicio_curso,
					':cierre_c' => $cierre_curso,
					':h_ll_h' => $hora_llegada_h,
					':h_ll_p' => $hora_llegada_p,
					':h_s_h' => $hora_salida_h,
					':h_s_p' => $hora_salida_p,
					':dia_c' => $dias_c,
					':nivel_c' => $nivel_curso,
					':organ' => $organizacion,
					':certifi' => $certificado,
					':p_u_c' => $pago_unico_curso,
					':p_s_c' => $pago_semanal_curso,
					':t_c_i' => $tendra_costo_ins,
					':p_i_c' => $pago_inscripcion_curso,
					':imagen' => $imagen,
					':acti' => $isActive
					)
				);	
		
		$_SESSION['success'] = "Curso agregado exitosamente, para agregar los requisitos visite consultar cursos y de click en agregar requisito";	
		
		/*header('Location: main.php');
		return;	*/	
					
	}catch(Exception $ex){
		echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
		echo '<h3> Error : '.$ex->getMessage().'</h3>';
		return;
	}
}

//Agragar instructores
if(isset($_POST['agregar_intructor'])){
	try{
		$nombre_ins = htmlentities(trim($_POST['nombre_ins']));
		$apellido_pa_ins = htmlentities(trim($_POST['apellido_pa_ins']));
		$apellido_ma_ins = htmlentities(trim($_POST['apellido_ma_ins']));
		$CURP = htmlentities(trim($_POST['CURP']));
		$email_instructor = htmlentities(trim($_POST['email'])); //Este dato se llama correo_ins en la BD
		$contrasena = generateStrongpassword();
		$cambiocontrasena = 1; //Valor por defecto  
		$id_tipo_usuario = 2; //Valor por defecto 
		$activo = 1; //Valor por defecto 
		
		$contrasena_c = password_hash($contrasena, PASSWORD_DEFAULT);
		
		$qry_instructores = "INSERT INTO comunitec_tbl_instructores
						(nombre_ins,apellido_pa_ins,apellido_ma_ins,correo_ins,CURP,contrasena,cambiocontrasena,id_tipo_usuario,isActive)
						VALUES (:nombre, :ap_pa, :ap_ma, :correo, :curp, :contrasena, :cambio, :tipo, :activo)";
		
		$stmt_instructores = $link->prepare($qry_instructores);
		$stmt_instructores->execute(array(
					':nombre' => $nombre_ins,
					':ap_pa' => $apellido_pa_ins,
					':ap_ma' => $apellido_ma_ins,
					':curp' => $CURP,
					':correo' => $email_instructor,
					':contrasena' => $contrasena_c,
					':cambio' => $cambiocontrasena,
					':tipo' => $id_tipo_usuario,
					':activo' => $activo
					)
				);	
				
		$_SESSION['success'] = "Instructor agregado exitosamente";	
				
		///Enviar correo a admin con contraseña
		$subject = "Bienvenido al portal Comunitec32k";
		$message = "¡Se creo el usuario ".$nombre_ins."! Para ingresar al portal debe acceder con su correo electrónico ".$email." y la contraseña: ".$contrasena." en la seccion staff";			
		enviarEmail($subject, $message, $email_instructor);	

		/*header('Location: main.php');
		return;	*/		
		
	}catch(Exception $ex){
		echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
		echo '<h3> Error : '.$ex->getMessage().'</h3>';
		return;	
	}
}

//visualizar seccion sobre nosotros
if(isset($_POST['visualizar'])){
	try{
		$titulo_sn = htmlentities(trim($_POST['titulo_sn']));	
		$subtitulo_sn = htmlentities(trim($_POST['subtitulo_sn']));	
		$parrafo1_sn = htmlentities(trim($_POST['parrafo1_sn']));	
		$parrafo2_sn = htmlentities(trim($_POST['parrafo2_sn']));	
		$punto1_sn = htmlentities(trim($_POST['punto1_sn']));	
		$punto2_sn = htmlentities(trim($_POST['punto2_sn']));	
		$punto3_sn = htmlentities(trim($_POST['punto3_sn']));	
		$parrafo3_sn = htmlentities(trim($_POST['parrafo3_sn']));	
		$img_sn = htmlentities(trim($_POST['img_sn']));	
		
		if($img_sn == ''){
			$img_sn = 'assets/img/comunitec32k-logo.png';
		}
		
	}catch(Exception $ex){
		echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
		echo '<h3> Error : '.$ex->getMessage().'</h3>';
		return;	
	}
}

if(isset($_POST['aplicar'])){
	$n_id_sb = 1;
	$n_titulo_sn = $_POST['titulo_sn'];
	$n_subtitulo_sn = $_POST['subtitulo_sn'];	
	$n_parrafo1_sn = $_POST['parrafo1_sn'];
	$n_parrafo2_sn = $_POST['parrafo2_sn'];
	$n_punto1_sn = $_POST['punto1_sn'];
	$n_punto2_sn = $_POST['punto2_sn'];
	$n_punto3_sn = $_POST['punto3_sn'];
	$n_parrafo3_sn = $_POST['parrafo3_sn'];
	$n_img_sn = $_POST['img_sn'];
	
		$update = 'UPDATE comunitec32k_sobre_nosotros SET 
						titulo_sn = :titulo,
						subtitulo_sn = :subtitulo,
						parrafo1_sn = :pa1,
						parrafo2_sn = :pa2,
						punto1_sn = :pu1,
						punto2_sn = :pu2,
						punto3_sn = :pu3,
						parrafo3_sn = :pa3,
						img_sn = :ima 
					WHERE 
						id_sb = :id';

					$stmt = $link->prepare($update);
					$stmt->execute(array(
						':id' => $n_id_sb,
						':titulo' => $n_titulo_sn,
						':subtitulo' => $n_subtitulo_sn,
						':pa1' => $n_parrafo1_sn,
						':pa2' => $n_parrafo2_sn,
						':pu1' => $n_punto1_sn,
						':pu2' => $n_punto2_sn,
						':pu3' => $n_punto3_sn,
						':pa3' => $n_parrafo3_sn,
						':ima' => $n_img_sn,
						)
					);	
					
			$_SESSION['success'] = "Se a actualizado la sección principal";	 								
}
//========Funciones========// 
function generateStrongpassword(){
  /*
  ************************************
  * Based on http://codepad.org/UL8k4aYK *
  ************************************
  */
  //$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
  $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^";
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	for ($i = 0; $i < 8; $i++) {
		$n = rand(0, $alphaLength);
		$pass[] = $alphabet[$n];
	}
  return implode($pass); //turn the array into a string    
}

	function enviarEmail($subject, $message, $email_instructor){
		
		$sender = "felipe.galan@comunitec32k.com";
		$email = $sender;
		$name2send = "Comunitec32k";
		///$mailto = $email.",".$sender;
		$mailto = $sender.",".$email_instructor;
		///$mailto = $ceo;
		
		$from="From: $name2send<$email>\r\nReturn-path: $sender";
		///$subject=
		///$message=
			
		mail($mailto, $subject, $message, $from);	
	}

?>
<html>
<head>
			
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Main Comunitec32k</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
				
	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/tablas_Style.css" rel="stylesheet">
				
	<!-- =======================================================
	* Template Name: Rapid - v2.1.0
	* Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->
</head>
<body>
	<!-- ======= Header ======= -->
	<header id="header" class="header-inner-pages">

		<div id="topbar">
			<div class="container">
				<div class="social-links">
					<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
						<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
				</div>
			</div>
		</div>

		<div class="container">

		<div class="logo float-left">
			<!-- Uncomment below if you prefer to use an image logo -->
			<h1 class="text-light"><a href="index.php" class="scrollto"><span>Comunitec32k</span></a></h1>
			<!-- <a href="index.html" class="scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a> -->
		</div>

		<nav class="main-nav float-right d-none d-lg-block">
			<ul>
				<li><a href="index.php #services">Cursos</a></li>
				<li><a href="index.php #services">Eventos</a></li>
				<li><a href="Registro.php">Resgístrarse</a></li>
				<li><a href="login.php">Staff</a></li>
			</ul>
		</nav><!-- .main-nav -->
		</div>
						
	</header><!-- #header -->			
						
	<div class="container">
		<img src="assets/img/comunitec32k-logo.png" class="img-fluid">
		<p>Main del portal de Comunitec</p>
	</div>
	
	<div class="container">
<?php
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
			
		//print_r($_SESSION); //En caso de revision descomentar el print_r	
?>		
	</div>					
	<!-- ======= Agregar cursos, eventos e instructores ======= -->					
	<section id="services" class="services section-bg">
	<div class="container">
	  
		<header class="section-header">
			<h3> Cursos / Eventos </h3>
		</header>
		 <br>
		<div class="row">
		
			<!--Agregar curso-->
			<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
				<div class="box">
					<h3>Agregar curso</h3>
											
					<form method="POST">
						<label>Nombre del curso</label>
						<input type="text" name="nombre_curso" class="form-control" required />
						 <br>
						<label>Subtítulo</label>
						<input type="text" name="sub_titulo_curso" class="form-control" required />
						 <br> 
						<label>Descripción</label>
						<textarea class="form-control" name="descripcion_curso" rows="5" data-rule="required" data-msg="Necesita una breve descripción" ></textarea>
						 <br>
						<label>Duración</label>
						<input type="text" name="duracion_curso" class="form-control" required />
						 <br>
						<label>Empieza el:</label>
						<input type="date" name="inicio_curso" class="form-control" required />
						 <br>
						<label>Finaliza el:</label>
						<input type="date" name="cierre_curso" class="form-control" required />
						 <br> 
						<label><b>Horario</b></label><br>
						<label>Hora de llegada</label>
						<select name="hora_llegada_h" class="form-control" required>
							<option value="1:00">1:00</option>
							<option value="2:00">2:00</option>
							<option value="3:00">3:00</option>
							<option value="4:00">4:00</option>
							<option value="5:00">5:00</option>
							<option value="6:00">6:00</option>
							<option value="7:00">7:00</option>
							<option value="8:00">8:00</option>
							<option value="9:00">9:00</option>
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
						</select>
						<select name="hora_llegada_p" class="form-control" required>
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>
						<label>Hora de salida</label>
						<select name="hora_salida_h" class="form-control" required>
							<option value="1:00">1:00</option>
							<option value="2:00">2:00</option>
							<option value="3:00">3:00</option>
							<option value="4:00">4:00</option>
							<option value="5:00">5:00</option>
							<option value="6:00">6:00</option>
							<option value="7:00">7:00</option>
							<option value="8:00">8:00</option>
							<option value="9:00">9:00</option>
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
						</select>
						<select name="hora_salida_p" class="form-control" required>
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>					
						<label>Dias de clase</label><br>
							<input type="checkbox" id="dc_lunes" name="dias_c[]" value="Lunes">
							<label for="dc_lunes"> Lunes</label>
							 <br>
							<input type="checkbox" id="dc_martes" name="dias_c[]" value="Martes">
							<label for="dc_martes"> Martes</label>
							 <br>
							<input type="checkbox" id="dc_miercoles" name="dias_c[]" value="Miercoles">
							<label for="dc_miercoles"> Miercoles</label>
							 <br>
							<input type="checkbox" id="dc_jueves" name="dias_c[]" value="Jueves">
							<label for="dc_jueves"> Jueves</label>
							 <br>
							<input type="checkbox" id="dc_viernes" name="dias_c[]" value="Viernes">
							<label for="dc_viernes"> Viernes</label>	
							 <br>
							<input type="checkbox" id="dc_sabado" name="dias_c[]" value="Sabado">
							<label for="dc_sabado"> Sabado</label>	
							 <br>
							<input type="checkbox" id="dc_domingo" name="dias_c[]" value="Domingo">
							<label for="dc_domingo"> Domingo</label>								
						 <br>						 
						<label>Nivel</label>
						 <br>
						<select name="nivel_curso" class="form-control" required >
							<option value="Unico">Unico</option>
							<option value="Basico">Basico</option>
							<option value="Intermedio">Intermedio</option>
							<option value="Avanzado">Avanzado</option>
						</select>	
						 <br>		
						<label>Impartido por:</label>
						<select name="organizacion" class="form-control" required >
							<option value="Cenaltec">Cenaltec</option>
							<option value="Icatech">Icatech</option>
						</select>	
						 <br>		
						<label>Entrega constancia con validez ofical</label>
						 <br>
						<select name="certificado" class="form-control" required >
							<option value="Si">Si</option>
							<option value="No">No</option>
						</select>	
						 <br>
						<label><b>Pagos</b></label><br> 
						<label>Pago unico</label>
						<input type="number" name="pago_unico_curso" class="form-control" required>
						 <br>
						<label>Pago semanal</label>
						<input type="number" name="pago_semanal_curso" class="form-control" required>						 
						 <br>
						<label>Costo inscripción</label>
						<select name="tendra_costo_ins" class="form-control" >
							<option value="No">No</option>
							<option value="Si">Si</option>
						</select>
						 <br>
						<label>Precio de inscripción</label>
						<input type="number" name="pago_inscripcion_curso" class="form-control" >
						 <br>	
						<label hidden>Imagen</label>
						<input type="text" name="imagen" class="form-control" hidden />						
						 <br>
							
						 <br> 
						<input type="submit" name="agregar_curso" value="Agregar" >
					</form>  
				</div>	
			</div><!--Fin agregar curso-->
			 <br>
	 
			<!--Agregar evento-->
			<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
				<div class="box">
					<h3>Agregar evento</h3>
										
					<form method="POST">
						<label>Nombre del evento</label>
						<input type="text" name="nombre_evento" class="form-control" required />
						<br>
						<label>Subtítulo</label>
						<input type="text" name="sub_titulo_evento" class="form-control"  />
						<br> 											 
						<label>Descripción</label>
						<textarea class="form-control" name="descripcion_evento" rows="5" data-rule="required" data-msg="Necesita una breve descripción" ></textarea>
						<br>
						<label>Duración</label>
						<select type="text" name="duracion_evento" class="form-control" required />
							<option value="1:00 hr">1:00 hr</option>
							<option value="1:30 hr">1:30 hr</option>
							<option value="2:00 hrs">2:00 hrs</option>
							<option value="2:30 hrs">2:30 hrs</option>
							<option value="3:00 hrs">3:00 hrs</option>
							<option value="3:30 hrs">3:30 hrs</option>
							<option value="4:00 hrs">4:00 hrs</option>
							<option value="4:30 hrs">4:30 hrs</option>
							<option value="5:00 hrs">5:00 hrs</option>
							<option value="5:30 hrs">5:30 hrs</option>
							<option value="6:00 hrs">6:00 hrs</option>
							<option value="6:30 hrs">6:30 hrs</option>
							<option value="7:00 hrs">7:00 hrs</option>
							<option value="7:30 hrs">7:30 hrs</option>
							<option value="8:00 hrs">8:00 hrs</option>
							<option value="8:30 hrs">8:30 hrs</option>
							<option value="9:00 hrs">9:00 hrs</option>
							<option value="9:30 hrs">9:30 hrs</option>
							<option value="10:00 hrs">10:00 hrs</option>
							<option value="10:30 hrs">10:30 hrs</option>
							<option value="11:00 hrs">11:00 hrs</option>
							<option value="11:30 hrs">11:30 hrs</option>
							<option value="12:00 hrs">12:00 hrs</option>
							<option value="12:30 hrs">12:30 hrss</option>
						</select>
						<br> 
						<label>Empieza el:</label>
						<input type="date" name="fecha_inicio" class="form-control" required />
						<br>
						<label>Finaliza el:</label>
						<input type="date" name="fecha_cierre" class="form-control" required />
						<br>
						<label>Hora de inicio</label>
						<select name="hora_inicio_h" class="form-control" required />
							<option value="1:00">1:00</option>
							<option value="1:30">1:30</option>
							<option value="2:00">2:00</option>
							<option value="2:30">2:30</option>
							<option value="3:00">3:00</option>
							<option value="3:30">3:30</option>
							<option value="4:00">4:00</option>
							<option value="4:30">4:30</option>
							<option value="5:00">5:00</option>
							<option value="5:30">5:30</option>
							<option value="6:00">6:00</option>
							<option value="6:30">6:30</option>
							<option value="7:00">7:00</option>
							<option value="7:30">7:30</option>
							<option value="8:00">8:00</option>
							<option value="8:30">8:30</option>
							<option value="9:00">9:00</option>
							<option value="9:30">9:30</option>
							<option value="10:00">10:00</option>
							<option value="10:30">10:30</option>
							<option value="11:00">11:00</option>
							<option value="11:30">11:30</option>
							<option value="12:00">12:00</option>
							<option value="12:30">12:30</option>	
						</select>						
						<select name="hora_inicio_p" class="form-control">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>	
						<label>Persona / Oragnización que lo imparten</label>
						<input type="text" name="PO_imparten" class="form-control" required />
						 <br>  
						<input type="submit" name="agregar_evento" value="Agregar">
					</form>
				</div>	
			</div><!--Fin agregar evento-->
		  
			<!--Agregar instructor -->
			<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
				<div class="box">
					<h3>Agregar Instructor</h3>

					<form method="POST">
					
						<label>Nombre</label>
						<input type="text" name="nombre_ins" class="form-control" required />
						 <br>
						<label>Apellido Paterno</label> 
						<input type="text" name="apellido_pa_ins" class="form-control" required />
						 <br>
						<label>Apellido Materno</label> 
						<input type="text" name="apellido_ma_ins" class="form-control" required />
						 <br>
						<label hidden>CURP</label> 
						<input type="text" name="CURP" class="form-control"  hidden />
						 <br>
						<label>Correo electronico</label> 
						<input type="email" name="email" class="form-control"  />
						 <br>	
						<input type="submit" name="agregar_intructor" value="Agregar Instructor" /> 
					</form>
			    
				</div>	
			</div><!--Fin actualizar curso o evento-->
		  
		  
		</div>  
	</div> 
	</section><!-- Fin de la seccion para modificar o agregar cursos y eventos-->

	<!-- ======= Seccion para modificar y consultar datos ======= -->	
	<section id="consultas" class="services section-bg">
	    
		<header class="section-header">
			<h3>Consultar Datos </h3>
		</header>
		
		<div class="container">
			<p>
			<h4>Acerca de cursos </h4>
			<a href="consultar_cursos.php"><input type="button" value="Consultar Cursos"></a>
			<a href="consultar_estudiantes_cenaltec.php"><input type="button" value="Estudiantes Cenaltec"></a>
			<a href="consultar_estudiantes_icatech.php"><input type="button" value="Estudiantes Icatech"></a>
			</p><p>
			<h4>Acerca de eventos </h4>
			<a href="consultar_eventos.php"><input type="button" value="Consultar Eventos"></a>
			<a href="consultar_inscritos_evento.php"><input type="button" value="Consultar personas inscritas a eventos"></a>
			</p><p>
			<h4>Acerca de visitas </h4>
			<a href="consultar_visitas.php"><input type="button" value="Consultar Visitas"></a>		
			<a href="consultar_motivos_visita.php"><input type="button" value="Consultar Motivos Visita"></a>	
			</p><p>
			<h4>Acerca de otros </h4>
			<a href="consultar_instructores.php"><input type="button" value="Consultar Instructores"></a>
			<a href="consultar_usuarios.php"><input type="button" value="Consultar Usuarios"></a>		
			</p>
		</div>
		 <br>
							
	</section>
	 <br>

	<!-- ======= Seccion para modificar la seccion "sobre nosotros" ======= -->	
	<section id="seccion_sobre_nosotros" class="services section-bg">
	    
		<header class="section-header">
			<h3>Sobre Nostros</h3>
		</header>

		<form method="POST">
		
		<div class="container">
			<label>Titulo</label>
			<input type="text" name="titulo_sn" class="form-control" placeholder="Ej. Sobre Nosotros" required />
		</div>
		 <br>
		<div class="container">
			<label>Subtitulo</label>
			<input type="text" name="subtitulo_sn" class="form-control" >
		</div>
		 <br>
		<div class="container">
			<label>Parrafo #1</label>
			<textarea name="parrafo1_sn" class="form-control"></textarea>
		</div>
		 <br>
		<div class="container">
			<label>Parrafo #2</label>
			<textarea name="parrafo2_sn" class="form-control"></textarea>
		</div>
		 <br>
		<div class="container">
			<label>Punto #1</label>
			<input type="text" name="punto1_sn" class="form-control" required>
		</div>
		 <br>	
		<div class="container">
			<label>Punto #2</label>
			<input type="text" name="punto2_sn" class="form-control" required>
		</div>
		 <br>
		<div class="container">
			<label>Punto #3</label>
			<input type="text" name="punto3_sn" class="form-control" required>
		</div>
		 <br>
		<div class="container">
			<label>Parrafo #3</label>
			<textarea name="parrafo3_sn" class="form-control"></textarea>
		</div>
		 <br> 
		<div class="container">
			<label>Imagen</label>
			<input type="text" name="img_sn" class="form-control" />	
		</div>	
		 <br>
		<div class="container"> 
			<input type="submit" name="visualizar" value="Visualizar"> 
			<input type="submit" name="aplicar" value="Aplicar">
		</div>	
		</form>
		
		   <section id="about" class="about">

		  <div class="container">
		  <h3>Visualización</h3>
			<div class="row">

			  <div class="col-lg-5 col-md-6">
				<div class="about-img">
				  <img src="<?php echo $img_sn ?>" alt="">
				</div>
			  </div>

			  <div class="col-lg-7 col-md-6">
				<div class="about-content">
				  <h2><?php echo $titulo_sn ?></h2>
				  <h3><?php echo $subtitulo_sn ?></h3>
				  <p><?php echo $parrafo1_sn ?></p>
				  <p><?php echo $parrafo2_sn ?></p>
				  <ul>
					<li><i class="ion-android-checkmark-circle"></i> <?php echo $punto1_sn ?></li>
					<li><i class="ion-android-checkmark-circle"></i> <?php echo $punto2_sn ?> </li>
					<li><i class="ion-android-checkmark-circle"></i> <?php echo $punto3_sn ?> </li>
				  </ul>
				  <p><?php echo $parrafo3_sn ?></p>
				</div>
			  </div>
			</div>
		  </div>

		</section><!-- Fin sección sobre nosotros -->
		
	</section>
	 <br>
	<!-- ======= Footer ======= -->
	<footer id="footer" class="section-bg">
		<div class="footer-top">


			<div class="container">
				<div class="copyright">
					&copy; Copyright <strong>Rapid</strong>. All Rights Reserved
					</div>
				<div class="credits">
			
					<!--
					All the links in the footer should remain intact.
					You can delete the links only if you purchased the pro version.
					Licensing information: https://bootstrapmade.com/license/
					Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
					-->
				
					Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
				</div>
			</div>
		</div>	
	</footer><!-- End  Footer -->

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/counterup/counterup.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
	<script src="assets/vendor/wow/wow.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>
	
</body>
</html>