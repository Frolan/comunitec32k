<?php

	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();
	
	//Istaciacion de clases
	$no_de_visita = new Visitas();
	$correo_telefono = new Visitas();
	$fecha_solicito_visita = new Visitas();
	$descripcion = new Visitas();
	
	//DB connection
	$conectado = 1;
	
	//statement
	$visitas = "SELECT * FROM comunitec_tbl_registro_visitas";
	$stmt = $link->query($visitas);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$no_de_visita->no_de_visita = htmlentities($row['no_de_visita']);
		$correo_telefono->correo_telefono = htmlentities($row['correo_telefono']);
		$fecha_solicito_visita->fecha_solicito_visita = htmlentities($row['fecha_solicito_visita']);
		$descripcion->descripcion = htmlentities($row['descripcion']);


		$arrNo_de_visita[] = ($no_de_visita->no_de_visita);
		$arrCorreo_telefono[] = ($correo_telefono->correo_telefono);
		$arrFecha_solicito_visita[] = ($fecha_solicito_visita->fecha_solicito_visita);
		$arrDescripcion[] = ($descripcion->descripcion);

		$cant_reg++;	
	}	
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Visitas Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">
  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

<!--Tabla de usaurios -->
<br>
	<div class="container">
		<h1><center>Consulta de Visitas</center></h1>
		<br>
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>No. de visita</th>
				<th scope="col">Correo-Telefono</th>
				<th scope="col">Fecha Solicitud Visita</th>
				<th scope="col">Motivo de visita</th>
			</tr>
	</thead>
			
		<?php
if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrDescripcion as $visita){
					$id = $arrNo_de_visita[$rowCount];
					$tel = $arrCorreo_telefono[$rowCount];
					$fec = $arrFecha_solicito_visita[$rowCount];

					echo '<tr>';
						echo '<th scope="row" hidden>'.$id.'</th>';
						echo '<td>'.$tel.'</td>';
						echo '<td>'.$fec.'</td>';
						echo '<td>'.$visita.'</td>';
			
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay visitas registrados</p>';
				echo '</div>';	
			}
		?>
	</table>
	</div>
	  <br>
	<div class = "container">
		<a href= "main.php"><button type="button">Regresar</button></a>
	</div>	
<br>

  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>