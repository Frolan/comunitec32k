<?php
    session_start();

    require_once "conexion.php";
	require_once "clases.php";

	UNSET($_SESSION['entrada']);
	UNSET($_SESSION['nombre_curso']);

	/*==========CURSOS==========*/
	
	///Instaciacion de clases
	$idCurso = new Cursos();
	$nombre_curso = new Cursos();
	$sub_titulo_curso = new Cursos();
	
	//statement
	$cursos_activos = "SELECT * FROM comunitec32k_cursos WHERE IsActive = 1 ORDER BY nombre_curso ASC";
	$stmt_cursos = $link->query($cursos_activos);
	$cant_reg_cursos = 0;
	while($row = $stmt_cursos->fetch(PDO::FETCH_ASSOC)){
		$idCurso->idCurso = htmlentities($row['idCurso']);
		$nombre_curso->nombre_curso = htmlentities($row['nombre_curso']);
		$sub_titulo_curso->sub_titulo_curso = htmlentities($row['sub_titulo_curso']);		
	
		$arrIdCurso[] = ($idCurso->idCurso);
		$arrNombre_curso[] = ($nombre_curso->nombre_curso );
		$arrSub_titulo_curso[] = ($sub_titulo_curso->sub_titulo_curso);
		
		$cant_reg_cursos++;
	}
	
	/*==========FIN DE CURSOS==========*/
	/*==========EVENTOS==========*/
	
	///Instaciacion de clases
	$nombre_evento = new Eventos();
	$sub_titulo_evento = new Eventos();
	
	//statement
	$eventos_activos = "SELECT * FROM comunitec32k_eventos WHERE isActive = 1 ORDER BY nombre_evento ASC";
	$stmt_eventos = $link->query($eventos_activos);
	$cant_reg_eventos = 0;
		while($row = $stmt_eventos->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$nombre_evento->nombre_evento = htmlentities($row['nombre_evento']);
		$sub_titulo_evento->sub_titulo_evento = htmlentities($row['sub_titulo_evento']);	

		$arrNombre_evento[] = ($nombre_evento->nombre_evento);
		$arrSub_titulo_evento[] = ($sub_titulo_evento->sub_titulo_evento);		
		
		$cant_reg_eventos++;	
	}		
	/*==========FIN DE CURSOS==========*/
	
	/*==========SOBRE NOSOTROS==========*/
	
	///Instaciacion de clases
	$id_sb = new sobre_nosotros();
	$titulo_sn = new sobre_nosotros();
	$subtitulo_sn = new sobre_nosotros();
	$parrafo1_sn = new sobre_nosotros();
	$parrafo2_sn = new sobre_nosotros();
	$punto1_sn = new sobre_nosotros();
	$punto2_sn = new sobre_nosotros();
	$punto3_sn = new sobre_nosotros();
	$parrafo3_sn = new sobre_nosotros();
	$img_sn = new sobre_nosotros();
	
	//statement
	$qry_sn = "SELECT * FROM comunitec32k_sobre_nosotros";
	$stmt_sn = $link->query($qry_sn);
	$cant_reg_sn = 0;
		while($row = $stmt_sn->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_sb->id_sb = htmlentities($row['id_sb']);
		$titulo_sn->titulo_sn = htmlentities($row['titulo_sn']);
		$subtitulo_sn->subtitulo_sn = htmlentities($row['subtitulo_sn']);
		$parrafo1_sn->parrafo1_sn = htmlentities($row['parrafo1_sn']);
		$parrafo2_sn->parrafo2_sn = htmlentities($row['parrafo2_sn']);
		$punto1_sn->punto1_sn = htmlentities($row['punto1_sn']);
		$punto2_sn->punto2_sn = htmlentities($row['punto2_sn']);
		$punto3_sn->punto3_sn = htmlentities($row['punto3_sn']);
		$parrafo3_sn->parrafo3_sn = htmlentities($row['parrafo3_sn']);
		$img_sn->img_sn = htmlentities($row['img_sn']);
	
		$arrId_sb[] = ($id_sb->id_sb);
		$arrTitulo_sn[] = ($titulo_sn->titulo_sn);
		$arrSubtitulo_sn[] = ($subtitulo_sn->subtitulo_sn);
		$arrParrafo1_sn[] = ($parrafo1_sn->parrafo1_sn);
		$arrParrafo2_sn[] = ($parrafo2_sn->parrafo2_sn);
		$arrPunto1_sn[] = ($punto1_sn->punto1_sn);
		$arrPunto2_sn[] = ($punto2_sn->punto2_sn);
		$arrPunto3_sn[] = ($punto3_sn->punto3_sn);
		$arrParrafo3_sn[] = ($parrafo3_sn->parrafo3_sn);
		$arrImg_sn[] = ($img_sn->img_sn);
	
		
		$cant_reg_sn++;	
		}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/logo_comunitec_Style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header-inner-pages">

    <div id="topbar">
      <div class="container">
        <div class="social-links">
          <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
		  <a href="https://www.facebook.com/comunitec32K" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
		  <a href="https://www.instagram.com/comunidadtecnologicadel/" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
        </div>
      </div>
    </div>

    <div class="container">

      <div class="logo float-left">
        <!-- Uncomment below if you prefer to use an image logo -->
        <h1 class="text-light"><a href="index.php" class="scrollto"><span>Comunitec32k</span></a></h1>
        <!-- <a href="index.html" class="scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a> -->
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li><a href="#services">Cursos</a></li>
          <li><a href="#services">Eventos</a></li>
		  <li><a href="registro.php">Resgístrarse</a></li>
		  <li><a href="login.php">Staff</a></li>
        </ul>
      </nav><!-- .main-nav -->

    </div>
  </header><!-- #header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="clearfix">
    <div class="container d-flex h-100">
      <div class="row justify-content-center align-self-center">
        <div class="col-md-6 intro-info order-md-first order-last">
          <h2>#Thei4Powerhouse <span></span></h2>
          <div>
            <a href="#about" class="btn-get-started scrollto">Empecemos</a>
          </div>
        </div>

        <div class="col-md-6 intro-img order-md-last order-first">
          <img src="assets/img/comunitec32k-logo.png" alt="" class="img-hero">
        </div>
      </div>

    </div>
  </section><!-- End Hero -->
	
	<div class="container">
				<?php
					if( isset($_SESSION['registro']) ){
						echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
						unset($_SESSION['registro']);
					}
				?>
	</div>			
  <main id="main">

    <!-- ======= Sección sobre nosotros ======= -->
    <section id="about" class="about">


      <div class="container">
        <div class="row">

		<?php //Seccion para que aparescan los cursos activos en la BD
		if($cant_reg_sn > 0){
			$rowCount_sn=0;
			foreach ($arrTitulo_sn as $titulo){	
						$id_sn = $arrId_sb[$rowCount_sn];
						$sub_sn = $arrSubtitulo_sn[$rowCount_sn];
						$p1_sn = $arrParrafo1_sn[$rowCount_sn];
						$p2_sn = $arrParrafo2_sn[$rowCount_sn];
						$pn1_sn = $arrPunto1_sn[$rowCount_sn];
						$pn2_sn = $arrPunto2_sn[$rowCount_sn];
						$pn3_sn = $arrPunto3_sn[$rowCount_sn];
						$p3_sn = $arrParrafo3_sn[$rowCount_sn];
						$img_sn = $arrImg_sn[$rowCount_sn];
						
				  echo '<div class="col-lg-5 col-md-6">';
					echo '<div class="about-img">';
					  echo '<img src="assets/img/interior.jpg" alt="">';
					echo '</div>';
				  echo '</div>';

				  echo '<div class="col-lg-7 col-md-6">';
					echo '<div class="about-content">';
					  echo '<h2>'.$titulo.'</h2>';
					  echo '<h3>'.$sub_sn.'</h3>';
					  echo '<p>'.$p1_sn.'</p>';
					  echo '<p>'.$p2_sn.'</p>';
					  echo '<ul>';
						echo '<li><i class="ion-android-checkmark-circle"></i>'.$pn1_sn.'</li>';
						echo '<li><i class="ion-android-checkmark-circle"></i>'.$pn2_sn.'</li>';
						echo '<li><i class="ion-android-checkmark-circle"></i>'.$pn3_sn.'</li>';
					  echo '</ul>';
					  echo '<p>'.$p2_sn.'</p>';
					echo '</div>';
				  echo '</div>';						
								
						$rowCount_sn ++;
			}	
		}else{
				echo '<div class="">';
					echo '<p>No hay Datos registrados</p>';
				echo '</div>';	
		}						
		?>


        </div>
      </div>

    </section><!-- Fin sección sobre nosotros -->

	 <!-- ======= Sección cursos  ======= -->
	 <section id="services" class="services section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Cursos</h3>
        </header>

        <div class="row">

				<?php //Seccion para que aparescan los cursos activos en la BD
				if($cant_reg_cursos > 0){
					$rowCount_cursos=0;
					foreach ($arrNombre_curso as $curso){	
						$id_curso = $arrIdCurso[$rowCount_cursos];
						$titulo = $arrSub_titulo_curso[$rowCount_cursos];
							
							echo '<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">';
								echo '<div class="box">';
								  echo '<div class="icon" style="background: #fceef3;"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>';
								  echo '<h4 class="title">'.$curso.'</h4>';
								  echo '<p class="description">'.$titulo.'</p>';
								  echo "<a href='ver_informacion_curso.php?Id_curso=".$id_curso."'>
											<button>Ver Informacion</button>
										</a>";
								  echo "<a href='redireccion.php?Nombre_curso=".$curso."'>
											<button>Inscribirse a curso </button>
										</a>";		
								echo '</div>';
							echo '</div>';
							
						$rowCount_cursos ++;
					}	
				}else{
						echo '<div class="">';
							echo '<p>No hay cursos registrados</p>';
						echo '</div>';	
				}						
				?>

      </div>
    </section><!-- Finde de sección cursos  -->
	
	<!-- ======= Sección eventos  ======= -->
	<section id="services" class="services section-bg">
      <div class="container">

        <header class="section-header">
          <h3>Eventos</h3>
        </header>

        <div class="row">

				<?php //Seccion para que aparescan los cursos activos en la BD
				if($cant_reg_eventos > 0){
							$rowCount_eventos=0;
					foreach ($arrNombre_evento as $evento){
					$titulo = $arrSub_titulo_evento[$rowCount_eventos];
							
							echo '<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">';
								echo '<div class="box">';
								  echo '<div class="icon" style="background: #fceef3;"><i class="ion-ios-analytics-outline" style="color: #ff689b;"></i></div>';
								  echo '<h4 class="title">'.$evento.'</h4>';
								  echo '<p class="description">'.$titulo.'</p>';
								  echo "<a href='ver_informacion_evento.php?Nombre_evento=".$evento."'>
											<button>Ver Informacion</button>
										</a>";
								  echo "<a href='formulario_evento.php?Nombre_evento=".$evento."'>
											<button>Inscribirse a evento </button>
										</a>";										
								echo '</div>';
							echo '</div>';
							
						$rowCount_eventos ++;
					}	
				}else{
						echo '<div class="">';
							echo '<p>No hay eventos registrados</p>';
						echo '</div>';	
				}						
				?>

		</div>
    </section><!-- Fin sección eventos -->

    <!-- ======= Clients Section ======= -->
    <section id="clients" class="clients wow fadeInUp">
      <div class="container">

        <header class="section-header">
          <h3>Aliados estratégicos</h3>
        </header>

        <div class="owl-carousel clients-carousel">
          <img src="assets/img/clients/Logo-Cenaltec.png" alt="">
          <img src="assets/img/clients/Logo-ICHEA.jpg" alt="">
          <img src="assets/img/clients/TTCO_NEUTRONA LOGOS.png" alt="">

        </div>

      </div>
    </section><!-- End Clients Section -->    

   

   
  </main><!-- End #main -->


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>