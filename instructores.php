<?php
require_once 'conexion.php';
session_start();
//var_dump($_POST);

if($_SESSION['entrada'] !== 1){
	$_SESSION['error'] = 'Favor de iniciar sesion';
	header('Location: login.php'); 
	return;
}

$failure = false;
$_SESSION['success'] = false;

//Agregar evento a BD 
if(isset($_POST['agregar_evento'])){
	try{
		$nombre_evento = htmlentities(trim($_POST['nombre_evento']));
		$sub_titulo_evento = htmlentities(trim($_POST['sub_titulo_evento']));
		$descripcion_evento = htmlentities(trim($_POST['descripcion_evento']));
		$duracion_evento = htmlentities(trim($_POST['duracion_evento']));
		$fecha_inicio = htmlentities(trim($_POST['fecha_inicio']));
		$fecha_cierre = htmlentities(trim($_POST['fecha_cierre']));
		$hora_inicio_h = htmlentities(trim($_POST['hora_inicio_h']));
		$hora_inicio_p = htmlentities(trim($_POST['hora_inicio_p']));
		$PO_imparten = htmlentities(trim($_POST['PO_imparten']));
		$isActive = 1;
		
		$qry_evento = "INSERT INTO comunitec32k_eventos
							(nombre_evento,sub_titulo_evento,descripcion_evento,duracion_evento,fecha_inicio,fecha_cierre,hora_inicio_evento_h,hora_inicio_evento_p,PO_imparten, isActive)
							VALUES (:nombre_even,:sub_titulo, :descripcion_e, :duracion_hora, :fecha_i, :fecha_c, :inicio_hora, :inicio_pe, :imparte, :activo)";
		
		$stmt_eventos = $link->prepare($qry_evento);
		$stmt_eventos->execute(array(
					':nombre_even' => $nombre_evento,
					':sub_titulo' => $sub_titulo_evento,
					':descripcion_e' => $descripcion_evento,
					':duracion_hora' => $duracion_evento,
					':fecha_i' => $fecha_inicio,
					':fecha_c' => $fecha_cierre,
					':inicio_hora' => $hora_inicio_h,
					':inicio_pe' => $hora_inicio_p,
					':imparte' => $PO_imparten,
					':activo' => $isActive
					)
				);	
		
		$_SESSION['success'] = "Evento agregado exitosamente";	
		
		/*header('Location: main.php');
		return;	*/		
					
	}catch(Exception $ex){
		echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
			echo '<h3> Error : '.$ex->getMessage().'</h3>';
			return;
	}
}

//Agregar curso a BD 
if(isset($_POST['agregar_curso'])){
	try{
		$nombre_curso = htmlentities(trim($_POST['nombre_curso']));
		$sub_titulo_curso = htmlentities(trim($_POST['sub_titulo_curso']));
		$descripcion_curso = htmlentities(trim($_POST['descripcion_curso']));
		$duracion_curso = htmlentities(trim($_POST['duracion_curso']));
		$inicio_curso = htmlentities(trim($_POST['inicio_curso']));
		$cierre_curso = htmlentities(trim($_POST['cierre_curso']));
		
		$hora_llegada_h = htmlentities(trim($_POST['hora_llegada_h']));
		$hora_llegada_p = htmlentities(trim($_POST['hora_llegada_p']));
		$hora_salida_h = htmlentities(trim($_POST['hora_salida_h']));
		$hora_salida_p = htmlentities(trim($_POST['hora_salida_p']));
		
		//Procesar datos del check box
		$dias_c = '';
		if(isset($_POST['dias_c'])){
			$dias_c = implode(',  ' , $_POST['dias_c']);
		}
				
		$nivel_curso = htmlentities(trim($_POST['nivel_curso']));
		$organizacion = htmlentities(trim($_POST['organizacion']));
		$certificado = htmlentities(trim($_POST['certificado']));
		
		$pago_unico_curso = htmlentities(trim($_POST['pago_unico_curso']));
		$pago_semanal_curso = htmlentities(trim($_POST['pago_semanal_curso']));
		$tendra_costo_ins = htmlentities(trim($_POST['tendra_costo_ins']));
		$pago_inscripcion_curso = htmlentities(trim($_POST['pago_inscripcion_curso']));
		
		$imagen = htmlentities(trim($_POST['imagen']));
		$isActive = 1;
		
	
		
		$qry_cursos = "INSERT INTO `comunitec32k_cursos`
						(`nombre_curso`, `sub_titulo_curso`, `descripcion_curso`, `duracion_curso`, `inicio_curso`, `cierre_curso`, `hora_llegada_h`, `hora_llegada_p`, `hora_salida_h`, `hora_salida_p`, `dias_c` ,`nivel_curso`, `organizacion`, `certificacion`, `pago_unico_curso`, `pago_semanal_curso`, `tendra_costo_ins`, `pago_inscripcion_curso`, `Imagen`, `IsActive`) 
						VALUES (:nombre_c, :sub_titulo,:descripcion_c,:duracion,:inicio_c,:cierre_c,:h_ll_h,:h_ll_p,:h_s_h,:h_s_p, :dia_c,:nivel_c,:organ,:certifi,:p_u_c,:p_s_c,:t_c_i,:p_i_c,:imagen,:acti)";
		
		$stmt_cursos = $link->prepare($qry_cursos);
		$stmt_cursos->execute(array(
					':nombre_c' => $nombre_curso,
					':sub_titulo' => $sub_titulo_curso,
					':descripcion_c' => $descripcion_curso,
					':duracion' => $duracion_curso,
					':inicio_c' => $inicio_curso,
					':cierre_c' => $cierre_curso,
					':h_ll_h' => $hora_llegada_h,
					':h_ll_p' => $hora_llegada_p,
					':h_s_h' => $hora_salida_h,
					':h_s_p' => $hora_salida_p,
					':dia_c' => $dias_c,
					':nivel_c' => $nivel_curso,
					':organ' => $organizacion,
					':certifi' => $certificado,
					':p_u_c' => $pago_unico_curso,
					':p_s_c' => $pago_semanal_curso,
					':t_c_i' => $tendra_costo_ins,
					':p_i_c' => $pago_inscripcion_curso,
					':imagen' => $imagen,
					':acti' => $isActive
					)
				);	
		
		$_SESSION['success'] = "Curso agregado exitosamente, para agregar los requisitos visite consultar cursos y de click en agregar requisito";	
		
		/*header('Location: main.php');
		return;	*/	
					
	}catch(Exception $ex){
		echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
		echo '<h3> Error : '.$ex->getMessage().'</h3>';
		return;
	}
}

//========Funciones========// 
function generateStrongpassword(){
  /*
  ************************************
  * Based on http://codepad.org/UL8k4aYK *
  ************************************
  */
  //$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
  $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789!@#$%^";
  $pass = array(); //remember to declare $pass as an array
  $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	for ($i = 0; $i < 8; $i++) {
		$n = rand(0, $alphaLength);
		$pass[] = $alphabet[$n];
	}
  return implode($pass); //turn the array into a string    
}

function enviarEmail($subject, $message){
		
	$sender = "linksunshine1@hotmail.com";
	$email = $sender;
	$name2send = "Jaguares Sistema de Control de Usuarios";
	///$mailto = $email.",".$sender;
	$ceo = 'linksunshine3@hotmail.com';
	$mailto = $ceo.",".$sender;
	///$mailto = $ceo;
		
	$from="From: $name2send<$email>\r\nReturn-path: $sender";
			
	mail($mailto, $subject, $message, $from);	
}

?>
<html>
<head>
			
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Instructores Comunitec32k</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
				
	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/tablas_Style.css" rel="stylesheet">
				
	<!-- =======================================================
	* Template Name: Rapid - v2.1.0
	* Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->
</head>
<body>
	<!-- ======= Header ======= -->
	<header id="header" class="header-inner-pages">

		<div id="topbar">
			<div class="container">
				<div class="social-links">
					<a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
						<a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
				</div>
			</div>
		</div>

		<div class="container">

		<div class="logo float-left">
			<!-- Uncomment below if you prefer to use an image logo -->
			<h1 class="text-light"><a href="index.php" class="scrollto"><span>Comunitec32k</span></a></h1>
			<!-- <a href="index.html" class="scrollto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a> -->
		</div>

		<nav class="main-nav float-right d-none d-lg-block">
			<ul>
				<li><a href="index.php #services">Cursos</a></li>
				<li><a href="index.php #services">Eventos</a></li>
				<li><a href="Registro.php">Resgístrarse</a></li>
				<li><a href="login.php">Staff</a></li>
			</ul>
		</nav><!-- .main-nav -->
		</div>
						
	</header><!-- #header -->			
						
	<div class="container">
		<img src="assets/img/comunitec32k-logo.png" class="img-fluid">
		<p>Main del portal de Comunitec</p>
	</div>
	
	<div class="container">
<?php
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
			
		//print_r($_SESSION); //En caso de revision descomentar el print_r
?>		
	</div>					
	<!-- ======= Agregar cursos, eventos e instructores ======= -->					
	<section id="services" class="services section-bg">
	<div class="container">
	  
		<header class="section-header">
			<h3> Cursos / Eventos </h3>
		</header>
		 <br>
		<div class="row">
		
			<!--Agregar curso-->
			<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
				<div class="box">
					<h3>Agregar curso</h3>
											
					<form method="POST">
						<label>Nombre del curso</label>
						<input type="text" name="nombre_curso" class="form-control" required />
						 <br>
						<label>Subtítulo</label>
						<input type="text" name="sub_titulo_curso" class="form-control" required />
						 <br> 
						<label>Descripción</label>
						<textarea class="form-control" name="descripcion_curso" rows="5" data-rule="required" data-msg="Necesita una breve descripción" ></textarea>
						 <br>
						<label>Duración</label>
						<input type="text" name="duracion_curso" class="form-control" required />
						 <br>
						<label>Empieza el:</label>
						<input type="date" name="inicio_curso" class="form-control" required />
						 <br>
						<label>Finaliza el:</label>
						<input type="date" name="cierre_curso" class="form-control" required />
						 <br> 
						<label><b>Horario</b></label><br>
						<label>Hora de llegada</label>
						<select name="hora_llegada_h" class="form-control" required>
							<option value="1:00">1:00</option>
							<option value="2:00">2:00</option>
							<option value="3:00">3:00</option>
							<option value="4:00">4:00</option>
							<option value="5:00">5:00</option>
							<option value="6:00">6:00</option>
							<option value="7:00">7:00</option>
							<option value="8:00">8:00</option>
							<option value="9:00">9:00</option>
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
						</select>
						<select name="hora_llegada_p" class="form-control" required>
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>
						<label>Hora de salida</label>
						<select name="hora_salida_h" class="form-control" required>
							<option value="1:00">1:00</option>
							<option value="2:00">2:00</option>
							<option value="3:00">3:00</option>
							<option value="4:00">4:00</option>
							<option value="5:00">5:00</option>
							<option value="6:00">6:00</option>
							<option value="7:00">7:00</option>
							<option value="8:00">8:00</option>
							<option value="9:00">9:00</option>
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
						</select>
						<select name="hora_salida_p" class="form-control" required>
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>					
						<label>Dias de clase</label><br>
							<input type="checkbox" id="dc_lunes" name="dias_c[]" value="Lunes">
							<label for="dc_lunes"> Lunes</label>
							 <br>
							<input type="checkbox" id="dc_martes" name="dias_c[]" value="Martes">
							<label for="dc_martes"> Martes</label>
							 <br>
							<input type="checkbox" id="dc_miercoles" name="dias_c[]" value="Miercoles">
							<label for="dc_miercoles"> Miercoles</label>
							 <br>
							<input type="checkbox" id="dc_jueves" name="dias_c[]" value="Jueves">
							<label for="dc_jueves"> Jueves</label>
							 <br>
							<input type="checkbox" id="dc_viernes" name="dias_c[]" value="Viernes">
							<label for="dc_viernes"> Viernes</label>	
							 <br>
							<input type="checkbox" id="dc_sabado" name="dias_c[]" value="Sabado">
							<label for="dc_sabado"> Sabado</label>	
							 <br>
							<input type="checkbox" id="dc_domingo" name="dias_c[]" value="Domingo">
							<label for="dc_domingo"> Domingo</label>								
						 <br>						 
						<label>Nivel</label>
						 <br>
						<select name="nivel_curso" class="form-control" required >
							<option value="Unico">Unico</option>
							<option value="Basico">Basico</option>
							<option value="Intermedio">Intermedio</option>
							<option value="Avanzado">Avanzado</option>
						</select>	
						 <br>		
						<label>Impartido por:</label>
						<select name="organizacion" class="form-control" required >
							<option value="Cenaltec">Cenaltec</option>
							<option value="Icatech">Icatech</option>
						</select>	
						 <br>		
						<label>Entrega constancia con validez ofical</label>
						 <br>
						<select name="certificado" class="form-control" required >
							<option value="Si">Si</option>
							<option value="No">No</option>
						</select>	
						 <br>
						<label><b>Pagos</b></label><br> 
						<label>Pago unico</label>
						<input type="number" name="pago_unico_curso" class="form-control" required>
						 <br>
						<label>Pago semanal</label>
						<input type="number" name="pago_semanal_curso" class="form-control" required>						 
						 <br>
						<label>Costo inscripción</label>
						<select name="tendra_costo_ins" class="form-control" >
							<option value="No">No</option>
							<option value="Si">Si</option>
						</select>
						 <br>
						<label>Precio de inscripción</label>
						<input type="number" name="pago_inscripcion_curso" class="form-control" >
						 <br>	
						<label hidden>Imagen</label>
						<input type="text" name="imagen" class="form-control" hidden />						
						 <br>
							
						 <br> 
						<input type="submit" name="agregar_curso" value="Agregar" >
					</form>  
				</div>	
			</div><!--Fin agregar curso-->
			 <br>
	 
			<!--Agregar evento-->
			<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
				<div class="box">
					<h3>Agregar evento</h3>
										
					<form method="POST">
						<label>Nombre del evento</label>
						<input type="text" name="nombre_evento" class="form-control" required />
						<br>
						<label>Subtítulo</label>
						<input type="text" name="sub_titulo_evento" class="form-control"  />
						<br> 											 
						<label>Descripción</label>
						<textarea class="form-control" name="descripcion_evento" rows="5" data-rule="required" data-msg="Necesita una breve descripción" ></textarea>
						<br>
						<label>Duración</label>
						<select type="text" name="duracion_evento" class="form-control" required />
							<option value="1:00 hr">1:00 hr</option>
							<option value="1:30 hr">1:30 hr</option>
							<option value="2:00 hrs">2:00 hrs</option>
							<option value="2:30 hrs">2:30 hrs</option>
							<option value="3:00 hrs">3:00 hrs</option>
							<option value="3:30 hrs">3:30 hrs</option>
							<option value="4:00 hrs">4:00 hrs</option>
							<option value="4:30 hrs">4:30 hrs</option>
							<option value="5:00 hrs">5:00 hrs</option>
							<option value="5:30 hrs">5:30 hrs</option>
							<option value="6:00 hrs">6:00 hrs</option>
							<option value="6:30 hrs">6:30 hrs</option>
							<option value="7:00 hrs">7:00 hrs</option>
							<option value="7:30 hrs">7:30 hrs</option>
							<option value="8:00 hrs">8:00 hrs</option>
							<option value="8:30 hrs">8:30 hrs</option>
							<option value="9:00 hrs">9:00 hrs</option>
							<option value="9:30 hrs">9:30 hrs</option>
							<option value="10:00 hrs">10:00 hrs</option>
							<option value="10:30 hrs">10:30 hrs</option>
							<option value="11:00 hrs">11:00 hrs</option>
							<option value="11:30 hrs">11:30 hrs</option>
							<option value="12:00 hrs">12:00 hrs</option>
							<option value="12:30 hrs">12:30 hrss</option>
						</select>
						<br> 
						<label>Empieza el:</label>
						<input type="date" name="fecha_inicio" class="form-control" required />
						<br>
						<label>Finaliza el:</label>
						<input type="date" name="fecha_cierre" class="form-control" required />
						<br>
						<label>Hora de inicio</label>
						<select name="hora_inicio_h" class="form-control" required />
							<option value="1:00">1:00</option>
							<option value="1:30">1:30</option>
							<option value="2:00">2:00</option>
							<option value="2:30">2:30</option>
							<option value="3:00">3:00</option>
							<option value="3:30">3:30</option>
							<option value="4:00">4:00</option>
							<option value="4:30">4:30</option>
							<option value="5:00">5:00</option>
							<option value="5:30">5:30</option>
							<option value="6:00">6:00</option>
							<option value="6:30">6:30</option>
							<option value="7:00">7:00</option>
							<option value="7:30">7:30</option>
							<option value="8:00">8:00</option>
							<option value="8:30">8:30</option>
							<option value="9:00">9:00</option>
							<option value="9:30">9:30</option>
							<option value="10:00">10:00</option>
							<option value="10:30">10:30</option>
							<option value="11:00">11:00</option>
							<option value="11:30">11:30</option>
							<option value="12:00">12:00</option>
							<option value="12:30">12:30</option>	
						</select>						
						<select name="hora_inicio_p" class="form-control">
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>	
						<label>Persona / Oragnización que lo imparten</label>
						<input type="text" name="PO_imparten" class="form-control" required />
						 <br>  
						<input type="submit" name="agregar_evento" value="Agregar">
					</form>
				</div>	
			</div><!--Fin agregar evento-->
		  
			<!--Agregar instructor -->
			<div class="col-md-6 col-lg-4 wow bounceInUp" data-wow-duration="1.4s">
				<div class="box">
					<h3>Consultas</h3>
					
					<a href="consultar_cursos.php"><input type="button" value="Consultar Cursos"></a>
					<a href="consultar_eventos.php"><input type="button" value="Consultar Eventos"></a>					
			    
				</div>	
			</div><!--Fin actualizar curso o evento-->
		  
		  
		</div>  
	</div> 
	</section><!-- Fin de la seccion para modificar o agregar cursos y eventos-->

<br>
	<!-- ======= Footer ======= -->
	<footer id="footer" class="section-bg">
		<div class="footer-top">


			<div class="container">
				<div class="copyright">
					&copy; Copyright <strong>Rapid</strong>. All Rights Reserved
					</div>
				<div class="credits">
			
					<!--
					All the links in the footer should remain intact.
					You can delete the links only if you purchased the pro version.
					Licensing information: https://bootstrapmade.com/license/
					Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
					-->
				
					Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
				</div>
			</div>
		</div>	
	</footer><!-- End  Footer -->

	<a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	<!-- Vendor JS Files -->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="assets/vendor/php-email-form/validate.js"></script>
	<script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/counterup/counterup.min.js"></script>
	<script src="assets/vendor/venobox/venobox.min.js"></script>
	<script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
	<script src="assets/vendor/wow/wow.min.js"></script>
	<script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

	<!-- Template Main JS File -->
	<script src="assets/js/main.js"></script>
	
</body>
</html>