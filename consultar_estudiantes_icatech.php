<?php
	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();
	//Istaciacion de clases
	$id_usuario = new Estudiantes_icatech();
	$fecha_actual = new Estudiantes_icatech();
	$nombre_curso = new Estudiantes_icatech();
	$fecha_inicio = new Estudiantes_icatech();
	$fecha_termino = new Estudiantes_icatech();
	$apellido_paterno = new Estudiantes_icatech();
	$apellido_materno = new Estudiantes_icatech();
	$nombre = new Estudiantes_icatech();
	$edad = new Estudiantes_icatech();
	$rfc = new Estudiantes_icatech();
	
	$curp = new Estudiantes_icatech();
	$domicilio = new Estudiantes_icatech();
	$colonia = new Estudiantes_icatech();
	$ciudad = new Estudiantes_icatech();
	$estado = new Estudiantes_icatech();
	$telefono = new Estudiantes_icatech();
	$telefono_trabajo = new Estudiantes_icatech();
	$correo_electronico = new Estudiantes_icatech();
	$fecha_nacimiento = new Estudiantes_icatech();
	$lugar_nacimiento = new Estudiantes_icatech();
	
	$sexo = new Estudiantes_icatech();
	$estado_civil = new Estudiantes_icatech();
	$discapacidad_presenta = new Estudiantes_icatech();
	$grado_estudios = new Estudiantes_icatech();
	$condicion_social = new Estudiantes_icatech();
	$nombre_empresa = new Estudiantes_icatech();
	$antiguedad = new Estudiantes_icatech();
	$direccion_empresa = new Estudiantes_icatech();
	$ocupacion_principal = new Estudiantes_icatech();
	$situacion_actual = new Estudiantes_icatech();
	
	$medio_enterado = new Estudiantes_icatech();
	$razon_eleccion = new Estudiantes_icatech();
	$razon_registro = new Estudiantes_icatech();
	$explicacion_pago = new Estudiantes_icatech();
	$areas_capacitacion = new Estudiantes_icatech();

	
	//statement
	$cenaltec_estudiantes = "SELECT * FROM icatech_tbl_usuarios ";
	$stmt = $link->query($cenaltec_estudiantes);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_usuario->id_usuario = htmlentities($row['id_usuario']);
		$fecha_actual->fecha_actual = htmlentities($row['fecha_actual']);
		$nombre->apellido_paterno = htmlentities($row['apellido_paterno']);
		$nombre->apellido_materno = htmlentities($row['apellido_materno']);
		$nombre->nombre = htmlentities($row['nombre']);
		$edad->edad = htmlentities($row['edad']);
		$rfc->rfc = htmlentities($row['rfc']);
		$nombre_curso->nombre_curso = htmlentities($row['nombre_curso']);
		$fecha_inicio->fecha_inicio = htmlentities($row['fecha_inicio']);
		$fecha_termino->fecha_termino = htmlentities($row['fecha_termino']);
		
		$curp->curp = htmlentities($row['curp']);
		$domicilio->domicilio = htmlentities($row['domicilio']);
		$colonia->colonia = htmlentities($row['colonia']);
		$ciudad->ciudad = htmlentities($row['ciudad']);
		$estado->estado = htmlentities($row['estado']);
		$telefono->telefono = htmlentities($row['telefono']);
		$telefono_trabajo->telefono_trabajo = htmlentities($row['telefono_trabajo']);
		$correo_electronico->correo_electronico = htmlentities($row['correo_electronico']);
		$fecha_nacimiento->fecha_nacimiento = htmlentities($row['fecha_nacimiento']);
		$lugar_nacimiento->lugar_nacimiento = htmlentities($row['lugar_nacimiento']);
		
		$sexo->sexo = htmlentities($row['sexo']);
		$estado_civil->estado_civil = htmlentities($row['estado_civil']);
		$discapacidad_presenta->discapacidad_presenta = htmlentities($row['discapacidad_presenta']);
		$grado_estudios->grado_estudios = htmlentities($row['grado_estudios']);
		$condicion_social->condicion_social = htmlentities($row['condicion_social']);
		$nombre_empresa->nombre_empresa = htmlentities($row['nombre_empresa']);
		$antiguedad->antiguedad = htmlentities($row['antiguedad']);
		$direccion_empresa->direccion_empresa = htmlentities($row['direccion_empresa']);
		$ocupacion_principal->ocupacion_principal = htmlentities($row['ocupacion_principal']);
		$situacion_actual->situacion_actual = htmlentities($row['situacion_actual']);
		
		$medio_enterado->medio_enterado = htmlentities($row['medio_enterado']);
		$razon_eleccion->razon_eleccion = htmlentities($row['razon_eleccion']);
		$razon_registro->razon_registro = htmlentities($row['razon_registro']);
		$explicacion_pago->explicacion_pago = htmlentities($row['explicacion_pago']);
		$areas_capacitacion->areas_capacitacion = htmlentities($row['areas_capacitacion']);
		
		//Arreglos
		$arrId_estudiante_I[] = ($id_usuario->id_usuario);
		$arrFecha[] = ($fecha_actual->fecha_actual);
		$arrNombres_I[] = ($nombre->nombre.' '.$nombre->apellido_paterno.' '.$nombre->apellido_materno);	
		$arrEdad[] = ($edad->edad);	
		$arrRfc[] = ($rfc->rfc);	
		$arrNombre_curso[] = ($nombre_curso->nombre_curso);	
		$arrFecha_inicio[] = ($fecha_inicio->fecha_inicio);	
		$arrFecha_termino[] = ($fecha_termino->fecha_termino);	

		$arrCurp[] = ($curp->curp);
		$arrDomicilio[] = ($domicilio->domicilio);
		$arrColonia[] = ($colonia->colonia);
		$arrCiudad[] = ($ciudad->ciudad);
		$arrEstado[] = ($estado->estado);
		$arrTelefono[] = ($telefono->telefono);
		$arrTelefono_trabajo[] = ($telefono_trabajo->telefono_trabajo);
		$arrCorreo[] = ($correo_electronico->correo_electronico);
		$arrFecha_nacimiento[] = ($fecha_nacimiento->fecha_nacimiento);
		$arrLugar_nacimiento[] = ($lugar_nacimiento->lugar_nacimiento);
				
		$arrSexo[] = ($sexo->sexo);	
		$arrEstado_civil[] = ($estado_civil->estado_civil);	
		$arrDiscapacidad[] = ($discapacidad_presenta->discapacidad_presenta);	
		$arrGrado_estudios[] = ($grado_estudios->grado_estudios);	
		$arrCondicion_social[] = ($condicion_social->condicion_social);	
		$arrNombre_empresa[] = ($nombre_empresa->nombre_empresa);	
		$arrAntiguedad[] = ($antiguedad->antiguedad);	
		$arrDireccion_empresa[] = ($direccion_empresa->direccion_empresa);	
		$arrOcupacion_principal[] = ($ocupacion_principal->ocupacion_principal);	
		$arrSituacion_actual[] = ($situacion_actual->situacion_actual);	
		
		$arrMedio_enterado[] = ($medio_enterado->medio_enterado);
		$arrRazon_eleccion[] = ($razon_eleccion->razon_eleccion);
		$arrRazon_registro[] = ($razon_registro->razon_registro);
		$arrExplicacion_pago[] = ($explicacion_pago->explicacion_pago);
		$arrAreas_capacitacion[] = ($areas_capacitacion->areas_capacitacion);
		
		$cant_reg++;	
	}
	

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Consulta de Registros Icatech</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">	
  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

<!--Tabla de cursos activos -->
	<div class="container">
		<h1><center>Consulta de Registros Icatech</center></h1>
		<br>

	<div class="container">
		<label>Buscar Estudiante</label><br>
		<input type="text" id="nombre" name="nombre">
		<input type="button" id="busca_nombre" name="busca_nombre_usuario" value="Buscar Usuario" onclick="buscar()">
	</div>	
		<br>
	<div class="container" id="datos_elemento">	
	</div>
		<br>

	<div class="container" id="datos_alumnos">	
	</div>
		<br>
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>ID alumno</th>
				<th scope="col">Fecha Inscrito</th>
				<th scope="col">Nombre</th>
				<th scope="col">Edad</th>
				<th scope="col">RFC</th>
				<th scope="col">Curp</th>	
				<th scope="col">Nombre Curso</th>
				<th scope="col">Fecha inicio</th>	
				
				<th scope="col">Fecha termino</th>
				<th scope="col">Domicilio</th>
				<th scope="col">Colonia</th>
				<th scope="col">Ciudad</th>
				<th scope="col">Estado</th>
				<th scope="col">Telefono</th>
				<th scope="col">Estado civil</th>
				<th scope="col">Correo</th>
				<th scope="col">Fecha Nacimiento</th>
				<th scope="col">Lugar Nacimiento</th>
				
				<th scope="col">Sexo</th>
				<th scope="col">Discapacidad</th>
				<th scope="col">Ultimo grado estudios</th>
				<th scope="col">Condicion social</th>
				<th scope="col">Nombre empresa</th>
				<th scope="col">Telefono trabajo</th>
				<th scope="col">Antigüedad</th>
				<th scope="col">Dirección empresa</th>
				<th scope="col">Ocupacion principal</th>
				<th scope="col">Situacion actual</th>
				
				<th scope="col">Medio de enterado</th>
				<th scope="col">Razon de eleccion</th>
				<th scope="col">Razon de registro</th>
				<th scope="col">Persona que cubre el curso</th>
				<th scope="col">Areas de interes</th>
				
				<th scope="col">Actualizar</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>	
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrNombres_I as $nombre){
					$id_usuari = $arrId_estudiante_I[$rowCount];
					$fecha_inscrit = $arrFecha[$rowCount];
					$Eda = $arrEdad[$rowCount];
					$RF = $arrRfc [$rowCount];
					$nombre_curs = $arrNombre_curso [$rowCount];
					$Fecha_inici = $arrFecha_inicio [$rowCount];			
					$Fecha_termin = $arrFecha_termino [$rowCount];
					
					$Cur = $arrCurp [$rowCount];	
					$Domicili = $arrDomicilio [$rowCount];
					$Coloni = $arrColonia [$rowCount];
					$Ciuda = $arrCiudad [$rowCount];		
					$Estad = $arrEstado [$rowCount];
					$Telefon = $arrTelefono [$rowCount];	
					$Telefon_trabaj = $arrTelefono_trabajo [$rowCount];
					$corre = $arrCorreo [$rowCount];
					$Fecha_nacimient = $arrFecha_nacimiento [$rowCount];
					$Lugar_nacimient = $arrLugar_nacimiento [$rowCount];
					
					$Sex = $arrSexo [$rowCount];
					$Estado_civi = $arrEstado_civil [$rowCount];
					$Discapacida = $arrDiscapacidad [$rowCount];
					$Grado_estudio = $arrGrado_estudios [$rowCount];
					$Condicion_socia = $arrCondicion_social [$rowCount];
					$Nombre_empres = $arrNombre_empresa [$rowCount];
					$Antigueda = $arrAntiguedad [$rowCount];
					$Direccion_empres = $arrDireccion_empresa [$rowCount];
					$Ocupacion_principa = $arrOcupacion_principal [$rowCount];
					$Situacion_actua = $arrSituacion_actual [$rowCount];
					
					$Medio_enterad = $arrMedio_enterado [$rowCount];
					$Razon_eleccio = $arrRazon_eleccion [$rowCount];
					$Razon_registro = $arrRazon_registro [$rowCount];
					$Explicacion_pago = $arrExplicacion_pago [$rowCount];
					$Areas_capacitacio = $arrAreas_capacitacion [$rowCount];
					
				
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id_usuari.'</th>';
						echo '<td>'.$fecha_inscrit.'</td>';
						echo '<td>'.$nombre.'</td>';
						echo '<td>'.$Eda.'</td>';
						echo '<td>'.$RF.'</td>';
						echo '<td>'.$Cur.'</td>';
						echo '<td>'.$nombre_curs.'</td>';	
						echo '<td>'.$Fecha_inici.'</td>';
						
						echo '<td>'.$Fecha_termin.'</td>';		
						echo '<td>'.$Domicili.'</td>';
						echo '<td>'.$Coloni.'</td>';
						echo '<td>'.$Ciuda.'</td>';	
						echo '<td>'.$Estad.'</td>';
						echo '<td>'.$Telefon.'</td>';
						echo '<td>'.$Estado_civi.'</td>';
						echo '<td>'.$corre.'</td>';
						echo '<td>'.$Fecha_nacimient.'</td>';
						echo '<td>'.$Lugar_nacimient.'</td>';
						
						echo '<td>'.$Sex.'</td>';
						echo '<td>'.$Discapacida.'</td>';
						echo '<td>'.$Grado_estudio.'</td>';
						echo '<td>'.$Condicion_socia.'</td>';
						echo '<td>'.$Nombre_empres.'</td>';
						echo '<td>'.$Telefon_trabaj.'</td>';
						echo '<td>'.$Antigueda.'</td>';
						echo '<td>'.$Direccion_empres.'</td>';
						echo '<td>'.$Ocupacion_principa.'</td>';
						echo '<td>'.$Situacion_actua.'</td>';
						
						echo '<td>'.$Medio_enterad.'</td>';
						echo '<td>'.$Razon_eleccio.'</td>';
						echo '<td>'.$Razon_registro.'</td>';
						echo '<td>'.$Explicacion_pago.'</td>';
						echo '<td>'.$Areas_capacitacio.'</td>';
						echo 	"<td>
									<a href='modificar_estudiantes_icatech.php?id=".$id_usuari."'>
										<button>Actualizar</button>
									</a>
								</td>";
						echo 	"<td>
									<a href='consultar_estudiantes_icatech_correo.php?id=".$id_usuari."'>
										<button>Enviar Correo</button>
									</a>
								</td>";	
							
					
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay usuarios registrados</p>';
				echo '</div>';	
			}
		?>
		</tbody>
	</table>
	</div>
	 <br>
	<div class = "container">
		<a href= "main.php"><button type="button">Regresar</button></a>
	</div>
<br>	
  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
<script>
	function buscar(){
		elemento = document.getElementById("nombre").value;
		elemento.trim();
		if( elemento != ''){///elemento
			///document.getElementById("datos_usuario").innerHTML = '<p style = "color: green";>Hola '+elemento;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText != ""){						
						document.getElementById("datos_elemento").innerHTML = "<br>"+this.responseText;
					}
				}
			};
			xmlhttp.open("GET","buscar_estudiantes_icatech.php?elemento="+elemento,true);
			xmlhttp.send();			
		}///elemento no está vacío		
	}
</script>
</html>