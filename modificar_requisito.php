<?php
require_once 'conexion.php';
	session_start();

	$id_requisito = $_GET['id'];
	
	$qry = 'SELECT * FROM comunitec32k_requisitos WHERE id_requisito = :id';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id' => $id_requisito)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$idCurso = htmlentities($row['idCurso']);	
	$requisito = htmlentities($row['requisito']);
	
	
	if(isset($_POST['actualizar_requisito'])){
		$n_id_requisito = $_POST['id_requisito'];
		$n_idCurso = $_POST['idCurso'];
		$n_requisito = $_POST['requisito'];

		
			$update = 'UPDATE comunitec32k_requisitos SET 
						idCurso = :id_c,
						requisito = :req		
					WHERE 
						id_requisito = :id_r';
							
						$stmt = $link->prepare($update);
						$stmt->execute(array(
							':id_r' => $n_id_requisito,
							':id_c' => $n_idCurso,
							':req' => $n_requisito	
							)
						);					
		header('Location: agregar_requisitos.php?id_curso='.$idCurso.'');
		return; 
	}
	
	if(isset($_POST['borrar_requisito'])){
		$n_id_requisito = $_POST['id_requisito'];

		
			$delete = 'DELETE FROM comunitec32k_requisitos 
					WHERE 
						id_requisito = :id_r';
					
					$stmt = $link->prepare($delete);
					$stmt->execute(array(
							':id_r' => $n_id_requisito,
							)
						);	
						
		header('Location: agregar_requisitos.php?id_curso='.$idCurso.'');
		return; 			
	}
?>

<html>
<head>

	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Modificar requisito</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
				
	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/tablas_Style.css" rel="stylesheet">
				
	<!-- =======================================================
	* Template Name: Rapid - v2.1.0
	* Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->

</head>
<body>
    <div class="container">
		<header class="section-header">
		   <br>
          <h3> Actualizar requisito </h3>
        </header>
			<form method="POST">
				<label hidden>ID curso: </label>
				<input type="text" name="id_requisito" hidden value ="<?php echo $id_requisito ?>">
				 <br>		
				<label hidden>Id curso</label>
				<input type="text" name="idCurso" class="form-control" required hidden value="<?php echo $idCurso ?>" />
				 <br>
				<label>Requisito</label>
				<input type="text" name="requisito" class="form-control" required  value="<?php echo $requisito ?>" />
				 <br>
				 <br>
				<input type="submit" name="actualizar_requisito" value="Actualizar" >
				<a href="consultar_cursos.php"><button type="button" title="Cancelar">Cancelar</button></a>
				<input type="submit" name="borrar_requisito" value="Borrar">
			</form>  	
	</div>


  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
 
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
  
</body>
</html>