<?php
require_once 'conexion.php';
	session_start();
	
	$id_evento = $_GET['id'];
	
	$qry = 'SELECT * FROM comunitec32k_eventos WHERE id_evento = :id_evento';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id_evento' => $id_evento )
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$nombre_evento = htmlentities($row['nombre_evento']);
	$sub_titulo_evento = htmlentities($row['sub_titulo_evento']);	
	$descripcion_evento = htmlentities($row['descripcion_evento']);
	$duracion_evento = htmlentities($row['duracion_evento']);
	$fecha_inicio = htmlentities($row['fecha_inicio']);
	$fecha_cierre = htmlentities($row['fecha_cierre']);
	$hora_inicio_evento_h = htmlentities($row['hora_inicio_evento_h']);
	$hora_inicio_evento_p = htmlentities($row['hora_inicio_evento_p']);
	$PO_imparten = htmlentities($row['PO_imparten']);
	$isActive = htmlentities($row['isActive']);
	
	
	if(isset($_POST['actualizar_evento'])){
		$n_id_evento = $_POST['id_evento'];
		$n_nombre_evento = $_POST['nombre_evento'];
		$n_sub_titulo_evento = $_POST['sub_titulo_evento'];
		$n_descripcion_evento = $_POST['descripcion_evento'];
		$n_duracion_evento = $_POST['duracion_evento'];
		$n_fecha_inicio = $_POST['fecha_inicio'];
		$n_fecha_cierre = $_POST['fecha_cierre'];
		$n_hora_inicio_evento_h = $_POST['hora_inicio_h'];
		$n_hora_inicio_evento_p = $_POST['hora_inicio_p'];
		$n_PO_imparten = $_POST['PO_imparten'];
		$n_isActive = $_POST['activador_evento'];
		
			$update = 'UPDATE comunitec32k_eventos SET
							id_evento = :id_evento,
							nombre_evento = :nombre_evento,
							sub_titulo_evento = :sub_titulo_evento,
							descripcion_evento = :descripcion_evento,
							duracion_evento = :duracion_evento,
							fecha_inicio = :fecha_inicio,
							fecha_cierre = :fecha_cierre,
							hora_inicio_evento_h = :hora_inicio_evento_h,
							hora_inicio_evento_p = :hora_inicio_evento_p,
							PO_imparten = :PO_imparten,
							isActive = :isActive 
						WHERE 
							id_evento = :id_evento';
							
						$stmt = $link->prepare($update);
						$stmt->execute(array(
							':id_evento' => $n_id_evento,
							':nombre_evento' => $n_nombre_evento,
							':sub_titulo_evento' => $n_sub_titulo_evento,
							':descripcion_evento' => $n_descripcion_evento,
							':duracion_evento' => $n_duracion_evento,
							':fecha_inicio' => $n_fecha_inicio,
							':fecha_cierre' => $n_fecha_cierre,
							':hora_inicio_evento_h' => $n_hora_inicio_evento_h,
							':hora_inicio_evento_p' => $n_hora_inicio_evento_p,
							':PO_imparten' => $n_PO_imparten,
							':isActive' => $n_isActive,
							)
						);					
		header('Location: consultar_eventos.php');
		return; 
	}
	
	if(isset($_POST['borrar'])){
	$n_id_evento = $_POST['id_evento'];

		
			$delete = 'DELETE FROM comunitec32k_eventos 
					WHERE 
						id_evento  = :id';
					
					$stmt = $link->prepare($delete);
					$stmt->execute(array(
							':id' => $n_id_evento,
							)
						);	
						
		header('Location: consultar_eventos.php');
		return; 			
	}		
?>
<html>
<head>

	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Modificar eventos</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
				
	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/tablas_Style.css" rel="stylesheet">
				
	<!-- =======================================================
	* Template Name: Rapid - v2.1.0
	* Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->

</head>
<body>
<div class="container">
		<header class="section-header">
		   <br>
          <h3> Actualizar datos de los eventos </h3>
        </header>
			<form action="" method="POST">
			
				<div class="container">
					<div class="">
						<br>
						<label hidden >ID curso: </label>
						<input type="text" name="id_evento" hidden value ="<?php echo $id_evento ?>" />
					</div>
				</div>
			
				<div class="container">
					<div class="">
						<br>
						<label>Nombre del Evento</label>
						<input type="text" name="nombre_evento" class="form-control" required value ="<?php echo $nombre_evento ?>" />
					</div>
				</div>
				
				<div class="container">
					<div class="">
						<br>
						<label>Subtítulo</label>
						<input type="text" name="sub_titulo_evento" class="form-control" value ="<?php echo $sub_titulo_evento ?>" />
					</div>
				</div>				
			
				<div class="container">
					<div class="">
						<br>
						<label>Descripcion</label>
						<input type="text" class="form-control" name="descripcion_evento" rows="5" data-rule="required" data-msg="Necesita una breve descripción" value ="<?php echo $descripcion_evento ?>" >
					</div>
				</div>
			
				<div class="container">
					<div class="">
						<br>
						<label>Duración</label>
						<select type="text" name="duracion_evento" class="form-control" required value ="<?php echo $duracion_evento ?>" />
							<option value="1:00 hr">1:00 hr</option>
							<option value="1:30 hr">1:30 hr</option>
							<option value="2:00 hrs">2:00 hrs</option>
							<option value="2:30 hrs">2:30 hrs</option>
							<option value="3:00 hrs">3:00 hrs</option>
							<option value="3:30 hrs">3:30 hrs</option>
							<option value="4:00 hrs">4:00 hrs</option>
							<option value="4:30 hrs">4:30 hrs</option>
							<option value="5:00 hrs">5:00 hrs</option>
							<option value="5:30 hrs">5:30 hrs</option>
							<option value="6:00 hrs">6:00 hrs</option>
							<option value="6:30 hrs">6:30 hrs</option>
							<option value="7:00 hrs">7:00 hrs</option>
							<option value="7:30 hrs">7:30 hrs</option>
							<option value="8:00 hrs">8:00 hrs</option>
							<option value="8:30 hrs">8:30 hrs</option>
							<option value="9:00 hrs">9:00 hrs</option>
							<option value="9:30 hrs">9:30 hrs</option>
							<option value="10:00 hrs">10:00 hrs</option>
							<option value="10:30 hrs">10:30 hrs</option>
							<option value="11:00 hrs">11:00 hrs</option>
							<option value="11:30 hrs">11:30 hrs</option>
							<option value="12:00 hrs">12:00 hrs</option>
							<option value="12:30 hrs">12:30 hrss</option>
						</select>
					</div>
				</div>
			
				<div class="container">
					<div class="">
						<br>
						<label>Empieza el:</label>
						<input type="date" name="fecha_inicio" class="form-control" required value ="<?php echo $fecha_inicio ?>" />
					</div>
				</div>
	
				<div class="container">
					<div class="">
						<br>
						<label>Termina el:</label>
						<input type="date" name="fecha_cierre" class="form-control" required value ="<?php echo $fecha_cierre ?>" />
					</div>
				</div>
	
				<div class="container">
					<div class="">
						<br>
						<label>Hora de inicio</label>
						<select name="hora_inicio_h" class="form-control" required value ="<?php echo $hora_inicio_evento_h ?>" />
							<option value="1:00">1:00</option>
							<option value="1:30">1:30</option>
							<option value="2:00">2:00</option>
							<option value="2:30">2:30</option>
							<option value="3:00">3:00</option>
							<option value="3:30">3:30</option>
							<option value="4:00">4:00</option>
							<option value="4:30">4:30</option>
							<option value="5:00">5:00</option>
							<option value="5:30">5:30</option>
							<option value="6:00">6:00</option>
							<option value="6:30">6:30</option>
							<option value="7:00">7:00</option>
							<option value="7:30">7:30</option>
							<option value="8:00">8:00</option>
							<option value="8:30">8:30</option>
							<option value="9:00">9:00</option>
							<option value="9:30">9:30</option>
							<option value="10:00">10:00</option>
							<option value="10:30">10:30</option>
							<option value="11:00">11:00</option>
							<option value="11:30">11:30</option>
							<option value="12:00">12:00</option>
							<option value="12:30">12:30</option>	
						</select>						
						<select name="hora_inicio_p" class="form-control" value ="<?php echo $hora_inicio_evento_p ?>" >
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
					</div>
				</div>

				<div class="container">
					<div class="">
						<br>
						<label>Persona / Oragnización que lo imparten</label>
						<input type="text" name="PO_imparten" class="form-control" required  value ="<?php echo $PO_imparten ?>"/>
					</div>
				</div>				
	
				<div class="container">
					<div class="">
						<br>
						<label>Activar / Desactivar:</label><br>
						<select name="activador_evento" class="form-control"  >
							<option value="1">Activar</option>
							<option value="0">Desactivar</option>
						</select>
					</div>
				</div>
				<br>	
	
				<div class="container">
						<input type="submit" name="actualizar_evento" value="Actualizar" >
						<a href="consultar_eventos.php"><button type="button" title="Cancelar">Cancelar</button></a>
						<input type="submit" name="borrar" value="Borrar">
				</div>		
	
			</form>
</div>
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
 
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
  
</body>
</html>