<?php
require_once 'conexion.php';
	session_start();
	
	$id_curso = $_GET['id_curso'];
	
	$qry = 'SELECT * FROM comunitec32k_cursos WHERE idCurso = :id_curso';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id_curso' => $id_curso)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$nombre_curso = htmlentities($row['nombre_curso']);
	$sub_titulo_curso = htmlentities($row['sub_titulo_curso']);	
	$descripcion_curso = htmlentities($row['descripcion_curso']);
	$duracion_curso = htmlentities($row['duracion_curso']);
	$inicio_curso = htmlentities($row['inicio_curso']);
	$cierre_curso = htmlentities($row['cierre_curso']);
	
	$hora_llegada_h = htmlentities($row['hora_llegada_h']);
	$hora_llegada_p = htmlentities($row['hora_llegada_p']);
	$hora_salida_h = htmlentities($row['hora_salida_h']);
	$hora_salida_p = htmlentities($row['hora_salida_p']);
	$dias_c = htmlentities($row['dias_c']);
	
	$nivel_curso = htmlentities($row['nivel_curso']);
	$organizacion = htmlentities($row['organizacion']);
	$certificacion = htmlentities($row['certificacion']);
	
	$pago_unico_curso = htmlentities($row['pago_unico_curso']);
	$pago_semanal_curso = htmlentities($row['pago_semanal_curso']);
	$tendra_costo_ins = htmlentities($row['tendra_costo_ins']);
	$pago_inscripcion_curso = htmlentities($row['pago_inscripcion_curso']);
	
	$Imagen = htmlentities($row['Imagen']);
	
	
	if(isset($_POST['actualizar_curso'])){
		$n_idCurso = htmlentities(trim($_POST['idCurso']));
		$n_nombre_curso = htmlentities(trim($_POST['nombre_curso']));
		$n_sub_titulo_curso = htmlentities(trim($_POST['sub_titulo_curso']));
		$n_descripcion_curso = htmlentities(trim($_POST['descripcion_curso']));
		$n_duracion_curso = htmlentities(trim($_POST['duracion_curso']));
		$n_inicio_curso = htmlentities(trim($_POST['inicio_curso']));
		$n_cierre_curso = htmlentities(trim($_POST['cierre_curso']));
		
		$n_hora_llegada_h = htmlentities(trim($_POST['hora_llegada_h']));
		$n_hora_llegada_p = htmlentities(trim($_POST['hora_llegada_p']));
		$n_hora_salida_h = htmlentities(trim($_POST['hora_salida_h']));
		$n_hora_salida_p = htmlentities(trim($_POST['hora_salida_p']));
		
		//Procesar datos del check box
		$n_dias_c = '';
		if(isset($_POST['dias_c'])){
			$n_dias_c = implode(',  ' , $_POST['dias_c']);
		}		
		
		$n_nivel_curso = htmlentities(trim($_POST['nivel_curso']));
		$n_organizacion = htmlentities(trim($_POST['organizacion']));
		$n_certificacion = htmlentities(trim($_POST['certificacion']));
		
		$n_pago_unico_curso = htmlentities(trim($_POST['pago_unico_curso']));
		$n_pago_semanal_curso = htmlentities(trim($_POST['pago_semanal_curso']));
		$n_tendra_costo_ins = htmlentities(trim($_POST['tendra_costo_ins']));
		$n_pago_inscripcion_curso = htmlentities(trim($_POST['pago_inscripcion_curso']));
		
		$n_Imagen = $_POST['Imagen'];
		$n_activador_curso = $_POST['activador_curso'];
		
			$update = 'UPDATE comunitec32k_cursos SET 
							nombre_curso = :nombre_curso,
							sub_titulo_curso = :sub_titulo_curso,
							descripcion_curso = :descripcion_curso,
							duracion_curso = :duracion_curso,
							inicio_curso = :inicio_curso,
							cierre_curso = :cierre_curso,
							
							hora_llegada_h = :hora_llegada_h,
							hora_llegada_p = :hora_llegada_p,
							hora_salida_h = :hora_salida_h,
							hora_salida_p = :hora_salida_p,
							dias_c = :dias_c,
							
							nivel_curso = :nivel_curso,
							organizacion = :organizacion,
							certificacion = :certificacion,
							
							pago_unico_curso = :pago_unico_curso,
							pago_semanal_curso = :pago_semanal_curso,
							tendra_costo_ins = :tendra_costo_inicial,
							pago_inscripcion_curso = :pago_inscripcion_curso,	
							
							Imagen = :Imagen,
							IsActive = :IsActive 
						WHERE 
							idCurso = :id_curso';
							
						$stmt = $link->prepare($update);
						$stmt->execute(array(
							':id_curso' => $n_idCurso,
							':nombre_curso' => $n_nombre_curso,
							':sub_titulo_curso' => $n_sub_titulo_curso,
							':descripcion_curso' => $n_descripcion_curso,
							':duracion_curso' => $n_duracion_curso,
							':inicio_curso' => $n_inicio_curso,
							':cierre_curso' => $n_cierre_curso,
							
							':hora_llegada_h' => $n_hora_llegada_h,
							':hora_llegada_p' => $n_hora_llegada_p,
							':hora_salida_h' => $n_hora_salida_h,
							':hora_salida_p' => $n_hora_salida_p,
							':dias_c' => $n_dias_c,
							
							':nivel_curso' => $n_nivel_curso,
							':organizacion' => $n_organizacion,
							':certificacion' => $n_certificacion,
							
							':pago_unico_curso' => $n_pago_unico_curso,
							':pago_semanal_curso' => $n_pago_semanal_curso,
							':tendra_costo_inicial' => $n_tendra_costo_ins,
							':pago_inscripcion_curso' => $n_pago_inscripcion_curso,
							
							':Imagen' => $n_Imagen,
							':IsActive' => $n_activador_curso
							)
						);					
		header('Location: consultar_cursos.php');
		return; 
	}
	
	if(isset($_POST['borrar'])){
	$n_idCurso = $_POST['idCurso'];

		
			$delete = 'DELETE FROM comunitec32k_cursos 
					WHERE 
						idCurso  = :id';
					
					$stmt = $link->prepare($delete);
					$stmt->execute(array(
							':id' => $n_idCurso,
							)
						);	
						
		header('Location: consultar_cursos.php');
		return; 			
	}		
?>

<html>
<head>

	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Modificar cursos</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
				
	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/tablas_Style.css" rel="stylesheet">
				
	<!-- =======================================================
	* Template Name: Rapid - v2.1.0
	* Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->

</head>
<body>
    <div class="container">
		<header class="section-header">
		   <br>
          <h3> Actualizar datos de cursos </h3>
        </header>
			<form method="POST">
				<label hidden>ID curso: </label>
				<input type="text" name="idCurso" hidden value ="<?php echo $id_curso ?>">
				 <br>		
				<label>Nombre del curso</label>
				<input type="text" name="nombre_curso" class="form-control" required value="<?php echo $nombre_curso ?>" />
				 <br>
				<label>Subtítulo</label>
				<input type="text" name="sub_titulo_curso" class="form-control" required  value="<?php echo $sub_titulo_curso ?>" />
				 <br> 				 
				<label>Descripción</label>
				<input type="text" class="form-control" name="descripcion_curso" rows="5" data-rule="required" data-msg="Necesita una breve descripción" value="<?php echo $descripcion_curso ?>" >
				 <br>
				<label>Duración</label>
				<select name="duracion_curso" class="form-control" required value="<?php echo $duracion_curso_n ?>" />
					<option value="5_hrs">5 hrs</option>
					<option value="10_hrs">10 hrs</option>
					<option value="15_hrs">15 hrs</option>
					<option value="20_hrs">20 hrs</option>
					<option value="25_hrs">25 hrs</option>
					<option value="30_hrs">30 hrs</option>
					<option value="35_hrs">35 hrs</option>
					<option value="40_hrs">40 hrs</option>
					<option value="45_hrs">45 hrs</option>
					<option value="50_hrs">50 hrs</option>
					<option value="55_hrs">55 hrs</option>
					<option value="60_hrs">60 hrs</option>
					<option value="65_hrs">65 hrs</option>
					<option value="70_hrs">70 hrs</option>
					<option value="75_hrs">75 hrs</option>
				</select>
				 <br>
				<label>Empieza el:</label>
				<input type="date" name="inicio_curso" class="form-control" required value="<?php echo $inicio_curso ?>" />
				 <br>
				<label>Termina el:</label>
				<input type="date" name="cierre_curso" class="form-control" required value="<?php echo $cierre_curso ?>" />
				 <br> 				 
				<label>Hora de llegada:</label>
						<select name="hora_llegada_h" class="form-control" required>
							<option value="1:00">1:00</option>
							<option value="2:00">2:00</option>
							<option value="3:00">3:00</option>
							<option value="4:00">4:00</option>
							<option value="5:00">5:00</option>
							<option value="6:00">6:00</option>
							<option value="7:00">7:00</option>
							<option value="8:00">8:00</option>
							<option value="9:00">9:00</option>
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
						</select>
						<select name="hora_llegada_p" class="form-control" required>
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>
				<label>Hora de salida :</label>
						<select name="hora_salida_h" class="form-control" required>
							<option value="1:00">1:00</option>
							<option value="2:00">2:00</option>
							<option value="3:00">3:00</option>
							<option value="4:00">4:00</option>
							<option value="5:00">5:00</option>
							<option value="6:00">6:00</option>
							<option value="7:00">7:00</option>
							<option value="8:00">8:00</option>
							<option value="9:00">9:00</option>
							<option value="10:00">10:00</option>
							<option value="11:00">11:00</option>
							<option value="12:00">12:00</option>
						</select>
						<select name="hora_salida_p" class="form-control" required>
							<option value="AM">AM</option>
							<option value="PM">PM</option>
						</select>
						 <br>	
				<label>Dias de curso:</label><br>
							<input type="checkbox" id="dc_lunes" name="dias_c[]" value="Lunes">
							<label for="dc_lunes"> Lunes</label>
							 <br>
							<input type="checkbox" id="dc_martes" name="dias_c[]" value="Martes">
							<label for="dc_martes"> Martes</label>
							 <br>
							<input type="checkbox" id="dc_miercoles" name="dias_c[]" value="Miercoles">
							<label for="dc_miercoles"> Miercoles</label>
							 <br>
							<input type="checkbox" id="dc_jueves" name="dias_c[]" value="Jueves">
							<label for="dc_jueves"> Jueves</label>
							 <br>
							<input type="checkbox" id="dc_viernes" name="dias_c[]" value="Viernes">
							<label for="dc_viernes"> Viernes</label>	
							 <br>
							<input type="checkbox" id="dc_sabado" name="dias_c[]" value="Sabado">
							<label for="dc_sabado"> Sabado</label>	
							 <br>
							<input type="checkbox" id="dc_domingo" name="dias_c[]" value="Domingo">
							<label for="dc_domingo"> Domingo</label>								
						 <br>				 
				<label>Nivel</label>
				 <br>
				<select name="nivel_curso" class="form-control" required value="<?php echo $nivel_curso ?>" >
					<option value="Unico">Unico</option>
					<option value="Basico">Basico</option>
					<option value="Intermedio">Intermedio</option>
					<option value="Avanzado">Avanzado</option>
				</select>	
				 <br>	
				<label>Impartido por:</label>
				<select name="organizacion" class="form-control" required >
					<option>Cenaltec</option>
					<option>Icatech</option>
				</select>	
				 <br>					 
				<label>Entrega constancia con validez ofical</label>
				 <br>
				<select name="certificacion" class="form-control" value="<?php echo $certificacion ?>" >
					<option value="Si"><span>Si</span><br>
					<option value="No"><span>No</span>
				</select>	
				 <br>
				<label>Pago unico</label>
				<input type="number" name="pago_unico_curso" class="form-control" value="<?php echo $pago_unico_curso ?>" required>
				 <br>
				<label>Pago semanal</label>
				<input type="number" name="pago_semanal_curso" class="form-control" value="<?php echo $pago_semanal_curso ?>" required>						 
				 <br>
				<label>Costo inscripción</label>
				<select name="tendra_costo_ins" class="form-control" >
					<option value="No">No</option>
					<option value="Si">Si</option>
				</select>
				 <br>
				<label>Precio de inscripción</label>
				<input type="number" name="pago_inscripcion_curso" class="form-control" >
				 <br>					 
				<label>Imagen</label>
				<input type="text" name="Imagen" class="form-control"   value="<?php echo $Imagen ?>" />
				 <br>  
				<label>Activar/Desactivar</label>
				 <br> 
				<select name="activador_curso" class="form-control"> 
					<option value="1"><span>Activar</span><br>
					<option value="0"><span>Desactivar</span>
				</select>
				 <br>
				 <br>
				<input type="submit" name="actualizar_curso" value="Actualizar" >
				<a href="consultar_cursos.php"><button type="button" title="Cancelar">Cancelar</button></a>
				<input type="submit" name="borrar" value="Borrar">
			</form>  	
	</div>


  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
 
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
  
</body>
</html>