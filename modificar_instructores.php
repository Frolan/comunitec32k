<?php
require_once "conexion.php";	
	session_start();
	
	//var_dump($_POST); <-- Para revisar que es lo que pasa en caso de fallo 
	
	$id_instructor = $_GET['id'];
	
	$qry = 'SELECT * FROM comunitec_tbl_instructores WHERE id_instructor = :id';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id' => $id_instructor)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
		
	$nombre_ins = htmlentities($row['nombre_ins']);
	$apellido_pa_ins = htmlentities($row['apellido_pa_ins']);
	$apellido_ma_ins = htmlentities($row['apellido_ma_ins']);
	$correo_ins = htmlentities($row['correo_ins']);
	$CURP = htmlentities($row['CURP']);
	$contrasena = htmlentities($row['contrasena']);
	$cambio_contrasena = htmlentities($row['cambiocontrasena']);
	$id_tipo_usuario = htmlentities($row['id_tipo_usuario']);
	$isActive = htmlentities($row['isActive']);
	
	if(isset($_POST['actualizar_curso'])){
	
		$n_id_instructor = htmlentities(trim($_POST['id_instructor']));
		$n_nombre_ins = htmlentities(trim($_POST['nombre_ins']));
		$n_apellido_pa_ins = htmlentities(trim($_POST['apellido_pa_ins']));
		$n_apellido_ma_ins = htmlentities(trim($_POST['apellido_ma_ins']));
		$n_correo_ins = htmlentities(trim($_POST['correo_ins']));
		$n_CURP = htmlentities(trim($_POST['CURP']));
		$n_contrasena = htmlentities(trim($_POST['contrasena']));
		$n_cambiocontrasena = htmlentities(trim($_POST['cambio_contrasena']));
		$n_id_tipo_usuario = htmlentities(trim($_POST['id_tipo_usuario']));
		$n_isActive = htmlentities(trim($_POST['activador_instructor']));		

			$update = 'UPDATE comunitec_tbl_instructores SET
				nombre_ins = :nombre,
				apellido_pa_ins = :apellido_pa,
				apellido_ma_ins = :apellido_ma,
				correo_ins = :correo_in,
				CURP = :curp,
				contrasena = :contrase,
				cambiocontrasena = :cambiocontra,
				id_tipo_usuario = :id_tipo_usu,
				isActive = :active 
			WHERE 
				id_instructor = :id';
							
		$stmt = $link->prepare($update);
		$stmt->execute(array(	
				':nombre' => $n_nombre_ins,
				':apellido_pa' => $n_apellido_pa_ins,
				':apellido_ma' => $n_apellido_ma_ins,
				':correo_in' => $n_correo_ins,
				':curp' => $n_CURP,
				':contrase' => $n_contrasena,
				':cambiocontra' => $n_cambiocontrasena,
				':id_tipo_usu' => $n_id_tipo_usuario,
				':active' => $n_isActive,
				':id' => $n_id_instructor
				)
			);					
		header('Location: consultar_instructores.php');
		return;
	}

	if(isset($_POST['borrar'])){
	$n_id_instructor = $_POST['id_instructor'];

		
			$delete = 'DELETE FROM comunitec_tbl_instructores 
					WHERE 
						id_instructor  = :id';
					
					$stmt = $link->prepare($delete);
					$stmt->execute(array(
							':id' => $n_id_instructor,
							)
						);	
						
		header('Location: consultar_instructores.php');
		return; 			
	}	
	
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Modificar instructores</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

<div class="container">
		<header class="section-header">
		   <br>
          <h3> Actualizar datos de instructores </h3>
        </header>
			<form method="POST">
						  <br>
						<label hidden >Id instructor</label>
						<input type="text" name="id_instructor" class="form-control" required value="<?php echo $id_instructor?>" hidden />
						  <br>
						<label>Nombre</label>
						<input type="text" name="nombre_ins" class="form-control" required value="<?php echo $nombre_ins?>" />
						  <br>
						<label>Apellido Paterno</label>
						<input type="text" name="apellido_pa_ins" class="form-control" required value="<?php echo $apellido_pa_ins?>" />
						  <br>
						<label>Apellido Materno</label>
						<input type="text" name="apellido_ma_ins" class="form-control" required value="<?php echo $apellido_ma_ins?>" />
						  <br>
						<label>Correo Electronico</label>
						<input type="mail" name="correo_ins" class="form-control" required value="<?php echo $correo_ins?>" />
						  <br>
						<label>Activar / Desactivar:</label><br>
						<select name="activador_instructor" class="form-control" required>
							<option value="1">Activar</option>
							<option value="0">Desactivar</option>
						</select>
						  <br>
						<label hidden >Curp</label>
						<input type="text" name="CURP" class="form-control" required value="<?php echo $CURP?>" hidden />
						  <br>
						<label hidden >Contrasena</label>
						<input type="text" name="contrasena" class="form-control" required value="<?php echo $contrasena?>" hidden />								
						  <br>
						<label hidden >Cambio contraseña</label>
						<input type="text" name="cambio_contrasena" class="form-control" required value="<?php echo $cambio_contrasena?>" hidden />									
						  <br>
						<label hidden >Id tipo usuario</label>
						<input type="text" name="id tipo usuario" class="form-control" required value="<?php echo $id_tipo_usuario?>"hidden  />								
						  <br>
						<input type="submit" name="actualizar_curso" value="Actualizar" >
						<a href="consultar_instructores.php"><button type="button" title="Cancelar">Cancelar</button></a>
						<input type="submit" name="borrar" value="Borrar">					
			</form>		
</div>  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>

</html>