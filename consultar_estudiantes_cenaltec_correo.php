<?php
	require_once "conexion.php";
	require_once "clases.php";
	session_start();

	$id_estudiante = $_GET['id'];	
	
	$qry_cenaltec_estudiantes = "SELECT * FROM cenaltec_tbl_estudiantes_cenaltec WHERE id_usuario = :id_es";
	$stmt_usuarios_c = $link->prepare($qry_cenaltec_estudiantes);
	$stmt_usuarios_c->execute(array(
		':id_es' => $id_estudiante)
	);	
	
		$row1 = $stmt_usuarios_c->fetch(PDO::FETCH_ASSOC);
		
		$ap_Paterno_t = htmlentities($row1['apellido_paterno']);
		$ap_Materno_t = htmlentities($row1['apellido_materno']);
		$nombre_t = htmlentities($row1['nombre']);
		$correo_electronico_t = htmlentities($row1['correo_electronico']);
	
	$_SESSION['success'] = "Se le ha enviado un mensaje por correo a ".$nombre_t.' '.$ap_Paterno_t.' '.$ap_Materno_t."";
	
	//Istaciacion de clases
	$id_usuario = new Estudiantes_cenaltec();
	$fecha_actual = new Estudiantes_cenaltec();
	$nombre = new Estudiantes_cenaltec();
	$calle_numero = new Estudiantes_cenaltec();
	$colonia = new Estudiantes_cenaltec();
	$cp = new Estudiantes_cenaltec();
	$municipio = new Estudiantes_cenaltec();
	$estado = new Estudiantes_cenaltec();
	
	$sexo = new Estudiantes_cenaltec();
	$curp = new Estudiantes_cenaltec();
	$fecha_nacimiento = new Estudiantes_cenaltec();
	$lugar_nacimiento = new Estudiantes_cenaltec();
	$telefono_casa = new Estudiantes_cenaltec();
	$telefono_trabajo = new Estudiantes_cenaltec();
	$celular = new Estudiantes_cenaltec();
	$estado_civil = new Estudiantes_cenaltec();
	$correo_electronico = new Estudiantes_cenaltec();
	$especificar_estudios = new Estudiantes_cenaltec();
	
	$tomado_curso_antes = new Estudiantes_cenaltec();
	$servicio_medico = new Estudiantes_cenaltec();
	$alergias_enf = new Estudiantes_cenaltec();
	$tipo_sangre = new Estudiantes_cenaltec();
	$presenta_discapacidad = new Estudiantes_cenaltec();
	$nombre_emergencia = new Estudiantes_cenaltec();
	$telefono_emergencia = new Estudiantes_cenaltec();
	$nombre_empresa = new Estudiantes_cenaltec();
	$puesto = new Estudiantes_cenaltec();
	$direccion_empresa = new Estudiantes_cenaltec();
	
	$antiguedad = new Estudiantes_cenaltec();
	$apoyo_empresa = new Estudiantes_cenaltec();
	$medio_enterado = new Estudiantes_cenaltec();
	$motivos_entrenamiento = new Estudiantes_cenaltec();
	$curso = new Estudiantes_cenaltec();
	$inicio_curso = new Estudiantes_cenaltec();
	$duracion_curso = new Estudiantes_cenaltec();
	$requiere_factura = new Estudiantes_cenaltec();
	$nombre_factura = new Estudiantes_cenaltec();
	
	$razon_social = new Estudiantes_cenaltec();
	$rfc_fac = new Estudiantes_cenaltec();
	$SFDI_fac = new Estudiantes_cenaltec();
	
	//statement
	$cenaltec_estudiantes = "SELECT * FROM cenaltec_tbl_estudiantes_cenaltec ";
	$stmt = $link->query($cenaltec_estudiantes);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_usuario->id_usuario = htmlentities($row['id_usuario']);
		$fecha_actual->fecha_actual = htmlentities($row['fecha_actual']);
		$nombre->apellido_paterno = htmlentities($row['apellido_paterno']);
		$nombre->apellido_materno = htmlentities($row['apellido_materno']);
		$nombre->nombre = htmlentities($row['nombre']);
		$calle_numero->calle_numero = htmlentities($row['calle_numero']);
		$colonia->colonia = htmlentities($row['colonia']);
		$cp->cp = htmlentities($row['cp']);
		$municipio->municipio = htmlentities($row['municipio']);
		$estado->estado = htmlentities($row['estado']);
		
		$sexo->sexo = htmlentities($row['sexo']);
		$curp->curp = htmlentities($row['curp']);
		$fecha_nacimiento->fecha_nacimiento = htmlentities($row['fecha_nacimiento']);
		$lugar_nacimiento->lugar_nacimiento = htmlentities($row['lugar_nacimiento']);
		$telefono_casa->telefono_casa = htmlentities($row['telefono_casa']);
		$telefono_trabajo->telefono_trabajo = htmlentities($row['telefono_trabajo']);
		$celular->celular = htmlentities($row['celular']);
		$estado_civil->estado_civil = htmlentities($row['estado_civil']);
		$correo_electronico->correo_electronico = htmlentities($row['correo_electronico']);
		$especificar_estudios->especificar_estudios = htmlentities($row['especificar_estudios']);
		
		$tomado_curso_antes->tomado_curso_antes = htmlentities($row['tomado_curso_antes']);
		$servicio_medico->servicio_medico = htmlentities($row['servicio_medico']);
		$alergias_enf->alergias_enf = htmlentities($row['alergias_enf']);
		$tipo_sangre->tipo_sangre = htmlentities($row['tipo_sangre']);
		$presenta_discapacidad->presenta_discapacidad = htmlentities($row['presenta_discapacidad']);
		$nombre_emergencia->nombre_emergencia = htmlentities($row['nombre_emergencia']);
		$telefono_emergencia->telefono_emergencia = htmlentities($row['telefono_emergencia']);
		$nombre_empresa->nombre_empresa = htmlentities($row['nombre_empresa']);
		$puesto->puesto = htmlentities($row['puesto']);
		$direccion_empresa->direccion_empresa = htmlentities($row['direccion_empresa']);
		
		$antiguedad->antiguedad = htmlentities($row['antiguedad']);
		$apoyo_empresa->apoyo_empresa = htmlentities($row['apoyo_empresa']);
		$medio_enterado->medio_enterado = htmlentities($row['medio_enterado']);
		$motivos_entrenamiento->motivos_entrenamiento = htmlentities($row['motivos_entrenamiento']);
		$curso->curso = htmlentities($row['curso']);
		$inicio_curso->inicio_curso = htmlentities($row['inicio_curso']);
		$duracion_curso->duracion_curso = htmlentities($row['duracion_curso']);
		$requiere_factura->requiere_factura = htmlentities($row['requiere_factura']);
		$nombre_factura->nombre_factura = htmlentities($row['nombre_factura']);
		
		$razon_social->razon_social = htmlentities($row['razon_social']);
		$rfc_fac->rfc_fac = htmlentities($row['rfc_fac']);
		$SFDI_fac->SFDI_fac = htmlentities($row['SFDI_fac']);
		

		//Arreglos
		$arrId_estudiante_C[] = ($id_usuario->id_usuario);
		$arrFecha[] = ($fecha_actual->fecha_actual);
		$arrNombres_C[] = ($nombre->nombre.' '.$nombre->apellido_paterno.' '.$nombre->apellido_materno);	
		$arrCalles[] = ($calle_numero->calle_numero);	
		$arrColonias[] = ($colonia->colonia);	
		$arrCp[] = ($cp->cp);	
		$arrMunicipios[] = ($municipio->municipio);	
		$arrEstados[] = ($estado->estado);	
		
		$arrSexo[] = ($sexo->sexo);	
		$arrCurp[] = ($curp->curp);	
		$arrFecha_nacimiento[] = ($fecha_nacimiento->fecha_nacimiento);	
		$arrLugar_nacimiento[] = ($lugar_nacimiento->lugar_nacimiento);	
		$arrTelefono_casa[] = ($telefono_casa->telefono_casa);	
		$arrTelefono_trabajo[] = ($telefono_trabajo->telefono_trabajo);	
		$arrCelular[] = ($celular->celular);	
		$arrEstado_civil[] = ($estado_civil->estado_civil);	
		$arrCorreo[] = ($correo_electronico->correo_electronico);	
		$arrEstudios[] = ($especificar_estudios->especificar_estudios);	
		
		$arrTomado_curso[] = ($tomado_curso_antes->tomado_curso_antes);
		$arrServicio_medico[] = ($servicio_medico->servicio_medico);
		$arrAlergias_enf[] = ($alergias_enf->alergias_enf);
		$arrTipo_sangre[] = ($tipo_sangre->tipo_sangre);
		$arrPresenta_discapacidad[] = ($presenta_discapacidad->presenta_discapacidad);
		$arrNombre_emergencia[] = ($nombre_emergencia->nombre_emergencia);
		$arrTelefono_emergencia[] = ($telefono_emergencia->telefono_emergencia);
		$arrNombre_empresa[] = ($nombre_empresa->nombre_empresa);
		$arrPuesto[] = ($puesto->puesto);
		$arrDireccion_empresa[] = ($direccion_empresa->direccion_empresa);
		
		$arrAntiguedad[] = ($antiguedad->antiguedad);
		$arrApoyo_empresa[] = ($apoyo_empresa->apoyo_empresa);
		$arrMedio_enterado[] = ($medio_enterado->medio_enterado);
		$arrMotivos_entrenamiento[] = ($motivos_entrenamiento->motivos_entrenamiento);
		$arrCurso[] = ($curso->curso);
		$arrInicio_curso[] = ($inicio_curso->inicio_curso);
		$arrDuracion_curso[] = ($duracion_curso->duracion_curso);
		$arrRequiere_factura[] = ($requiere_factura->requiere_factura);
		$arrNombre_factura[] = ($nombre_factura->nombre_factura);
		
		$arrRazon_social[] = ($razon_social->razon_social);
		$arrRfc_fac[] = ($rfc_fac->rfc_fac);
		$arrSfdi_fac[] = ($SFDI_fac->SFDI_fac);
		
		$cant_reg++;	
	}
	
	$subject = "Bienvenido al portal Comunitec32k";
	$message = "Este es un mensaje de prueba, gracias por tu interes en el portal de Comunitec32k, esperamos que puedas superarte con nuestros cursos";			
	enviarEmail($subject, $message, $correo_electronico_t); 
	
	//========Funciones========// 	
	function enviarEmail($subject, $message, $correo_electronico_t){
		
		$sender = "felipe.galan@comunitec32k.com";
		$email = $sender;
		$name2send = "Comunitec32k";
		///$mailto = $email.",".$sender;
		$mailto = $correo_electronico_t;
		///$mailto = $ceo;
		
		$from="From: $name2send<$email>\r\nReturn-path: $sender";
		///$subject=
		///$message=
			
		mail($mailto, $subject, $message, $from);	
	}
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Consultar de Registros Cenaltec</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">	
  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

<!--Tabla de cursos activos -->
	<div class="container">
		<h1><center>Consulta de Registros Cenaltec</center></h1>
		<br>

<?php
		if( isset($_SESSION['success']) ){
			echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
			unset($_SESSION['success']);
		}
			
		//print_r($_SESSION); //En caso de revision descomentar el print_r	
?>	
		
	<div class="container">
		<label>Buscar Estudiante</label><br>
		<input type="text" id="nombre" name="nombre">
		<input type="button" id="busca_nombre" name="busca_nombre_usuario" value="Buscar Usuario" onclick="buscar()">
	</div>	
		<br>
	<div class="container" id="datos_elemento">	
	</div>
		<br>
		
	<div class="container" id="datos_alumnos">	
	</div>
		<br>
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>ID alumno</th>
				<th scope="col">Fecha Inscrito</th>
				<th scope="col">Nombre</th>
				<th scope="col">Calle</th>
				<th scope="col">Colonia</th>
				<th scope="col">Codigo Postal</th>
				<th scope="col">Municipio</th>	
				<th scope="col">Estado</th>
				
				<th scope="col">Sexo</th>	
				<th scope="col">Curp</th>
				<th scope="col">Fecha Nacimiento</th>
				<th scope="col">Lugar Nacimiento</th>
				<th scope="col">Telefono casa</th>
				<th scope="col">Telefono trabajo</th>
				<th scope="col">Celular</th>
				<th scope="col">Estado Civil</th>
				<th scope="col">Correo</th>
				<th scope="col">Ultimo nivel estudios</th>
				
				<th scope="col">Primer curso</th>
				<th scope="col">Servicio Medico</th>
				<th scope="col">Alergias/Enfermedades</th>
				<th scope="col">Tipo de sangre</th>
				<th scope="col">Discapacidades</th>
				<th scope="col">Persona de emergencia</th>
				<th scope="col">Telefono emergencia</th>
				<th scope="col">Empresa donde labora</th>
				<th scope="col">Puesto</th>
				
				<th scope="col">Dirección</th>
				<th scope="col">Antigüedad</th>
				<th scope="col">Apoyo Empresa</th>
				<th scope="col">Medio enterado</th>
				<th scope="col">Motivo de entrenamiento</th>
				<th scope="col">Curso</th>
				<th scope="col">Inicio curso</th>
				<th scope="col">Duracion del curso</th>
				<th scope="col">Factura</th>
				<th scope="col">Nombre Factura</th>
				
				<th scope="col">Razon social</th>
				<th scope="col">RFC</th>
				<th scope="col">SDFI</th>
				
				<th scope="col">Actualizar</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>	
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrNombres_C as $nombre){
					$id_usu = $arrId_estudiante_C[$rowCount];
					$fec = $arrFecha[$rowCount];
					$cal = $arrCalles[$rowCount];
					$col = $arrColonias [$rowCount];
					$cp = $arrCp [$rowCount];
					$mun = $arrMunicipios [$rowCount];			
					$est = $arrEstados [$rowCount];
					
					$sex = $arrSexo [$rowCount];	
					$cur = $arrCurp [$rowCount];
					$fna = $arrFecha_nacimiento [$rowCount];
					$lna = $arrLugar_nacimiento [$rowCount];		
					$tca = $arrTelefono_casa [$rowCount];
					$ttr = $arrTelefono_trabajo [$rowCount];
					$cel = $arrCelular [$rowCount];	
					$esc = $arrEstado_civil [$rowCount];
					$cor = $arrCorreo [$rowCount];
					$stu = $arrEstudios [$rowCount];
					
					$tom = $arrTomado_curso [$rowCount];
					$ser = $arrServicio_medico [$rowCount];
					$ale = $arrAlergias_enf [$rowCount];
					$tip = $arrTipo_sangre [$rowCount];
					$dis = $arrPresenta_discapacidad [$rowCount];
					$nem = $arrNombre_emergencia [$rowCount];
					$tel = $arrTelefono_emergencia [$rowCount];
					$eno = $arrNombre_empresa [$rowCount];
					$pue = $arrPuesto [$rowCount];
					$dir = $arrDireccion_empresa [$rowCount];
					
					$ant = $arrAntiguedad [$rowCount];
					$apo = $arrApoyo_empresa [$rowCount];
					$med = $arrMedio_enterado [$rowCount];
					$mot = $arrMotivos_entrenamiento [$rowCount];
					$cus = $arrCurso [$rowCount];
					$ini = $arrInicio_curso [$rowCount];
					$dur = $arrDuracion_curso [$rowCount];
					$req = $arrRequiere_factura [$rowCount];
					$nfa = $arrNombre_factura [$rowCount];
					
					$raz = $arrRazon_social [$rowCount];
					$rfc = $arrRfc_fac [$rowCount];
					$sfd = $arrSfdi_fac [$rowCount];
					
					$cor = $arrCorreo [$rowCount];
				
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id_usu.'</th>';
						echo '<td>'.$fec.'</td>';
						echo '<td>'.$nombre.'</td>';
						echo '<td>'.$cal.'</td>';
						echo '<td>'.$col.'</td>';
						echo '<td>'.$cp.'</td>';
						echo '<td>'.$mun.'</td>';	
						echo '<td>'.$est.'</td>';
						
						echo '<td>'.$sex.'</td>';		
						echo '<td>'.$cur.'</td>';
						echo '<td>'.$fna.'</td>';
						echo '<td>'.$lna.'</td>';	
						echo '<td>'.$tca.'</td>';
						echo '<td>'.$ttr.'</td>';
						echo '<td>'.$cel.'</td>';
						echo '<td>'.$esc.'</td>';
						echo '<td>'.$cor.'</td>';
						echo '<td>'.$stu.'</td>';
						
						echo '<td>'.$tom.'</td>';
						echo '<td>'.$ser.'</td>';
						echo '<td>'.$ale.'</td>';
						echo '<td>'.$tip.'</td>';
						echo '<td>'.$dis.'</td>';
						echo '<td>'.$nem.'</td>';
						echo '<td>'.$tel.'</td>';
						echo '<td>'.$eno.'</td>';
						echo '<td>'.$pue.'</td>';
						echo '<td>'.$dir.'</td>';
						
						echo '<td>'.$ant.'</td>';
						echo '<td>'.$apo.'</td>';
						echo '<td>'.$med.'</td>';
						echo '<td>'.$mot.'</td>';
						echo '<td>'.$cus.'</td>';
						echo '<td>'.$ini.'</td>';
						echo '<td>'.$dur.'</td>';
						
						echo '<td>'.$req.'</td>';
						echo '<td>'.$nfa.'</td>';
						
						echo '<td>'.$raz.'</td>';
						echo '<td>'.$rfc.'</td>';
						echo '<td>'.$sfd.'</td>';			
						echo 	"<td>
									<a href='modificar_estudiantes_cenaltec.php?id=".$id_usu."'>
										<button>Actualizar</button>
									</a>
								</td>";			
						echo 	"<td>
									<a href='consultar_estudiantes_cenaltec_correo.php?id=".$id_usu."'>
										<button>Enviar Correo</button>
									</a>
								</td>";	
								
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay usuarios registrados</p>';
				echo '</div>';	
			}
		?>
		</tbody>
	</table>
	</div>
	 <br>
	<div class = "container">
		<a href= "main.php"><button type="button">Regresar</button></a>
	</div>
<br>	
  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
<script>
	function buscar(){
		elemento = document.getElementById("nombre").value;
		elemento.trim();
		if( elemento != ''){///elemento
			///document.getElementById("datos_usuario").innerHTML = '<p style = "color: green";>Hola '+elemento;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText != ""){						
						document.getElementById("datos_elemento").innerHTML = "<br>"+this.responseText;
					}
				}
			};
			xmlhttp.open("GET","buscar_estudiantes_cenaltec.php?elemento="+elemento,true);
			xmlhttp.send();			
		}///elemento no está vacío		
	}
</script>
</html>