<?php
require_once 'conexion.php';
	session_start();
	
	$id_usuario = $_GET['id'];
	
	$qry = 'SELECT * FROM icatech_tbl_usuarios WHERE id_usuario = :id';
	$stmt = $link->prepare($qry);
	$stmt->execute(array(
		':id' => $id_usuario)
	);
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	$apellido_paterno = htmlentities($row['apellido_paterno']);
	$apellido_materno = htmlentities($row['apellido_materno']);	
	$nombre = htmlentities($row['nombre']);
	$edad = htmlentities($row['edad']);
	$rfc = htmlentities($row['rfc']);
	$curp = htmlentities($row['curp']);	
	$domicilio = htmlentities($row['domicilio']);
	$colonia = htmlentities($row['colonia']);
	$ciudad = htmlentities($row['ciudad']);
	$estado = htmlentities($row['estado']);
	$telefono = htmlentities($row['telefono']);
	$correo_electronico = htmlentities($row['correo_electronico']);
	
	if(isset($_POST['actualizar_estudiante'])){
		
		$n_id_usuario = htmlentities(trim($_POST['id_usuario']));
		$n_apellido_paterno = htmlentities(trim($_POST['apellido_paterno']));
		$n_apellido_materno = htmlentities(trim($_POST['apellido_materno']));
		$n_nombre = htmlentities(trim($_POST['nombre']));
		$n_edad = htmlentities(trim($_POST['edad']));
		$n_rfc = htmlentities(trim($_POST['rfc']));
		$n_curp = htmlentities(trim($_POST['curp']));
		$n_domicilio = htmlentities(trim($_POST['domicilio']));		
		$n_colonia = htmlentities(trim($_POST['colonia']));
		$n_ciudad = htmlentities(trim($_POST['ciudad']));
		$n_estado = htmlentities(trim($_POST['estado']));
		$n_telefono = htmlentities(trim($_POST['telefono']));
		$n_correo_electronico = htmlentities(trim($_POST['correo_electronico']));
		
		
			$update = 'UPDATE icatech_tbl_usuarios SET 
							apellido_paterno = :apellido_patern,
							apellido_materno = :apellido_matern,
							nombre = :nombr,
							edad = :eda,
							rfc = :rf,
							curp = :cur,
							domicilio = :domicili,
							colonia = :coloni,
							ciudad = :ciuda,
							estado = :estad,
							telefono = :telefon,
							correo_electronico = :correo_electronic
						WHERE
							id_usuario = :id_usuari';
							
						$stmt = $link->prepare($update);
						$stmt->execute(array(
							':apellido_patern' => $n_apellido_paterno,
							':apellido_matern' => $n_apellido_materno,
							':nombr' => $n_nombre,
							':eda' => $n_edad,
							':rf' => $n_rfc,
							':cur' => $n_curp,
							':domicili' => $n_domicilio,	
							':coloni' => $n_colonia,
							':ciuda' => $n_ciudad,
							':estad' => $n_estado,
							':telefon' => $n_telefono,
							':id_usuari' => $n_id_usuario,	
							':correo_electronic' => $n_correo_electronico	
							)
						);					
		header('Location: consultar_estudiantes_icatech.php');
		return; 
	}

	if(isset($_POST['borrar'])){
	$n_id_usuario = $_POST['id_usuario'];

		
			$delete = 'DELETE FROM icatech_tbl_usuarios 
					WHERE 
						id_usuario  = :id';
					
					$stmt = $link->prepare($delete);
					$stmt->execute(array(
							':id' => $n_id_usuario,
							)
						);	
						
		header('Location: consultar_estudiantes_icatech.php');
		return; 			
	}	
	
?>

<html>
<head>

	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>Modificar usuarios Icatech</title>
	<meta content="" name="descriptison">
	<meta content="" name="keywords">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
	<link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
	<link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
				
	<!-- Template Main CSS File -->
	<link href="assets/css/style.css" rel="stylesheet">
	<link href="assets/css/tablas_Style.css" rel="stylesheet">
				
	<!-- =======================================================
	* Template Name: Rapid - v2.1.0
	* Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->

</head>
<body>
    <div class="container">
		<header class="section-header">
		   <br>
          <h3> Actualizar datos del participante (Icatech) </h3>
        </header>
			<form method="POST">
				<label hidden>ID Usuario: </label>
				<input type="text" name="id_usuario" hidden value ="<?php echo $id_usuario ?>">
				 <br>		
				<label>Apellido Paterno</label>
				<input type="text" name="apellido_paterno" class="form-control" required value="<?php echo $apellido_paterno ?>" />
				 <br>
				<label>Apellido Materno</label>
				<input type="text" name="apellido_materno" class="form-control" required  value="<?php echo $apellido_materno ?>" />
				 <br> 				 
				<label>Nombre</label>
				<input type="text" class="form-control" name="nombre" value="<?php echo $nombre ?>" >
				 <br>
				<label>Edad</label>
				<input type="text" class="form-control" name="edad" value="<?php echo $edad ?>" >
				 <br>
				<label>RFC:</label>
				<input type="text" name="rfc" class="form-control" required value="<?php echo $rfc ?>" />
				 <br> 				 
				<label>curp:</label>
				<input type="text" name="curp" class="form-control" required value="<?php echo $curp ?>" />
				 <br> 	
				<label>Domicilio:</label>
				<input type="text" name="domicilio" class="form-control" required value="<?php echo $domicilio ?>" />
				 <br> 	
				<label>Colonia:</label>
				<input type="text" name="colonia" class="form-control" required value="<?php echo $colonia ?>" />
				 <br> 			 
				<label>Ciudad:</label>
				<input type="text" name="ciudad" class="form-control" required value="<?php echo $ciudad ?>" />
				 <br> 	
				<label>Estado:</label>
				<input type="text" name="estado" class="form-control" required value="<?php echo $estado ?>" />
				 <br> 				 
				<label>Telefono:</label>
				<input type="text" name="telefono" class="form-control" required value="<?php echo $telefono ?>" />
				 <br> 
				<label>Correo:</label>
				<input type="text" name="correo_electronico" class="form-control" required value="<?php echo $correo_electronico ?>" />
				 <br>  
				
				<input type="submit" name="actualizar_estudiante" value="Actualizar" >
				<a href="consultar_estudiantes_icatech.php"><button type="button" title="Cancelar">Cancelar</button></a>
				<input type="submit" name="borrar" value="Borrar">
			</form>  	
	</div>


  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
 
    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
  
</body>
</html>