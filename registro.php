<?php
require_once 'conexion.php';
session_start();

$_SESSION['success'] = false;

if(isset($_POST['btnRegUsuario'])){
		try{
			$ap_Paterno = htmlentities($_POST['ap_Paterno']);
			$ap_Materno = htmlentities($_POST['ap_Materno']);	
			$nombre = htmlentities($_POST['nombre']);	
			$ano_nacimiento = htmlentities($_POST['ano_nacimiento']);	
			$correo_electronico = htmlentities($_POST['correo_electronico']);	
			$telefono = htmlentities($_POST['telefono']);	
			$organizacion = htmlentities($_POST['organizacion']);	
			$colonia = htmlentities($_POST['colonia']);	

			$agregar_persona = "INSERT INTO comunitec_tbl_usuarios
							(ap_Paterno, ap_Materno, nombre, ano_nacimiento, correo_electronico, telefono, organizacion, colonia) 
							VALUES (:apellido_p, :apellido_m, :nombre, :ano, :correo, :tel, :orga, :col)
							";
			$persona = $link->prepare($agregar_persona);
			$persona->execute(array(
					':apellido_p' => $ap_Paterno,
					':apellido_m' => $ap_Materno,
					':nombre' => $nombre,
					':ano' => $ano_nacimiento,
					':correo' => $correo_electronico,
					':tel' => $telefono,
					':orga' => $organizacion,
					':col' => $colonia,
					)
				);
			$_SESSION['success'] = "A sido registrado con exito";	
			
		}catch(Exception $ex){
			echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
			echo '<h3> Error : '.$ex->getMessage().'</h3>';
			return;
		}
}	

if(isset($_POST['login'])){
		try{
			$motivo_visita = htmlentities($_POST['motivo_visita']);
			$loginEmail = htmlentities($_POST['loginEmail']);	


			$registrar_visita = "INSERT INTO comunitec_tbl_registro_visitas
								(correo_telefono, descripcion) 
								VALUES (:corr, :des)
							";
			$visita = $link->prepare($registrar_visita);
			$visita->execute(array(
					':corr' => $loginEmail,
					':des' => $motivo_visita,
					)
				);
			$_SESSION['success'] = "Su visita se a registrado";	
			
		}catch(Exception $ex){
			echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
			echo '<h3> Error : '.$ex->getMessage().'</h3>';
			return;
		}
}	
	
?>
<html>
<head>
	
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Registro Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>
<br>

	<!--Formulario para un nuevo usuario-->

	<div class="container">
		<h1><center>Registrate para recibir información para los proximos cursos<center></h1>
		<form action="registro.php" method="POST" class="registro">
			
<?php
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
?>				
			
			<div class="form-group">
				<label>Apellido paterno</label>
				<input type="text" name="ap_Paterno" id="ap_Paterno" class="form-control" required />
			</div>	
			
			<div class="form-group">
				<label>Apellido materno</label>
				<input type="text" name="ap_Materno" id="ap_Materno" class="form-control" required />
			</div>	
			
			<div class="form-group">
				<label>Nombre</label>
				<input type="text" name="nombre" id="nombre"  class="form-control" required />
			</div>	
			
			<div class="form-group">
				<label>Año de Nacimiento</label>
				<input type="number" name="ano_nacimiento" id="f_nacimiento" class="form-control"  />
			</div>	
			
			<div class="form-group">
				<label>Correo electronico</label>
				<input type="email" name="correo_electronico" id="correo_electronico" class="form-control" placeholder="alguien@hotmail.com" required />
			</div>	
						
			<div class="form-group">
				<label>Telefono</label>
				<input type="text" name="telefono" id="telefono" class="form-control" pattern="^\d{10}$" placeholder="6561234567"  />
			</div>	
			
			<div class="form-group">
				<label>Empresa/Institucion/Escuela</label>
				<input type="text" name="organizacion" id="organizacion" class="form-control"  />
			</div>	
			
			<div class="form-group">
				<label>Colonia / Fraccionamiento(Opcional)</label>
				<input type="text" name="colonia" id="colonia" class="form-control"/>
			</div>		
			
			<div class="form-group">
				<input type="checkbox" name="checkbox" value="check" id="agree" required />
				He leido la <a href="politicasprivacidad.html">politica de privacidad</a> de la comunidad tecnologica del centro.
			</div>	
			
			<div class="form-group">
				<input type="submit" class="btnAgregar" value="Registrar nuevo usuario(a)" name="btnRegUsuario">
			</div>
			
		</form>
	</div>
	<br>
	
	
	<!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->
  
</body>
</html>