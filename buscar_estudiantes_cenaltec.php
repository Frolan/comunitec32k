<?php
	require_once('conexion.php');
	$elemento = $_GET['elemento'];
	///echo '<p style = "color: green";>Hola '.$usuario;


$qry = "SELECT * FROM cenaltec_tbl_estudiantes_cenaltec 
                    WHERE nombre = :elemento";

    try{
            $stmt = $link->prepare($qry);
            $stmt->execute(array(
                    ':elemento' => $elemento)
                );
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
			
            $nombre = 'Usuario no encontrado!';
            if( $row == true ){    

				$id_usuario = htmlentities($row['id_usuario']);
				$fecha_actual = htmlentities($row['fecha_actual']);
				$apellido_paterno = htmlentities($row['apellido_paterno']);
				$apellido_materno = htmlentities($row['apellido_materno']);
				$nombre = htmlentities($row['nombre']);
				$calle_numero = htmlentities($row['calle_numero']);
				$colonia = htmlentities($row['colonia']);
				$cp = htmlentities($row['cp']);
				$municipio = htmlentities($row['municipio']);
				$estado = htmlentities($row['estado']);
				
				$sexo = htmlentities($row['sexo']);
				$curp = htmlentities($row['curp']);
				$fecha_nacimiento = htmlentities($row['fecha_nacimiento']);
				$lugar_nacimiento = htmlentities($row['lugar_nacimiento']);
				$telefono_casa = htmlentities($row['telefono_casa']);
				$telefono_trabajo = htmlentities($row['telefono_trabajo']);
				$celular = htmlentities($row['celular']);
				$estado_civil = htmlentities($row['estado_civil']);
				$correo_electronico = htmlentities($row['correo_electronico']);
				$especificar_estudios = htmlentities($row['especificar_estudios']);
				
				$tomado_curso_antes = htmlentities($row['tomado_curso_antes']);
				$servicio_medico = htmlentities($row['servicio_medico']);
				$alergias_enf = htmlentities($row['alergias_enf']);
				$tipo_sangre = htmlentities($row['tipo_sangre']);
				$presenta_discapacidad = htmlentities($row['presenta_discapacidad']);
				$nombre_emergencia = htmlentities($row['nombre_emergencia']);
				$telefono_emergencia = htmlentities($row['telefono_emergencia']);
				$nombre_empresa = htmlentities($row['nombre_empresa']);
				$puesto = htmlentities($row['puesto']);
				$direccion_empresa = htmlentities($row['direccion_empresa']);
				
				$antiguedad = htmlentities($row['antiguedad']);
				$apoyo_empresa = htmlentities($row['apoyo_empresa']);
				$medio_enterado = htmlentities($row['medio_enterado']);
				$motivos_entrenamiento = htmlentities($row['motivos_entrenamiento']);
				$curso = htmlentities($row['curso']);
				$inicio_curso = htmlentities($row['inicio_curso']);
				$duracion_curso = htmlentities($row['duracion_curso']);
				$requiere_factura = htmlentities($row['requiere_factura']);
				$nombre_factura = htmlentities($row['nombre_factura']);
				$razon_social = htmlentities($row['razon_social']);
			
				$rfc_fac = htmlentities($row['rfc_fac']);
				$SFDI_fac = htmlentities($row['SFDI_fac']);

                echo '<table>';
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id_usuario.'</th>';
						echo '<td>'.$fecha_actual.'</td>';
						echo '<td>'.$nombre.'</td>';
						echo '<td>'.$calle_numero.'</td>';
						echo '<td>'.$colonia.'</td>';
						echo '<td>'.$cp.'</td>';
						echo '<td>'.$municipio.'</td>';	
						echo '<td>'.$estado.'</td>';
						echo '<td>'.$sexo.'</td>';		
						echo '<td>'.$curp.'</td>';
						echo '<td>'.$fecha_nacimiento.'</td>';
						echo '<td>'.$lugar_nacimiento.'</td>';	
						echo '<td>'.$telefono_casa.'</td>';
						echo '<td>'.$telefono_trabajo.'</td>';
						echo '<td>'.$celular.'</td>';
						echo '<td>'.$estado_civil.'</td>';
						echo '<td>'.$correo_electronico.'</td>';
						echo '<td>'.$especificar_estudios.'</td>';	
						echo '<td>'.$tomado_curso_antes.'</td>';
						echo '<td>'.$servicio_medico.'</td>';
						echo '<td>'.$alergias_enf.'</td>';
						echo '<td>'.$tipo_sangre.'</td>';
						echo '<td>'.$presenta_discapacidad.'</td>';
						echo '<td>'.$nombre_emergencia.'</td>';
						echo '<td>'.$telefono_emergencia.'</td>';
						echo '<td>'.$nombre_empresa.'</td>';
						echo '<td>'.$puesto.'</td>';
						echo '<td>'.$direccion_empresa.'</td>';		
						echo '<td>'.$antiguedad.'</td>';
						echo '<td>'.$apoyo_empresa.'</td>';
						echo '<td>'.$medio_enterado.'</td>';
						echo '<td>'.$motivos_entrenamiento.'</td>';
						echo '<td>'.$curso.'</td>';
						echo '<td>'.$inicio_curso.'</td>';
						echo '<td>'.$duracion_curso.'</td>';		
						echo '<td>'.$requiere_factura.'</td>';
						echo '<td>'.$nombre_factura.'</td>';					
						echo '<td>'.$razon_social.'</td>';
						echo '<td>'.$rfc_fac.'</td>';
						echo '<td>'.$SFDI_fac.'</td>';							
						echo 	"<td>
									<a href='modificar_estudiantes_cenaltec.php?id=".$id_usuario."'>
										<button>Actualizar</button>
									</a>
								</td>";	
						echo 	"<td>
									<a href='consultar_estudiantes_cenaltec_correo.php?id=".$id_usuario."'>
										<button>Enviar Correo</button>
									</a>
								</td>";									
                    echo '</tr>';
                echo '</table>';
            
        }else{
            echo '<p style="color: red">'.$nombre.'</p>';
        }
	}catch(Exception $ex){
		echo '<h1>Hubo un error, favor de contactar al soporte tecnico </h1><br>';
		echo '<h3> Error : '.$ex->getMessage().'</h3>';
		return;
	}	
	

	
?>