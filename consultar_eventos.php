<?php

	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();	
	
	//Redireccionando a su respectivo dashboard
	if(isset($_POST['regreso'])){
		
		if($_SESSION['id_tipo_usuario'] == 1){
			header('Location: main.php');
			return;
		}else{
			header('Location: instructores.php');
			return;
		}	
	}			
	
	//Istaciacion de clases
	$id_evento = new Eventos();
	$nombre_evento = new Eventos();
	$sub_titulo_evento = new Eventos();
	$descripcion_evento = new Eventos();
	$duracion_evento = new Eventos();
	$fecha_inicio = new Eventos();
	$fecha_cierre = new Eventos();
	$hora_inicio_evento= new Eventos();
	$PO_imparten = new Eventos();
	$isActive = new Eventos();
	
	//DB connection
	$conectado = 1;

	//statement
	$eventos_activos = "SELECT * FROM comunitec32k_eventos ORDER BY IsActive ";
	$stmt = $link->query($eventos_activos);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_evento->id_evento = htmlentities($row['id_evento']);
		$nombre_evento->nombre_evento = htmlentities($row['nombre_evento']);
		$sub_titulo_evento->sub_titulo_evento = htmlentities($row['sub_titulo_evento']);
		$descripcion_evento->descripcion_evento = htmlentities($row['descripcion_evento']);
		$duracion_evento->duracion_evento = htmlentities($row['duracion_evento']);
		$fecha_inicio->fecha_inicio = htmlentities($row['fecha_inicio']);
		$fecha_cierre->fecha_cierre = htmlentities($row['fecha_cierre']);
		$hora_inicio_evento->hora_inicio_evento_h = htmlentities($row['hora_inicio_evento_h']);
		$hora_inicio_evento->hora_inicio_evento_p = htmlentities($row['hora_inicio_evento_p']);
		$PO_imparten->PO_imparten = htmlentities($row['PO_imparten']);
		$isActive->isActive = htmlentities($row['isActive']);


		$arrId_evento[] = ($id_evento->id_evento);
		$arrNombre_evento[] = ($nombre_evento->nombre_evento);
		$arrSub_titulo_evento[] = ($sub_titulo_evento->sub_titulo_evento);
		$arrDescripcion_evento[] = ($descripcion_evento->descripcion_evento);
		$arrDuracion_evento[] = ($duracion_evento->duracion_evento); 
		$arrFecha_inicio[] = ($fecha_inicio->fecha_inicio);
		$arrFecha_cierre[] = ($fecha_cierre->fecha_cierre);
		$arrHora_inicio_evento[] = ($hora_inicio_evento->hora_inicio_evento_h.' '.$hora_inicio_evento->hora_inicio_evento_p);
		$arrPO_imparten[] = ($PO_imparten->PO_imparten);
		$arrisActive[] = ($isActive->isActive);
		
		$cant_reg++;	
	}	
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Eventos Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">
  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
<!--Tabla de eventos activos -->
	<div class="container">
	<br>
		<h1><center>Consulta de Eventos</center></h1>
		<br>
	<?php
		//print_r($_SESSION); //En caso de revision descomentar el print_r
	?>		
	<div class="container">
		<label>Buscar Evento</label><br>
		<input type="text" id="nombre_evento" name="nombre_evento" >
		<input type="button" id="busca_nombre_usuario" name="busca_nombre_usuario" value="Buscar Usuario" onclick="buscar_evento()">
	</div>	
		<br>
	<div class="container" id="datos_evento">	
	</div>
		<br>		
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>ID Evento</th>
				<th scope="col">Nombre del Evento</th>
				<th scope="col">Subtítulo</th>
				<th scope="col">Descripción</th>
				<th scope="col">Duración</th>
				<th scope="col">Fecha de inicio</th>
				<th scope="col">Fecha de cierre</th>
				<th scope="col">Hora de inicio</th>				
				<th scope="col">Persona / Organización que lo imparte</th>
				<th scope="col">Activo</th>
				<th scope="col">Actualizar</th>
				<th></th>
			</tr>
	</thead>
			
		<?php //Consulta que atrae todos los datos 
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrNombre_evento as $evento){
					$id = $arrId_evento[$rowCount];
					$titulo = $arrSub_titulo_evento[$rowCount];
					$des = $arrDescripcion_evento[$rowCount];
					$dur = $arrDuracion_evento [$rowCount];
					$ini = $arrFecha_inicio [$rowCount];
					$cie = $arrFecha_cierre [$rowCount];
					$hora = $arrHora_inicio_evento [$rowCount];
					$PO = $arrPO_imparten [$rowCount];
					$act = $arrisActive [$rowCount];
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id.'</th>';
						echo '<td>'.$evento.'</td>';
						echo '<td>'.$titulo.'</td>';
						echo '<td>'.$des.'</td>';
						echo '<td>'.$dur.'</td>';
						echo '<td>'.$ini.'</td>';
						echo '<td>'.$cie.'</td>';
						echo '<td>'.$hora.'</td>';
						echo '<td>'.$PO.'</td>';
						echo '<td>'.$act.'</td>';
						echo	"<td>
									<a href='modificar_eventos.php?id=".$id."'>
										<button>Actualizar</button>
									</a>
								</td>";							
					echo '</tr>';			
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay eventos registrados</p>';
				echo '</div>';	
			}
		?>
	</table>
	</div>
	  <br>
	<div class = "container">	
		<form method="POST">
			<input type="submit" name="regreso" value="Regresar">
		</form>
	</div>
<br>

  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
<script>
	function buscar_evento(){
		evento = document.getElementById("nombre_evento").value;
		evento.trim();
		if( evento != ''){///evento
			///document.getElementById("datos_evento").innerHTML = '<p style = "color: green";>Hola '+evento;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText != ""){						
						document.getElementById("datos_evento").innerHTML = "<br>"+this.responseText;
					}
				}
			};
			xmlhttp.open("GET","buscar_eventos.php?evento="+evento,true);
			xmlhttp.send();			
		}///evento no está vacío

		
	}
</script>
</html>