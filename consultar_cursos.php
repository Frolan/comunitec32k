<?php
	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();	

	//Redireccionando a su respectivo dashboard
	if(isset($_POST['regreso'])){
		
		if($_SESSION['id_tipo_usuario'] == 1){
			header('Location: main.php');
			return;
		}else{
			header('Location: instructores.php');
			return;
		}	
	}	
	
	//Istaciacion de clases
	$idCurso = new Cursos();
	$nombre_curso = new Cursos();
	$sub_titulo_curso = new Cursos();
	$descripcion_curso = new Cursos();
	$duracion_curso = new Cursos();
	$inicio_curso = new Cursos();
	$cierre_curso = new Cursos();
	
	$horario_curso = new Cursos();
	$dias_c = new Cursos();
	
	$nivel_curso = new Cursos();
	$organizacion = new Cursos();
	$certificacion = new Cursos();
	
	$pago_unico_curso = new Cursos();
	$pago_semanal_curso = new Cursos();
	$tendra_costo_inicial = new Cursos();
	$pago_inscripcion_curso = new Cursos();
	
	$Imagen = new Cursos();
	$IsActive = new Cursos();
	
	//DB connection
	$conectado = 1;
	
	//statement
	$cursos_activos = "SELECT * FROM comunitec32k_cursos ORDER BY IsActive ";
	$stmt = $link->query($cursos_activos);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$idCurso->idCurso = htmlentities($row['idCurso']);
		$nombre_curso->nombre_curso = htmlentities($row['nombre_curso']);
		$sub_titulo_curso->sub_titulo_curso = htmlentities($row['sub_titulo_curso']);
		$descripcion_curso->descripcion_curso = htmlentities($row['descripcion_curso']);
		$duracion_curso->duracion_curso = htmlentities($row['duracion_curso']);
		$inicio_curso->inicio_curso = htmlentities($row['inicio_curso']);
		$cierre_curso->cierre_curso = htmlentities($row['cierre_curso']);
		$horario_curso->hora_llegada_h = htmlentities($row['hora_llegada_h']);
		$horario_curso->hora_llegada_p = htmlentities($row['hora_llegada_p']);
		$horario_curso->hora_salida_h = htmlentities($row['hora_salida_h']);
		$horario_curso->hora_salida_p = htmlentities($row['hora_salida_p']);
		$dias_c->dias_c = htmlentities($row['dias_c']);		
		$nivel_curso->nivel_curso = htmlentities($row['nivel_curso']);
		$organizacion->organizacion = htmlentities($row['organizacion']);
		$certificacion->certificacion = htmlentities($row['certificacion']);		
		$pago_unico_curso->pago_unico_curso = htmlentities($row['pago_unico_curso']);
		$pago_semanal_curso->pago_semanal_curso = htmlentities($row['pago_semanal_curso']);
		$pago_inscripcion_curso->tendra_costo_inicial = htmlentities($row['tendra_costo_ins']);
		$pago_inscripcion_curso->pago_inscripcion_curso = htmlentities($row['pago_inscripcion_curso']);	
		$Imagen->Imagen = htmlentities($row['Imagen']);
		$IsActive->IsActive = htmlentities($row['IsActive']);

		//Arreglos
		$arrIdCurso[] = ($idCurso->idCurso);
		$arrNombre_curso[] = ($nombre_curso->nombre_curso );
		$arrSub_titulo_curso[] = ($sub_titulo_curso->sub_titulo_curso);
		$arrDescripcion_curso[] = ($descripcion_curso->descripcion_curso);
		$arrDuracion_curso[] = ($duracion_curso->duracion_curso); 
		$arrInicio_curso[] = ($inicio_curso->inicio_curso);
		$arrCierre_curso[] = ($cierre_curso->cierre_curso);		
		$arrHorario[] = ($horario_curso->hora_llegada_h.' '.$horario_curso->hora_llegada_p.' - '.$horario_curso->hora_salida_h.' '.$horario_curso->hora_salida_p);
		$arrDias[] = ($dias_c->dias_c);	
		$arrNivel_curso[] = ($nivel_curso->nivel_curso);
		$arrOrganizacion[] = ($organizacion->organizacion);
		$arrCertificacion[] = ($certificacion->certificacion);		
		$arrPago_unico[] = ($pago_unico_curso->pago_unico_curso);
		$arrPago_semanal[] = ($pago_semanal_curso->pago_semanal_curso);
		$arrInscripcion[] = ($pago_inscripcion_curso->tendra_costo_inicial.', '.$pago_inscripcion_curso->pago_inscripcion_curso );		
		$arrImagen[] = ($Imagen->Imagen);
		$arrIsActive[] = ($IsActive->IsActive);
		
		$cant_reg++;	
	}
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Cursos Comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
<br>
<!--Tabla de cursos activos -->
	<div class="container">
		<h1><center>Consulta de Cursos</center></h1>
		<br>
	<?php
		//print_r($_SESSION); //En caso de revision descomentar el print_r
	?>	
	<div class="container">
		<label>Buscar Cursos</label><br>
		<input type="text" id="nombre_curso" name="nombre_curso" placeholder="Ej. Programacion">
		<input type="button" id="busca_nombre_usuario" name="busca_nombre_usuario" value="Buscar Usuario" onclick="buscar_usuario()">
	</div>	
		<br>
	<div class="container" id="datos_curso">	
	</div>
		<br>
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>ID Curso</th>
				<th scope="col">Nombre Curso</th>
				<th scope="col">Subtítulo</th>
				<th scope="col" hidden>Descripcion</th>
				<th scope="col">Duracion Curso</th>
				<th scope="col">Fecha de Inicio</th>
				<th scope="col">Fecha de Cierre</th>	
				<th scope="col">Horario</th>
				<th scope="col">Dias de curso</th>	
				<th scope="col">Nivel</th>
				<th scope="col">Organizacion</th>
				<th scope="col">Certificacion</th>
				<th scope="col">Pago unico</th>
				<th scope="col">Pago semanal</th>
				<th scope="col">Costo inicial</th>
				<th scope="col" hidden>Imagen</th>
				<th scope="col">Activo</th>
				<th scope="col">Actualizar</th>
				<th scope="col"></th>
			</tr>
		</thead>
		<tbody>	
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrNombre_curso as $curso){
					$id_curso = $arrIdCurso[$rowCount];
					$titulo = $arrSub_titulo_curso[$rowCount];
					$des = $arrDescripcion_curso[$rowCount];
					$dur = $arrDuracion_curso [$rowCount];
					$ini = $arrInicio_curso [$rowCount];
					$cie = $arrCierre_curso [$rowCount];			
					$hor = $arrHorario [$rowCount];
					$dia = $arrDias [$rowCount];	
					$niv = $arrNivel_curso [$rowCount];
					$org = $arrOrganizacion [$rowCount];
					$cer = $arrCertificacion [$rowCount];		
					$uni = $arrPago_unico [$rowCount];
					$sem = $arrPago_semanal [$rowCount];
					$ins = $arrInscripcion [$rowCount];	
					$ima = $arrImagen [$rowCount];
					$act = $arrIsActive [$rowCount];
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id_curso.'</th>';
						echo '<td>'.$curso.'</td>';
						echo '<td>'.$titulo.'</td>';
						echo '<td scope="row" hidden>'.$des.'</td>';
						echo '<td>'.$dur.'</td>';
						echo '<td>'.$ini.'</td>';
						echo '<td>'.$cie.'</td>';	
						echo '<td>'.$hor.'</td>';
						echo '<td>'.$dia.'</td>';		
						echo '<td>'.$niv.'</td>';
						echo '<td>'.$org.'</td>';
						echo '<td>'.$cer.'</td>';	
						echo '<td>'.$uni.'</td>';
						echo '<td>'.$sem.'</td>';
						echo '<td>'.$ins.'</td>';
						echo '<td scope="row" hidden>'.$ima.'</td>';
						echo '<td>'.$act.'</td>';
						echo	"<td>
									<a href='modificar_cursos.php?id_curso=".$id_curso."'>
										<button>Actualizar</button>
									</a>
								</td>";
						echo	"<td>
									<a href='agregar_requisitos.php?id_curso=".$id_curso."'>
										<button>Agregar Requisitos</button>
									</a>
								</td>";								
						echo '</tr>';		
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay usuarios registrados</p>';
				echo '</div>';	
			}
		?>
		</tbody>
	</table>
	</div>
	 <br>
	<div class = "container">	
		<form method="POST">
			<input type="submit" name="regreso" value="Regresar">
		</form>
	</div>
<br>	


  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
<script>
	function buscar_usuario(){
		curso = document.getElementById("nombre_curso").value;
		curso.trim();
		if( curso != ''){///curso
			///document.getElementById("datos_usuario").innerHTML = '<p style = "color: green";>Hola '+curso;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText != ""){						
						document.getElementById("datos_curso").innerHTML = "<br>"+this.responseText;
					}
				}
			};
			xmlhttp.open("GET","buscar_cursos.php?curso="+curso,true);
			xmlhttp.send();			
		}///curso no está vacío		
	}
</script>
</html>