<?php
	require_once "conexion.php";
	require_once "clases.php";
	
	session_start();	
	
	//Istaciacion de clases
	$id_usuario = new asistentes_evento();	
	$id_evento = new asistentes_evento();	
	$fecha_inscrito = new asistentes_evento();	
	$apellido_paterno = new asistentes_evento();	
	$apellido_materno = new asistentes_evento();	
	$nombre = new asistentes_evento();	
	$nombre_evento = new asistentes_evento();	
	$correo_electronico = new asistentes_evento();	

	//statement
	$usuario_registrados = "SELECT * FROM comunitec_tbl_asistentes_evento ORDER BY nombre_evento ASC ";
	$stmt = $link->query($usuario_registrados);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_usuario->id_usuario = htmlentities($row['id_usuario']);
		$id_evento->id_evento = htmlentities($row['id_evento']);
		$fecha_inscrito->fecha_inscrito = htmlentities($row['fecha_inscrito']);
		$nombre->apellido_paterno = htmlentities($row['apellido_paterno']);
		$nombre->apellido_materno = htmlentities($row['apellido_materno']);
		$nombre->nombre = htmlentities($row['nombre']);
		$nombre_evento->nombre_evento = htmlentities($row['nombre_evento']);
		$correo_electronico->correo_electronico = htmlentities($row['correo_electronico']);

		//Arreglos
		$arrId_usuario[] = ($id_usuario->id_usuario);
		$arrId_evento[] = ($id_evento->id_evento);
		$arrFecha_inscrito[] = ($fecha_inscrito->fecha_inscrito);	
		$arrNombres[] = ($nombre->nombre.' '.$nombre->apellido_paterno.' '.$nombre->apellido_materno);	
		$arrNombre_evento[] = ($nombre_evento->nombre_evento);	
		$arrCorreo_electronico[] = ($correo_electronico->correo_electronico);	
		
		$cant_reg++;	
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Consulta de personas registradas a eventos</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">	
  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>

<!--Tabla de cursos activos -->
	<div class="container">
		<h1><center>Consulta de personas registradas a eventos</center></h1>
		<br>

	<div class="container">
		<label>Buscar Persona</label><br>
		<input type="text" id="nombre" name="nombre">
		<input type="button" id="busca_nombre" name="busca_nombre_usuario" value="Buscar Usuario" onclick="buscar()">
	</div>	
		<br>
	<div class="container" id="datos_elemento">	
	</div>
		<br>

	<div class="container" id="datos_alumnos">	
	</div>
		<br>
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>ID usuario</th>
				<th scope="col" hidden>ID evento</th>
				<th scope="col">Fecha inscrito</th>
				<th scope="col">Nombre</th>
				<th scope="col">Evento al que esta inscrito</th>
				<th scope="col">Correo electronico</th>	

			</tr>
		</thead>
		<tbody>	
		<?php
		if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrNombres as $nombre){
					$id_usuari = $arrId_usuario[$rowCount];
					$id_event = $arrId_evento[$rowCount];
					$fecha_inscrit = $arrFecha_inscrito[$rowCount];
					$nombre_event = $arrNombre_evento [$rowCount];
					$correo_electronic = $arrCorreo_electronico [$rowCount];
				
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id_usuari.'</th>';
						echo '<td scope="row" hidden>'.$id_event.'</td>';
						echo '<td>'.$fecha_inscrit.'</td>';
						echo '<td>'.$nombre.'</td>';
						echo '<td>'.$nombre_event.'</td>';
						echo '<td>'.$correo_electronic.'</td>';
					echo '</tr>';
								
					$rowCount ++;
				}
		}else{
				echo '<div class="">';
					echo '<p>No hay usuarios registrados</p>';
				echo '</div>';	
			}
		?>
		</tbody>
	</table>
	</div>
	 <br>
	<div class = "container">
		<a href= "main.php"><button type="button">Regresar</button></a>
	</div>
<br>	
  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
<script>
	function buscar(){
		elemento = document.getElementById("nombre").value;
		elemento.trim();
		if( elemento != ''){///elemento
			///document.getElementById("datos_usuario").innerHTML = '<p style = "color: green";>Hola '+elemento;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText != ""){						
						document.getElementById("datos_elemento").innerHTML = "<br>"+this.responseText;
					}
				}
			};
			xmlhttp.open("GET","buscar_inscritos_evento.php?elemento="+elemento,true);
			xmlhttp.send();			
		}///elemento no está vacío		
	}
</script>
</html>