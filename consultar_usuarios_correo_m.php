<?php

	require_once "conexion.php";
	require_once "clases.php";
	session_start();	
	//var_dump($_POST);
	
	$_SESSION['entrada'] = 1;

	$_SESSION['success'] = "Se le ha enviado un mensaje por correo a todos";
	
	//Istaciacion de clases
	$id_usuario = new Personas_Registradas();
	$ap_Paterno = new Personas_Registradas();
	$ap_Materno = new Personas_Registradas();
	$nombre = new Personas_Registradas();
	$ano_nacimiento = new Personas_Registradas();
	$correo_electronico = new Personas_Registradas();
	$telefono = new Personas_Registradas();
	$organizacion = new Personas_Registradas();
	$colonia = new Personas_Registradas();

	
	//DB connection
	$conectado = 1;
	
	//stmt es la abreviatura de statement
	$qry = "SELECT * FROM comunitec_tbl_usuarios";
	$stmt = $link->query($qry);
	$cant_reg = 0;
	while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
		//print_r($row);
		$id_usuario->id_usuario = htmlentities($row['id_usuario']);
		$nombre->ap_Paterno = htmlentities($row['ap_Paterno']);
		$nombre->ap_Materno = htmlentities($row['ap_Materno']);
		$nombre->nombre = htmlentities($row['nombre']);
		$ano_nacimiento->ano_nacimiento = htmlentities($row['ano_nacimiento']);
		$correo_electronico->correo_electronico = htmlentities($row['correo_electronico']);	
		$telefono->telefono = htmlentities($row['telefono']);
		$organizacion->organizacion = htmlentities($row['organizacion']);
		$colonia->colonia = htmlentities($row['colonia']);		

		$arrId_usuario[] = ($id_usuario->id_usuario);
		$arrAp_Paterno[] = ($ap_Paterno->ap_Paterno);	
		$arrAp_Materno[] = ($ap_Materno->ap_Materno);
		$arrNombre[] = ($nombre->nombre.' '.$nombre->ap_Paterno.' '.$nombre->ap_Materno);
		$arrAno_Nacimientos[] = ($ano_nacimiento->ano_nacimiento);
		$arrCorreo_Electronico[] = ($correo_electronico->correo_electronico);
		$arrTelefono[] = ($telefono->telefono);
		$arrOrganizacion[] = ($organizacion->organizacion);
		$arrColonia[] = ($colonia->colonia);

		$cant_reg++;
		

	}
	
	$subject = "Bienvenido al portal Comunitec32k";
	$message = "Este es un mensaje de prueba, gracias por tu interes en el portal de Comunitec32k, esperamos que puedas superarte con nuestros cursos";			
	
	if($cant_reg > 0){
	$rowCount1=0;
		foreach ($arrCorreo_Electronico as $correos){
			enviarEmail($subject, $message, $correos); 	
		}
	}
	
	//========Funciones========// 	
	function enviarEmail($subject, $message, $correos){
		
		$sender = "felipe.galan@comunitec32k.com";
		$email = $sender;
		$name2send = "Comunitec32k";
		///$mailto = $email.",".$sender;
		$mailto = $correos;
		///$mailto = $ceo;
		
		$from="From: $name2send<$email>\r\nReturn-path: $sender";
		///$subject=
		///$message=
			
		mail($mailto, $subject, $message, $from);	
	}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Usuarios comunitec32k</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Montserrat:300,400,500,600,700" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="assets/vendor/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <link href="assets/css/tablas_Style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Rapid - v2.1.0
  * Template URL: https://bootstrapmade.com/rapid-multipurpose-bootstrap-business-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>
<body>
<br>
<!--Tabla de usaurios -->
	<div class="container">
		<h1><center>Consulta de Usuarios</center></h1>
		<br>
		
<?php
			if( isset($_SESSION['success']) ){
				echo '<p style="color:green;">'.htmlentities(trim($_SESSION['success'])).'</p>';
				unset($_SESSION['success']);
			}
			
		//print_r($_SESSION); //En caso de revision descomentar el print_r	
?>			
		
	<!---Ajax--->
	<div class="container">
		<label>Buscar Persona</label><br>
		<input type="text" id="nombre_usuario" name="nombre" placeholder="Ej. Programacion">
		<input type="button" id="busca_nombre_usuario" name="busca_nombre_usuario" value="Buscar Usuario" onclick="buscar_usuario()">
	</div>	
		<br>
	<div class="container" id="datos_usuario">	
	</div>
		<br><!---Fin de Ajax--->

	
	<div class="container" id="">	
		<form method="POST">
			<input type="submit" id="enviar_correos" name="enviar_correos" value="Enviar correo">
		</form>
	</div>
	
		<br>			
	<table>
		<thead>
			<tr>
				<th scope="col" hidden>ID Usuario</th>
				<th scope="col">Nombre</th>
				<th scope="col">Año de nacimiento</th>
				<th scope="col">Correo Electronico</th>
				<th scope="col">Telefono</th>
				<th scope="col">Organización</th>
				<th scope="col">Colonia / Fraccionamiento</th>
				<th scope="col">Actualizar</th>
			</tr>
		</thead>
		<tbody>
			<?php
			if($cant_reg > 0){
				$rowCount=0;
				foreach ($arrNombre as $usuario){
					$id = $arrId_usuario[$rowCount];
					$Fe_Nacimiento = $arrAno_Nacimientos [$rowCount];
					$Co_Electronico = $arrCorreo_Electronico[$rowCount];
					$Tel = $arrTelefono[$rowCount];
					$Org = $arrOrganizacion[$rowCount];
					$Col = $arrColonia[$rowCount];
					
					echo '<tr>';
						echo '<th scope="row" hidden>'.$id.'</th>';
						echo '<td>'.$usuario.'</td>';
						echo '<td>'.$Fe_Nacimiento.'</td>';
						echo '<td>'.$Co_Electronico.'</td>';
						echo '<td>'.$Tel.'</td>';
						echo '<td>'.$Org.'</td>';
						echo '<td>'.$Col.'</td>';
						echo	"<td>	
									<a href='consultar_usuarios_correo.php?id=".$id."'>
										<button>Enviar correo</button>
									</a>	
								</td>";
					echo '</tr>';
						
					$rowCount ++;			
				}

			}else{
				echo '<div class="">';
					echo '<p>No hay usuarios registrados</p>';
				echo '</div>';	
			}
			?>
		</tbody>	
	</table>
	</div>
	 <br>
	<div class = "container">
		<a href= "main.php"><button type="button">Regresar</button></a>
	</div>
<br>


  
  <!-- ======= Footer ======= -->
  <footer id="footer" class="section-bg">
    <div class="footer-top">


    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Rapid</strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!--
        All the links in the footer should remain intact.
        You can delete the links only if you purchased the pro version.
        Licensing information: https://bootstrapmade.com/license/
        Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Rapid
      -->
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer><!-- End  Footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/mobile-nav/mobile-nav.js"></script>
  <script src="assets/vendor/wow/wow.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>
  
</body>
<script>
	function buscar_usuario(){
		usuario = document.getElementById("nombre_usuario").value;
		usuario.trim();
		if( usuario != ''){///usuario
			///document.getElementById("datos_usuario").innerHTML = '<p style = "color: green";>Hola '+usuario;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp = new XMLHttpRequest();
			} else {
				// code for IE6, IE5
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange = function() {
				if (this.readyState == 4 && this.status == 200) {
					if (this.responseText != ""){						
						document.getElementById("datos_usuario").innerHTML = "<br>"+this.responseText;
					}
				}
			};
			xmlhttp.open("GET","buscar_usuarios.php?usuario="+usuario,true);
			xmlhttp.send();			
		}///usuario no está vacío

		
	}
</script>
</html>